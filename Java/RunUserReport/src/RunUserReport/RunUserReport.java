package RunUserReport;


import java.awt.Color;
import java.io.*;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.*;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.lang.*;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSetMetaData;

import org.ini4j.Profile.Section;
import org.ini4j.Wini;

import com.fasterxml.jackson.databind.ObjectMapper;



public class RunUserReport {
	
	
	
	//Global vars declaration
	
	//Skyline db
	public static BasicDataSource dataSource = new BasicDataSource();
	//Information Schema db
	public static BasicDataSource dataSourceIS = new BasicDataSource();
	//Params json passed from php
	//public static Map<String, Object> params = new HashMap<String, Object>();
	//Active db name
	public static String dbName = null;
	//Scope
	public static Map<String, Object> scope = null;
	//Path
	public static String path = null;
	//Mail settings
	public static String mailUserName = null;
	public static String mailPassword = null;
	public static String mailHost = null;
	public static String mailPort = null;
	
	
	
	//Constructor
	public RunUserReport() throws Exception {
		
		//get the root path of current instance of Skyline
    	String pth = RunUserReport.class.getProtectionDomain().getCodeSource().getLocation().getPath();
    	String decodedPath = URLDecoder.decode(pth, "UTF-8");
    	path = decodedPath;
    	if (path.contains("C:/xampp/htdocs/Skyline/Java")) {
    		path = "C:/xampp/htdocs/Skyline/public";
    	} else {
        	path = path.substring(0, path.lastIndexOf("/jar"));
        	if (path.matches("^/[A-Z]:/.{0,}")) {
        		path = path.substring(1);
        	}
    	}
    	
		//retrieving skyline's conf
		String ini = path + "/application/config/application.ini";
		
		String userName = null;
		String password = null;
		
		File f = new File(ini);
		if (f.exists()) {
			//config file exists
			//Wini conf = new Wini(f);
			//String[] tmp = conf.get("DataBase", "Conn").split("=");
			//dbName = tmp[2].replace("\"", "");
			//userName = conf.get("DataBase", "Username");
			//password = conf.get("DataBase", "Password");
            //Above commented out by Neil Brownlee to fix issue with Reporting. New Config needed in all live systems after update.
            Wini conf = new Wini(f);
			String[] tmp = conf.get("ReportDataBase", "Conn").split("=");
			dbName = tmp[2].replace("\"", "");
			userName = conf.get("ReportDataBase", "Username");
			password = conf.get("ReportDataBase", "Password");
            
            
			mailUserName = conf.get("SMTP", "Username");
			mailPassword = conf.get("SMTP", "Password");
			mailHost = conf.get("SMTP", "Host");
			mailPort = conf.get("SMTP", "Port");
		} else {		
			//config file doesn't exist, assigning default values
			dbName = "skyline";
			userName = "root";
			password = "";
		}
		
		//initiate current database
    	dataSource.setDriverClassName("com.mysql.jdbc.Driver");
    	dataSource.setUsername(userName);
    	if(password != null && !password.equals("") && !password.equals("\"\"")) {
    		dataSource.setPassword(password);
		}
    	dataSource.addConnectionProperty("zeroDateTimeBehavior", "convertToNull");
    	dataSource.addConnectionProperty("allowMultiQueries", "true");
    	dataSource.setUrl("jdbc:mysql://localhost/" + dbName);
    	//dataSource.setUrl("jdbc:mysql://localhost/" + dbName + "?zeroDateTimeBehavior=convertToNull&allowMultiQueries=true");
    	
    	//initiate information schema
    	dataSourceIS.setDriverClassName("com.mysql.jdbc.Driver");
    	dataSourceIS.setUsername(userName);
    	if(password != null && !password.equals("") && !password.equals("\"\"")) {
    		dataSourceIS.setPassword(password);
		}
    	dataSourceIS.setUrl("jdbc:mysql://localhost/information_schema");
    	
	}
	
	
	
	//Main method
	public static void main(String[] args) throws Exception {
		
		@SuppressWarnings("unused")
		RunUserReport n = new RunUserReport();
		
		Map<String, Object> params = getParams(args);
		
		String file = createReport(params);
		
		System.out.println(file);
		
    }
	
	
	
	//Create and save report file
	@SuppressWarnings("all")
	public static String createReport(Map<String, Object> params) throws Exception {
		
    	Map<String, Object> queryData = null;
    	String query = null;
    	ArrayList<Map<String, Object>> columnData = null;
    	String reportName = null;
    	
    	if (params.containsKey("getCount") && (boolean)params.get("getCount")) {
    		//get main query for row count
	        queryData = buildQueryCount(params);
	        query = (String) queryData.get("query");
    	} else {
	        //get main query and table data        
	        queryData = buildQuery(params);
	        query = (String) queryData.get("query");
	        String primaryTable = (String) queryData.get("primaryTable");
	        reportName = (String) queryData.get("reportName");
	        String reportDescription = (String) queryData.get("reportDescription");
	        ArrayList<Map<String, Object>> tableData = (ArrayList<Map<String, Object>>) queryData.get("tableData");
	        columnData = (ArrayList<Map<String, Object>>) queryData.get("columnData");  
    	}
        
        
    	Map<String, Object> queryParams = new HashMap<String, Object>();
    	if (scope != null) {
    		queryParams.put("id", params.get("userTypeID"));
    	}
    	
        ArrayList<String[]> data = null;
        
    	if (params.containsKey("getCount") && (boolean)params.get("getCount")) {
	        //get the main data set row count
	        data = query(query, queryParams);
	        return data.get(0)[0];
    	} else {
	        //get the main data set
	        data = query(query, queryParams);
    	}
        
        
        //Add header to data set
        String[] header = new String[columnData.size()];
        for(int i = 0; i < header.length; i++) {
        	header[i] = (String) columnData.get(i).get("Name");
        }
        data.add(0, header);
        

        //create new excel file
		XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet sheet = wb.createSheet("Report");
        
        
        /*
        //Logo
        String logoPath = "";
        if(params.get("logo") == null) {
        	logoPath = "C:\\xampp\\htdocs\\Skyline\\public\\images\\brandLogos\\1000_logo.png";
        } else {
        	logoPath = path + "/images/brandLogos/" + params.get("logo");
        }
        InputStream is = new FileInputStream(logoPath);
        byte[] bytes = IOUtils.toByteArray(is);
        int pictureIdx = wb.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG);
        is.close();

        CreationHelper helper = wb.getCreationHelper();
        Drawing drawing = sheet.createDrawingPatriarch();

        ClientAnchor anchor = helper.createClientAnchor();
        anchor.setCol1(4);
        anchor.setRow1(2);
        Picture pict = drawing.createPicture(anchor, pictureIdx);
    	pict.resize();       
        */
        
        
        //get styles
        Map<String, XSSFCellStyle> styles = createStyles(wb);
        String sheetRef = sheet.getPackagePart().getPartName().getName();

        //save the template
        File template = new File("template.xlsx");
        FileOutputStream os = new FileOutputStream(template);
        wb.write(os);
        os.close();

        //create a substitute xml, generate data and write to it
        File tmp = File.createTempFile("sheet", ".xml");
        Writer fw = new FileWriter(tmp);
        generate(fw, styles, data, queryData);
        fw.close();

        //create final excel file name
	    Calendar c = new GregorianCalendar();
	    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss.SS");
	    String now = format.format(c.getTime());
	    String fileName = reportName + " " + now;
        String pathFinal = path + "/reports/" + fileName + ".xlsx";
	    
        //switch main sheet xml in excel package with generated substitute
        FileOutputStream out = new FileOutputStream(new File(pathFinal));
        substitute(template, tmp, sheetRef.substring(1), out);
        out.close();
        
		System.gc();
        template.delete();
		
        //return generated file name
		return fileName;
		
	}
	
	
	
	@SuppressWarnings("all")
	public static Map<String, Object> getParams(String[] args) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> params = new HashMap<String, Object>();
		
    	if (args != null && args.length != 0) {
    		String hash = args[0];
    		String pathTmp = path + "/temp/" + hash + ".json";
    		File input = new File(pathTmp);
    		params = mapper.readValue(input, Map.class);
    		input.delete();
    	} else {
    		//default params
    		params.put("reportID", "2");
    	}
    	
    	return params;
    	
	}
    
	
	
	//Main query building and table data retrieval method
	@SuppressWarnings("all")
	private static Map<String, Object> buildQuery(Map<String, Object> params) throws Exception {
		
		//get report data
		String q = "SELECT * FROM user_reports WHERE UserReportID = :reportID";
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("reportID", params.get("reportID"));
		ArrayList<String[]> reportData = query(q, values);
		
		//get user data
		q = "SELECT * FROM user WHERE UserID = :userID";
		values = new HashMap<String, Object>();
		values.put("userID", reportData.get(0)[1]);
		ArrayList<String[]> userData = query(q, values);
		
		//parse report structure json into object
    	ObjectMapper mapper = new ObjectMapper();
    	Map<String, Object> s = mapper.readValue(reportData.get(0)[6], HashMap.class);
    	
    	//get report columns
    	ArrayList<HashMap> columns = (ArrayList<HashMap>) s.get("columns");
    	for(int i = 0; i < columns.size(); i++) {
        	//get column comment json from information schema
    		q = "SELECT	COLUMN_COMMENT AS comment " +
        		"FROM 	COLUMNS " +
        		"WHERE	TABLE_SCHEMA = :db AND" +
        		"		TABLE_NAME = :table AND" +
        		"		COLUMN_NAME = :column";
        	values = new HashMap<String, Object>();
        	values.put("db", dbName);
        	values.put("table", columns.get(i).get("table"));
        	values.put("column", columns.get(i).get("column"));
        	ArrayList<String[]> columnRaw = query(q, values, dataSourceIS);
        	//parse column comment json into object
        	Map<String, Object> columnData = mapper.readValue(columnRaw.get(0)[0], HashMap.class);
        	//merge column data into columns object
        	columns.get(i).putAll(columnData);
    	}
    	
    	
    	//build the final query
    	
    	String query = "SELECT ";
    	
		//return whole set of data
		for(int i = 0; i < columns.size(); i++) {
			//log(columns.get(i).toString() + "\n");
			if(columns.get(i).get("RefTable") != null) {
				query += ((Map<String, Object>) columns.get(i).get("RefTable")).get("RefTableCol");
			} else {
	    		query += columns.get(i).get("table") + "." + columns.get(i).get("column");
			}
			if(i != columns.size() - 1) {
				query += ", ";
			}
    	}
    	
    	query += " FROM " + s.get("primaryTable");
    	
    	
    	//get list of tables
    	ArrayList<String> tableList = new ArrayList<String>();
    	for(int i = 0; i < columns.size(); i++) {
    		tableList.add((String) columns.get(i).get("table"));
    	}    	
    	//remove duplicates from "tables"
    	HashSet hsTables = new HashSet();
    	hsTables.addAll(tableList);
    	tableList.clear();
    	tableList.addAll(hsTables);
    	
    	
    	//get table data
    	String tablesInline = "";
    	for (int i = 0; i < tableList.size(); i++) {
    		tablesInline += "'" + tableList.get(i) + "'";
    		if (i != tableList.size() - 1) {
    			tablesInline += ",";
    		}
    	}
    	q = "SELECT		TABLES.TABLE_NAME 		AS 'table', " +
			"			TABLES.TABLE_COMMENT 	AS 'comment', " +
			"			COLUMNS.COLUMN_NAME 	AS 'primaryColumn' " +

			"FROM		TABLES " +

			"LEFT JOIN	COLUMNS ON TABLES.TABLE_NAME = COLUMNS.TABLE_NAME AND " + 
			"			COLUMNS.COLUMN_KEY = 'PRI' " +

			"WHERE		TABLES.TABLE_SCHEMA = :db AND " + 
			"			COLUMNS.TABLE_SCHEMA = :db AND " +
			"			TABLES.TABLE_NAME IN (" + tablesInline + ")";

    	values = new HashMap<String, Object>();
    	values.put("db", dbName);
    	
    	ArrayList<String[]> tableRaw = query(q, values, dataSourceIS);
    	ArrayList<HashMap> tableData = new ArrayList<HashMap>();
    	for(String[] row: tableRaw) {
    		tableData.add(new HashMap());
			tableData.get(tableData.size() - 1).put("tableName", tableRaw.get(tableData.size() - 1)[0]);
			tableData.get(tableData.size() - 1).put("displayName", mapper.readValue(row[1], HashMap.class).get("Name"));
			tableData.get(tableData.size() - 1).put("primaryCol", tableRaw.get(tableData.size() - 1)[2]);
			if (tableRaw.get(tableData.size() - 1)[0].equals(s.get("primaryTable")) && params.get("userType") != null && !params.get("userType").equals("Admin")) {
				scope = (Map<String, Object>) mapper.readValue(row[1], Map.class).get("Scope");
			}
    	}
    	
    	
    	//mark single (1 to 1 relation) tables
    	for (int i = 0; i < tableData.size(); i++) {

    		//skip primary table
    		if (tableData.get(i).get("tableName").equals(s.get("primaryTable"))) {
    			continue;
    		}
    		
    		q = "	SELECT	    col.REFERENCED_TABLE_NAME AS name, " +
    			"				col.COLUMN_NAME AS mainCol, " +
    			"				col.REFERENCED_COLUMN_NAME AS refCol, " +
    			"				tbls.TABLE_COMMENT AS comment " +
    			
    			"	FROM	    KEY_COLUMN_USAGE AS col " +
    			
    			"	LEFT JOIN   TABLES AS tbls ON tbls.TABLE_NAME = col.REFERENCED_TABLE_NAME AND " + 
    			"				tbls.TABLE_SCHEMA = col.CONSTRAINT_SCHEMA " +
    			
				"	WHERE	    col.TABLE_NAME = :primaryTable AND " + 
				"				col.CONSTRAINT_SCHEMA = :db AND " +
				"				tbls.TABLE_SCHEMA = :db AND " +
				"				col.CONSTRAINT_NAME != 'PRIMARY' AND " +
				"				tbls.TABLE_NAME = :secondaryTable " +
				
				"	GROUP BY    col.REFERENCED_TABLE_NAME";
    		
	    	values = new HashMap<String, Object>();
	    	values.put("primaryTable", s.get("primaryTable"));
	    	values.put("secondaryTable", tableData.get(i).get("tableName"));
	    	values.put("db", dbName);
	    	ArrayList<String[]> result = query(q, values, dataSourceIS);
	    	
	    	if(result.size() != 0) {
	    		tableData.get(i).put("single", true);
	    		tableData.get(i).put("refCol", result.get(0)[2]);
	    	} else {
	    		tableData.get(i).put("single", false);
	    	}
    	
    	}
	
    	
    	//mark multiple (multiple to 1 relation) tables
    	for (int i = 0; i < tableData.size(); i++) {

    		//skip primary table
    		if (tableData.get(i).get("tableName").equals(s.get("primaryTable"))) {
    			continue;
    		}
    		
	    	q = "	SELECT	    col.TABLE_NAME AS name, " +
	    		"				col.COLUMN_NAME AS mainCol, " +
	    		"				col.REFERENCED_COLUMN_NAME AS refCol, " +
	    		"				tbls.TABLE_COMMENT AS comment " +
	    		
	    		"	FROM	    KEY_COLUMN_USAGE AS col " +
	    		
				"	LEFT JOIN   TABLES AS tbls ON tbls.TABLE_NAME = col.TABLE_NAME AND " + 
				"				tbls.TABLE_SCHEMA = col.CONSTRAINT_SCHEMA " +
				
				"	WHERE	    col.REFERENCED_TABLE_NAME = :primaryTable AND " + 
				"				col.CONSTRAINT_SCHEMA = :db AND " +
				"				tbls.TABLE_SCHEMA = :db AND " +
				" 				tbls.TABLE_NAME = :secondaryTable";
				
	    	values = new HashMap<String, Object>();
	    	values.put("primaryTable", s.get("primaryTable"));
	    	values.put("secondaryTable", tableData.get(i).get("tableName"));
	    	values.put("db", dbName);
	    	ArrayList<String[]> result = query(q, values, dataSourceIS);
	    	
	    	if(result.size() != 0) {
	    		tableData.get(i).put("multi", true);
	    		tableData.get(i).put("refCol", result.get(0)[2]);
	    	} else {
	    		tableData.get(i).put("multi", false);
	    	}
	    	
    	}
    

    	//construct joins
    	
    	//join selected tables
    	for(int i = 0; i < tableData.size(); i++) {
    		
    		HashMap cur = tableData.get(i);
    		
    		//skip primary table
    		if(cur.get("tableName").equals(s.get("primaryTable"))) {
    			continue;
    		}
    		
    		if((Boolean) cur.get("multi") == true) {
    			query += " LEFT JOIN " + cur.get("tableName") + " ON " + cur.get("tableName") + "." + cur.get("refCol") + " = " + 
						 s.get("primaryTable") + "." + cur.get("refCol");
    		} else {
    			query += " LEFT JOIN " + cur.get("tableName") + " ON " + cur.get("tableName") + "." + cur.get("primaryCol") + " = "	+ 
    					 s.get("primaryTable") + "." + cur.get("refCol");
    		}

    	}
    	
    	//join column reference tables
    	for (int i = 0; i < columns.size(); i++) {
    		if(columns.get(i).get("RefTable") != null && ((Map<String, Section>) columns.get(i).get("RefTable")).get("RefTableName") != null) {
    			String refTable = (String) ((Map<String, Object>) columns.get(i).get("RefTable")).get("RefTableName");
    			String refTableID = (String) ((Map<String, Object>) columns.get(i).get("RefTable")).get("RefTableID");
    			query += " LEFT JOIN " + refTable + " ON " + refTable + "." + refTableID + " = " + s.get("primaryTable") + "." + columns.get(i).get("column");
    		}
    	}
    	
    	//scope
    	if (scope != null && params.get("userType") != null && params.get("userTypeID") != null) {
    		switch (params.get("userType").toString()) {
    			case "Network" : {
    				if (((Map<String, Object>) scope.get("Network")).get("Join") != null) {
    					query += " " + ((Map<String, Object>) scope.get("Network")).get("Join") + " ";
    				}
    				break;
    			}
    			case "Client" : {
    				if (((Map<String, Object>) scope.get("Client")).get("Join") != null) {
    					query += " " + ((Map<String, Object>) scope.get("Client")).get("Join") + " ";
    				}
    				break;
    			}
    			case "Branch" : {
    				if (((Map<String, Object>) scope.get("Branch")).get("Join") != null) {
    					query += " " + ((Map<String, Object>) scope.get("Branch")).get("Join") + " ";
    				}
    				break;
    			}
    			case "ServiceProvider" : {
    				if (((Map<String, Object>) scope.get("ServiceProvider")).get("Join") != null) {
    					query += " " + ((Map<String, Object>) scope.get("ServiceProvider")).get("Join") + " ";
    				}
    				break;
    			}
    			case "Manufacturer" : {
    				if (((Map<String, Object>) scope.get("Manufacturer")).get("Join") != null) {
    					query += " " + ((Map<String, Object>) scope.get("Manufacturer")).get("Join") + " ";
    				}
    				break;
    			}
    		}
    	}
     	
    	
    	//where clause
    	query += " WHERE TRUE ";
    	
    	//scope
    	if (scope != null && params.get("userType") != null && params.get("userTypeID") != null) {
    		switch (params.get("userType").toString()) {
				case "Network" : {
					query += " AND " + ((Map<String, Object>) scope.get("Network")).get("Where") + " ";
					break;
				}
				case "Client" : {
					query += " AND " + ((Map<String, Object>) scope.get("Client")).get("Where") + " ";
					break;
				}
				case "Branch" : {
					query += " AND " + ((Map<String, Object>) scope.get("Branch")).get("Where") + " ";
					break;
				}
				case "ServiceProvider" : {
					query += " AND " + ((Map<String, Object>) scope.get("ServiceProvider")).get("Where") + " ";
					break;
				}
				case "Manufacturer" : {
					query += " AND " + ((Map<String, Object>) scope.get("Manufacturer")).get("Where") + " ";
					break;
				}
			}
    	}
    	
    	ArrayList<String> subset = null;
    	
    	//column subsets
    	for (int i = 0; i < columns.size(); i++) {
    		subset = (ArrayList<String>) columns.get(i).get("subset");
    		if (subset != null && subset.size() != 0) {
    			
				if (subset.get(0).equals("-1") || subset.get(0).equals("")) {
					subset.remove(0);  
					if (subset.size() == 0) {
    					query += " AND " + columns.get(i).get("table") + "." + columns.get(i).get("column") + " IS NULL ";
					} else {
    					query += " AND (" + columns.get(i).get("table") + "." + columns.get(i).get("column") + " IS NULL OR " 
    							+ columns.get(i).get("table") + "." + columns.get(i).get("column") + " IN(" + StringUtils.join(subset, ",") + "))";
					}
				} else {
					if (subset.size() != 0) {
						query += " AND " + columns.get(i).get("table") + "." + columns.get(i).get("column") + " IN(" + StringUtils.join(subset, ",") + ")";
					}
				}
    			
    		}
    	}  
    	
    	//date range
    	if (params.get("filter") != null) {
			String column = (String) ((Map<String, Object>) params.get("filter")).get("column");
			if (column != null && !column.equals("")) {
				
				String dateFrom = (String) ((Map<String, Object>) params.get("filter")).get("dateFrom");
				String dateTo = (String) ((Map<String, Object>) params.get("filter")).get("dateTo");
				if (dateFrom == null || dateFrom.equals("")) {
					dateFrom = "0000-00-00";
				}
				if (dateTo == null || dateTo.equals("")) {
					dateTo = "9999-99-99";
				}
				
			    DateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy");
			    DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
			    
			    Date inputFrom = inputFormat.parse(dateFrom); 
				String from = outputFormat.format(inputFrom);
			    Date inputTo = inputFormat.parse(dateTo); 
				String to = outputFormat.format(inputTo);
			    
				if(from.equals("0000-00-00") && to.equals("9999-99-99")) {
					query += " AND (" + column + " >= '" + from + "' AND " + column + " <= '" + to + "') OR " + column + " IS NULL";
				} else {
					query += " AND " + column + " >= '" + from + "' AND " + column + " <= '" + to + "'";
				}
			}
			
			if (((Map<String, Section>) params.get("filter")).get("jobType") != null) {
				if (((Map<String, Object>) params.get("filter")).get("jobType").equals("open")) {
					query += " AND job.ClosedDate IS NULL ";
				}
				if (((Map<String, Object>) params.get("filter")).get("jobType").equals("open")) {
					query += " AND job.ClosedDate IS NOT NULL ";
				}
			}
			
    	}
    	
    	
    	//Group By
    	
    	//scope
    	if (scope != null && params.get("userType") != null && params.get("userTypeID") != null) {
    		switch (params.get("userType").toString()) {
    			case "Network" : {
    				if (((Map<String, Object>) scope.get("Network")).get("GroupBy") != null) {
    					query += " GROUP BY " + ((Map<String, Object>) scope.get("Network")).get("GroupBy") + " ";
    				}
    				break;
    			}
    			case "Client" : {
    				if (((Map<String, Object>) scope.get("Client")).get("GroupBy") != null) {
    					query += " GROUP BY " + ((Map<String, Object>) scope.get("Client")).get("GroupBy") + " ";
    				}
    				break;
    			}
    			case "Branch" : {
    				if (((Map<String, Object>) scope.get("Branch")).get("GroupBy") != null) {
    					query += " GROUP BY " + ((Map<String, Object>) scope.get("Branch")).get("GroupBy") + " ";
    				}
    				break;
    			}
    			case "ServiceProvider" : {
    				if (((Map<String, Object>) scope.get("ServiceProvider")).get("GroupBy") != null) {
    					query += " GROUP BY " + ((Map<String, Object>) scope.get("ServiceProvider")).get("GroupBy") + " ";
    				}
    				break;
    			}
    			case "Manufacturer" : {
    				if (((Map<String, Object>) scope.get("Manufacturer")).get("GroupBy") != null) {
    					query += " GROUP BY " + ((Map<String, Object>) scope.get("Manufacturer")).get("GroupBy") + " ";
    				}
    				break;
    			}
    		}
    	}
    	
    	
    	log("FINAL QUERY: " + query + "\n");
    	
    	Map<String, Object> ret = new HashMap<String, Object>();
    	ret.put("query", query);
    	ret.put("tableData", tableData);
    	ret.put("primaryTable", s.get("primaryTable"));
    	ret.put("columnData", columns);
    	ret.put("reportName", reportData.get(0)[4]);
    	ret.put("reportDescription", reportData.get(0)[5]);
    	
		return ret;
		
	}
	
	
	
	//Main query building and table data retrieval function
	@SuppressWarnings("all")
	private static Map<String, Object> buildQueryCount(Map<String, Object> params) throws Exception {
		
		//get report data
		/*
		String q = "SELECT * FROM user_reports WHERE UserReportID = :reportID";
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("reportID", params.get("reportID"));
		ArrayList<String[]> reportData = query(q, values);
		*/
		
		//get user data
		/*
		q = "SELECT * FROM user WHERE UserID = :userID";
		values = new HashMap<String, Object>();
		values.put("userID", reportData.get(0)[1]);
		ArrayList<String[]> userData = query(q, values);
		*/
		
		Map<String, Object> values = new HashMap<String, Object>();
		String q = "";
		
		//parse report structure json into object
    	ObjectMapper mapper = new ObjectMapper();
    	//Map<String, Object> s = mapper.readValue(structureJson, HashMap.class);
    	Map<String, Object> s = (Map<String, Object>) params.get("structure");

    	
    	//get report columns
    	ArrayList<HashMap> columns = (ArrayList<HashMap>) s.get("columns");
    	for(int i = 0; i < columns.size(); i++) {
        	//get column comment json from information schema
    		q = "SELECT	COLUMN_COMMENT AS comment " +
        		"FROM 	COLUMNS " +
        		"WHERE	TABLE_SCHEMA = :db AND" +
        		"		TABLE_NAME = :table AND" +
        		"		COLUMN_NAME = :column";
        	values = new HashMap<String, Object>();
        	values.put("db", dbName);
        	values.put("table", columns.get(i).get("table"));
        	values.put("column", columns.get(i).get("column"));
        	ArrayList<String[]> columnRaw = query(q, values, dataSourceIS);
        	//parse column comment json into object
        	Map<String, Object> columnData = mapper.readValue(columnRaw.get(0)[0], HashMap.class);
        	//merge column data into columns object
        	columns.get(i).putAll(columnData);
    	}
    	
    	
    	//build the final query
    	
    	String query = "SELECT COUNT(*) FROM " + s.get("primaryTable");
    	
    	
    	//get list of tables
    	ArrayList<String> tableList = new ArrayList<String>();
    	for(int i = 0; i < columns.size(); i++) {
    		tableList.add((String) columns.get(i).get("table"));
    	}    	
    	//remove duplicates from "tables"
    	HashSet hsTables = new HashSet();
    	hsTables.addAll(tableList);
    	tableList.clear();
    	tableList.addAll(hsTables);
    	//tableList.remove(s.get("primaryTable"));
    	
    	
    	//get table data
    	String tablesInline = "";
    	for(int i = 0; i < tableList.size(); i++) {
    		tablesInline += "'" + tableList.get(i) + "'";
    		if(i != tableList.size() - 1) {
    			tablesInline += ",";
    		}
    	}
    	q = "SELECT		TABLES.TABLE_NAME 		AS 'table', " +
			"			TABLES.TABLE_COMMENT 	AS 'comment', " +
			"			COLUMNS.COLUMN_NAME 	AS 'primaryColumn' " +

			"FROM		TABLES " +

			"LEFT JOIN	COLUMNS ON TABLES.TABLE_NAME = COLUMNS.TABLE_NAME AND " + 
			"			COLUMNS.COLUMN_KEY = 'PRI' " +

			"WHERE		TABLES.TABLE_SCHEMA = :db AND " + 
			"			COLUMNS.TABLE_SCHEMA = :db AND " +
			"			TABLES.TABLE_NAME IN (" + tablesInline + ")";

    	values = new HashMap<String, Object>();
    	values.put("db", dbName);
    	
    	ArrayList<String[]> tableRaw = query(q, values, dataSourceIS);
    	ArrayList<HashMap> tableData = new ArrayList<HashMap>();
    	
    	for (String[] row: tableRaw) {
    		tableData.add(new HashMap());
    		try {
				tableData.get(tableData.size() - 1).put("tableName", tableRaw.get(tableData.size() - 1)[0]);
				tableData.get(tableData.size() - 1).put("displayName", mapper.readValue(row[1], HashMap.class).get("Name"));
				tableData.get(tableData.size() - 1).put("primaryCol", tableRaw.get(tableData.size() - 1)[2]);
				if (tableRaw.get(tableData.size() - 1)[0].equals(s.get("primaryTable")) && !params.get("userType").equals("Admin")) {
					scope = (Map<String, Object>) mapper.readValue(row[1], HashMap.class).get("Scope");
				}
			} catch (Exception e) {
	    		log("table name: " + tableRaw.get(tableData.size() - 1)[0]);
				log("table json dump: " + row[1]);
				log(e.getLocalizedMessage());
			}
    	}

    	
    	//mark single (1 to 1 relation) tables
    	for(int i = 0; i < tableData.size(); i++) {

    		//skip primary table
    		if(tableData.get(i).get("tableName").equals(s.get("primaryTable"))) {
    			continue;
    		}
    		
    		q = "	SELECT	    col.REFERENCED_TABLE_NAME AS name, " +
    			"				col.COLUMN_NAME AS mainCol, " +
    			"				col.REFERENCED_COLUMN_NAME AS refCol, " +
    			"				tbls.TABLE_COMMENT AS comment " +
    			
    			"	FROM	    KEY_COLUMN_USAGE AS col " +
    			
    			"	LEFT JOIN   TABLES AS tbls ON tbls.TABLE_NAME = col.REFERENCED_TABLE_NAME AND " + 
    			"				tbls.TABLE_SCHEMA = col.CONSTRAINT_SCHEMA " +
    			
				"	WHERE	    col.TABLE_NAME = :primaryTable AND " + 
				"				col.CONSTRAINT_SCHEMA = :db AND " +
				"				tbls.TABLE_SCHEMA = :db AND " +
				"				col.CONSTRAINT_NAME != 'PRIMARY' AND " +
				"				tbls.TABLE_NAME = :secondaryTable " +
				
				"	GROUP BY    col.REFERENCED_TABLE_NAME";
    		
	    	values = new HashMap<String, Object>();
	    	values.put("primaryTable", s.get("primaryTable"));
	    	values.put("secondaryTable", tableData.get(i).get("tableName"));
	    	values.put("db", dbName);
	    	ArrayList<String[]> result = query(q, values, dataSourceIS);
	    	
	    	if(result.size() != 0) {
	    		tableData.get(i).put("single", true);
	    		tableData.get(i).put("refCol", result.get(0)[2]);
	    	} else {
	    		tableData.get(i).put("single", false);
	    	}
    	
    	}
	
    	
    	//mark multiple (multiple to 1 relation) tables
    	for(int i = 0; i < tableData.size(); i++) {

    		//skip primary table
    		if(tableData.get(i).get("tableName").equals(s.get("primaryTable"))) {
    			continue;
    		}
    		
	    	q = "	SELECT	    col.TABLE_NAME AS name, " +
	    		"				col.COLUMN_NAME AS mainCol, " +
	    		"				col.REFERENCED_COLUMN_NAME AS refCol, " +
	    		"				tbls.TABLE_COMMENT AS comment " +
	    		
	    		"	FROM	    KEY_COLUMN_USAGE AS col " +
	    		
				"	LEFT JOIN   TABLES AS tbls ON tbls.TABLE_NAME = col.TABLE_NAME AND " + 
				"				tbls.TABLE_SCHEMA = col.CONSTRAINT_SCHEMA " +
				
				"	WHERE	    col.REFERENCED_TABLE_NAME = :primaryTable AND " + 
				"				col.CONSTRAINT_SCHEMA = :db AND " +
				"				tbls.TABLE_SCHEMA = :db AND " +
				" 				tbls.TABLE_NAME = :secondaryTable";
				
	    	values = new HashMap<String, Object>();
	    	values.put("primaryTable", s.get("primaryTable"));
	    	values.put("secondaryTable", tableData.get(i).get("tableName"));
	    	values.put("db", dbName);
	    	ArrayList<String[]> result = query(q, values, dataSourceIS);
	    	
	    	if(result.size() != 0) {
	    		tableData.get(i).put("multi", true);
	    		tableData.get(i).put("refCol", result.get(0)[2]);
	    	} else {
	    		tableData.get(i).put("multi", false);
	    	}
	    	
    	}
    

    	//construct joins
    	
    	//join selected tables
    	for(int i = 0; i < tableData.size(); i++) {
    		
    		HashMap cur = tableData.get(i);
    		
    		//skip primary table
    		if(cur.get("tableName").equals(s.get("primaryTable"))) {
    			continue;
    		}
    		
    		if((Boolean) cur.get("multi") == true) {
    			query += " LEFT JOIN " + cur.get("tableName") + " ON " + cur.get("tableName") + "." + cur.get("refCol") + " = " + 
						 s.get("primaryTable") + "." + cur.get("refCol");
    		} else {
    			query += " LEFT JOIN " + cur.get("tableName") + " ON " + cur.get("tableName") + "." + cur.get("primaryCol") + " = "	+ 
    					 s.get("primaryTable") + "." + cur.get("refCol");
    		}

    	}
    	
    	//join column reference tables
    	for(int i = 0; i < columns.size(); i++) {
    		if(columns.get(i).get("RefTable") != null && ((Map<String, Section>) columns.get(i).get("RefTable")).get("RefTableName") != null) {
    			String refTable = (String) ((LinkedHashMap<String, Object>) columns.get(i).get("RefTable")).get("RefTableName");
    			String refTableID = (String) ((LinkedHashMap<String, Object>) columns.get(i).get("RefTable")).get("RefTableID");
    			query += " LEFT JOIN " + refTable + " ON " + refTable + "." + refTableID + " = " + columns.get(i).get("table") + "." + columns.get(i).get("column");
    		}
    	}

    	//scope
    	if (scope != null && params.get("userType") != null && params.get("userTypeID") != null) {
    		switch (params.get("userType").toString()) {
    			case "Network" : {
    				if (((Map<String, Object>) scope.get("Network")).get("Join") != null) {
    					query += " " + ((Map<String, Object>) scope.get("Network")).get("Join") + " ";
    				}
    				break;
    			}
    			case "Client" : {
    				if (((Map<String, Object>) scope.get("Client")).get("Join") != null) {
    					query += " " + ((Map<String, Object>) scope.get("Client")).get("Join") + " ";
    				}
    				break;
    			}
    			case "Branch" : {
    				if (((Map<String, Object>) scope.get("Branch")).get("Join") != null) {
    					query += " " + ((Map<String, Object>) scope.get("Branch")).get("Join") + " ";
    				}
    				break;
    			}
    			case "ServiceProvider" : {
    				if (((Map<String, Object>) scope.get("ServiceProvider")).get("Join") != null) {
    					query += " " + ((Map<String, Object>) scope.get("ServiceProvider")).get("Join") + " ";
    				}
    				break;
    			}
    			case "Manufacturer" : {
    				if (((Map<String, Object>) scope.get("Manufacturer")).get("Join") != null) {
    					query += " " + ((Map<String, Object>) scope.get("Manufacturer")).get("Join") + " ";
    				}
    				break;
    			}
    		}
    	}
    	
    	
    	//WHERE CLAUSE
    	query += " WHERE TRUE ";
    	
    	//scope
    	if (scope != null && params.get("userType") != null && params.get("userTypeID") != null) {
    		switch (params.get("userType").toString()) {
				case "Network" : {
					query += " AND " + ((Map<String, Object>) scope.get("Network")).get("Where") + " ";
					break;
				}
				case "Client" : {
					query += " AND " + ((Map<String, Object>) scope.get("Client")).get("Where") + " ";
					break;
				}
				case "Branch" : {
					query += " AND " + ((Map<String, Object>) scope.get("Branch")).get("Where") + " ";
					break;
				}
				case "ServiceProvider" : {
					query += " AND " + ((Map<String, Object>) scope.get("ServiceProvider")).get("Where") + " ";
					break;
				}
				case "Manufacturer" : {
					query += " AND " + ((Map<String, Object>) scope.get("Manufacturer")).get("Where") + " ";
					break;
				}
			}
    	}
    	
    	ArrayList<String> subset = null;
    	
    	//column subsets
    	for(int i = 0; i < columns.size(); i++) {
    		subset = (ArrayList<String>) columns.get(i).get("subset");
    		if(subset != null && subset.size() != 0) {
    			
				if(subset.get(0).equals("-1") || subset.get(0).equals("")) {
					subset.remove(0);
					if(subset.size() == 0) {
    					query += " AND " + columns.get(i).get("table") + "." + columns.get(i).get("column") + " IS NULL ";
					} else {
    					query += " AND (" + columns.get(i).get("table") + "." + columns.get(i).get("column") + " IS NULL OR " 
    							+ columns.get(i).get("table") + "." + columns.get(i).get("column") + " IN(" + StringUtils.join(subset, ",") + "))";
					}
				} else {
					if(subset.size() != 0) {
						query += " AND " + columns.get(i).get("table") + "." + columns.get(i).get("column") + " IN(" + StringUtils.join(subset, ",") + ")";
					}
				}
				
    		}
    	}    	
    	
    	//date range
    	if (params.get("filter") != null) {
			String column = (String) ((Map<String, Object>) params.get("filter")).get("column");
			if (column != null && !column.equals("")) {
				
				String dateFrom = (String) ((Map<String, Object>) params.get("filter")).get("dateFrom");
				String dateTo = (String) ((Map<String, Object>) params.get("filter")).get("dateTo");
				if (dateFrom == null || dateFrom.equals("")) {
					dateFrom = "0000-00-00";
				}
				if (dateTo == null || dateTo.equals("")) {
					dateTo = "9999-99-99";
				}
				
			    DateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy");
			    DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
			    
			    Date inputFrom = inputFormat.parse(dateFrom); 
				String from = outputFormat.format(inputFrom);
			    Date inputTo = inputFormat.parse(dateTo); 
				String to = outputFormat.format(inputTo);
			    
				if(from.equals("0000-00-00") && to.equals("9999-99-99")) {
					query += " AND (" + column + " >= '" + from + "' AND " + column + " <= '" + to + "') OR " + column + " IS NULL";
				} else {
					query += " AND " + column + " >= '" + from + "' AND " + column + " <= '" + to + "'";
				}
			}
			
			if (((Map<String, Section>) params.get("filter")).get("jobType") != null) {
				if (((Map<String, Section>) params.get("filter")).get("jobType").equals("open")) {
					query += " AND job.ClosedDate IS NULL ";
				}
				if (((Map<String, Section>) params.get("filter")).get("jobType").equals("open")) {
					query += " AND job.ClosedDate IS NOT NULL ";
				}
			}
			
    	}
    	
    	
    	//Group By
    	
    	//scope
    	if (scope != null && params.get("userType") != null && params.get("userTypeID") != null) {
    		switch (params.get("userType").toString()) {
    			case "Network" : {
    				if (((Map<String, Object>) scope.get("Network")).get("GroupBy") != null) {
    					query += " GROUP BY " + ((Map<String, Object>) scope.get("Network")).get("GroupBy") + " ";
    				}
    				break;
    			}
    			case "Client" : {
    				if (((Map<String, Object>) scope.get("Client")).get("GroupBy") != null) {
    					query += " GROUP BY " + ((Map<String, Object>) scope.get("Client")).get("GroupBy") + " ";
    				}
    				break;
    			}
    			case "Branch" : {
    				if (((Map<String, Object>) scope.get("Branch")).get("GroupBy") != null) {
    					query += " GROUP BY " + ((Map<String, Object>) scope.get("Branch")).get("GroupBy") + " ";
    				}
    				break;
    			}
    			case "ServiceProvider" : {
    				if (((Map<String, Object>) scope.get("ServiceProvider")).get("GroupBy") != null) {
    					query += " GROUP BY " + ((Map<String, Object>) scope.get("ServiceProvider")).get("GroupBy") + " ";
    				}
    				break;
    			}
    			case "Manufacturer" : {
    				if (((Map<String, Object>) scope.get("Manufacturer")).get("GroupBy") != null) {
    					query += " GROUP BY " + ((Map<String, Object>) scope.get("Manufacturer")).get("GroupBy") + " ";
    				}
    				break;
    			}
    		}
    	}

    	
    	log("FINAL QUERY: " + query + "\n");
    	
    	Map<String, Object> ret = new HashMap<String, Object>();
    	ret.put("query", query);
    	
		return ret;
		
	}	
	
	
	
	//generate main sheet xml file
    @SuppressWarnings("all")
	private static void generate(Writer out, Map<String, XSSFCellStyle> styles, ArrayList<String[]> data, Map<String, Object> meta) throws Exception {
    	
        Calendar calendar = Calendar.getInstance();
        
        SpreadsheetWriter sw = new SpreadsheetWriter(out);
        sw.beginSheet(data);
        
        //Document header
        sw.writeCustom("<row r=\"1\" ht=\"30\" customHeight=\"1\">");
    	sw.createCell(0, (String) meta.get("reportName"), styles.get("name").getIndex());
        sw.endRow();
        
        //Description
        sw.insertRow(2);
        sw.createCell(0, "Description:", styles.get("whiteBold").getIndex());
        sw.createCell(1, (String) meta.get("reportDescription"));
        sw.endRow();
        
        //Date Created
	    Calendar c = new GregorianCalendar();
	    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	    String now = format.format(c.getTime());
        sw.insertRow(3);
        sw.createCell(0, "Date Created:", styles.get("whiteBold").getIndex());
        sw.createCell(1, now);
        sw.endRow();
        
        /*
        //Table header
        sw.insertRow(2);
        sw.createCell(0, "Client:", styles.get("whiteBold").getIndex());
        sw.createCell(1, (String)params.get("clientName"));
        sw.endRow();
        
        sw.insertRow(3);
        sw.createCell(0, "Branch:", styles.get("whiteBold").getIndex());
        sw.createCell(1, (String)params.get("branchName"));
        sw.endRow();
        
        sw.insertRow(4);
        sw.createCell(0, "Date From:", styles.get("whiteBold").getIndex());
        sw.createCell(1, (String)params.get("dateFrom"));
        sw.endRow();
        
        sw.insertRow(5);
        sw.createCell(0, "Date To:", styles.get("whiteBold").getIndex());
        sw.createCell(1, (String)params.get("dateTo"));
        sw.endRow();
        */
        
        
        sw.insertRow(8);
        for(int i = 0; i < data.get(0).length; i++) {
        	sw.createCell(i, data.get(0)[i], styles.get("header").getIndex());
        }
        sw.endRow();
        
        
		int lastRow = 0;
        
        //Data rows
        for(int rownum = 1; rownum < data.size(); rownum++) {
        	
        	lastRow = rownum + 8;
        	
            sw.insertRow(rownum + 8);
            
            Double numVal;
            String stringVal;
            
            for(int i = 0; i < data.get(0).length; i++) {
            	
            	if(data.get(rownum)[i] == null) {
            		data.get(rownum)[i] = "";
            	}
            	
            	if(rownum % 2 == 0) {
            		
    				stringVal = stripControlChars(StringEscapeUtils.escapeXml(data.get(rownum)[i]));
        			try	{
        				numVal = Double.parseDouble(stringVal);
        				numVal = (double)(Math.round(numVal * 1000)) / 1000;
            			sw.createCell(i, numVal, styles.get("whiteRowLeft").getIndex());
        			} catch(NumberFormatException e) {
        				//not a double
            			sw.createCell(i, stringVal, styles.get("whiteRowLeft").getIndex());
        			}
            		
            	} else {
            		
    				stringVal = stripControlChars(StringEscapeUtils.escapeXml(data.get(rownum)[i]));
        			try	{
        				numVal = Double.parseDouble(stringVal);
        				numVal = (double)(Math.round(numVal * 1000)) / 1000;
            			sw.createCell(i, numVal, styles.get("grayRowLeft").getIndex());
        			} catch(NumberFormatException e) {
        				//not a double
            			sw.createCell(i, stringVal, styles.get("grayRowLeft").getIndex());
        			}
            		
            	}
            	
            }
            
            sw.endRow();
            
            calendar.roll(Calendar.DAY_OF_YEAR, 1);
        }
        
        /*
        //Totals
        sw.insertRow(lastRow + 2);
        sw.createCell(0, "Record Count:", styles.get("header").getIndex());
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 2, 1).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>" + "SUBTOTAL(9," + new CellReference(9, 1).formatAsString() + ":" + new CellReference(lastRow, 1).formatAsString() + ")</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 2, 2).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>" + "SUBTOTAL(9," + new CellReference(9, 2).formatAsString() + ":" + new CellReference(lastRow, 2).formatAsString() + ")</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 2, 3).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>" + "SUBTOTAL(9," + new CellReference(9, 3).formatAsString() + ":" + new CellReference(lastRow, 3).formatAsString() + ")</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 2, 4).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>" + "SUBTOTAL(9," + new CellReference(9, 4).formatAsString() + ":" + new CellReference(lastRow, 4).formatAsString() + ")</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 2, 5).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>" + "SUBTOTAL(9," + new CellReference(9, 5).formatAsString() + ":" + new CellReference(lastRow, 5).formatAsString() + ")</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 2, 6).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>" + "SUBTOTAL(9," + new CellReference(9, 6).formatAsString() + ":" + new CellReference(lastRow, 6).formatAsString() + ")</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 2, 7).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>" + "SUBTOTAL(9," + new CellReference(9, 7).formatAsString() + ":" + new CellReference(lastRow, 7).formatAsString() + ")</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 2, 8).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>" + "SUBTOTAL(9," + new CellReference(9, 8).formatAsString() + ":" + new CellReference(lastRow, 8).formatAsString() + ")</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 2, 9).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>" + "SUBTOTAL(9," + new CellReference(9, 9).formatAsString() + ":" + new CellReference(lastRow, 9).formatAsString() + ")</f></c>");
        sw.endRow();

        //Percentages
        sw.insertRow(lastRow + 3);
        sw.createCell(0, "Percentage:", styles.get("header").getIndex());
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 3, 1).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>(ROUND(((" + new CellReference(lastRow + 2, 1).formatAsString() + "/" + new CellReference(lastRow + 2, 9).formatAsString() + ") * 100),2))&amp;\"%\"</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 3, 2).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>(ROUND(((" + new CellReference(lastRow + 2, 2).formatAsString() + "/" + new CellReference(lastRow + 2, 9).formatAsString() + ") * 100),2))&amp;\"%\"</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 3, 3).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>(ROUND(((" + new CellReference(lastRow + 2, 3).formatAsString() + "/" + new CellReference(lastRow + 2, 9).formatAsString() + ") * 100),2))&amp;\"%\"</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 3, 4).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>(ROUND(((" + new CellReference(lastRow + 2, 4).formatAsString() + "/" + new CellReference(lastRow + 2, 9).formatAsString() + ") * 100),2))&amp;\"%\"</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 3, 5).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>(ROUND(((" + new CellReference(lastRow + 2, 5).formatAsString() + "/" + new CellReference(lastRow + 2, 9).formatAsString() + ") * 100),2))&amp;\"%\"</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 3, 6).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>(ROUND(((" + new CellReference(lastRow + 2, 6).formatAsString() + "/" + new CellReference(lastRow + 2, 9).formatAsString() + ") * 100),2))&amp;\"%\"</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 3, 7).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>(ROUND(((" + new CellReference(lastRow + 2, 7).formatAsString() + "/" + new CellReference(lastRow + 2, 9).formatAsString() + ") * 100),2))&amp;\"%\"</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 3, 8).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>(ROUND(((" + new CellReference(lastRow + 2, 8).formatAsString() + "/" + new CellReference(lastRow + 2, 9).formatAsString() + ") * 100),2))&amp;\"%\"</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 3, 9).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>(ROUND(((" + new CellReference(lastRow + 2, 9).formatAsString() + "/" + new CellReference(lastRow + 2, 9).formatAsString() + ") * 100),2))&amp;\"%\"</f></c>");
        sw.endRow();
		*/
        
        sw.endSheet(data);
        
    }
	
	
	
    private static Map<String, XSSFCellStyle> createStyles(XSSFWorkbook wb) {
    	
        Map<String, XSSFCellStyle> styles = new HashMap<String, XSSFCellStyle>();
        XSSFDataFormat fmt = wb.createDataFormat();
        
        XSSFColor borderColor = new XSSFColor(Color.decode("0xA4A4A4"));
        
        XSSFCellStyle style1 = wb.createCellStyle();
        style1.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
        style1.setDataFormat(fmt.getFormat("0.0%"));
        styles.put("percent", style1);
        
        XSSFCellStyle style2 = wb.createCellStyle();
        style2.setAlignment(XSSFCellStyle.ALIGN_CENTER);
        style2.setDataFormat(fmt.getFormat("0.0X"));
        styles.put("coeff", style2);
        
        XSSFCellStyle style3 = wb.createCellStyle();
        style3.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
        style3.setDataFormat(fmt.getFormat("$#,##0.00"));
        styles.put("currency", style3);
        
        XSSFCellStyle style4 = wb.createCellStyle();
        style4.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
        style4.setDataFormat(fmt.getFormat("mmm dd"));
        styles.put("date", style4);
        
        XSSFCellStyle style5 = wb.createCellStyle();
        XSSFFont headerFont = wb.createFont();
        headerFont.setBold(true);
        style5.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        style5.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
        style5.setFont(headerFont);
        style5.setBorderBottom(CellStyle.BORDER_THIN);
        style5.setBottomBorderColor(borderColor);
        style5.setBorderTop(CellStyle.BORDER_THIN);
        style5.setTopBorderColor(borderColor);
        style5.setBorderRight(CellStyle.BORDER_THIN);
        style5.setRightBorderColor(borderColor);
        style5.setBorderLeft(CellStyle.BORDER_THIN);
        style5.setLeftBorderColor(borderColor);
        styles.put("header", style5);
        
        XSSFCellStyle darkGrayCenter = wb.createCellStyle();
        darkGrayCenter.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        darkGrayCenter.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
        darkGrayCenter.setFont(headerFont);
        darkGrayCenter.setBorderBottom(CellStyle.BORDER_THIN);
        darkGrayCenter.setBottomBorderColor(borderColor);
        darkGrayCenter.setBorderTop(CellStyle.BORDER_THIN);
        darkGrayCenter.setTopBorderColor(borderColor);
        darkGrayCenter.setBorderRight(CellStyle.BORDER_THIN);
        darkGrayCenter.setRightBorderColor(borderColor);
        darkGrayCenter.setBorderLeft(CellStyle.BORDER_THIN);
        darkGrayCenter.setLeftBorderColor(borderColor);
        darkGrayCenter.setAlignment(CellStyle.ALIGN_CENTER);
        styles.put("darkGrayCenter", darkGrayCenter);
        
		XSSFCellStyle grayRowCenter = wb.createCellStyle();
		grayRowCenter.setFillForegroundColor(new XSSFColor(Color.decode("0xE8E8E8")));
		grayRowCenter.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		grayRowCenter.setBorderBottom(CellStyle.BORDER_THIN);
		grayRowCenter.setBottomBorderColor(borderColor);
		grayRowCenter.setBorderTop(CellStyle.BORDER_THIN);
		grayRowCenter.setTopBorderColor(borderColor);
		grayRowCenter.setBorderRight(CellStyle.BORDER_THIN);
		grayRowCenter.setRightBorderColor(borderColor);
		grayRowCenter.setBorderLeft(CellStyle.BORDER_THIN);
		grayRowCenter.setLeftBorderColor(borderColor);
		grayRowCenter.setAlignment(CellStyle.ALIGN_CENTER);
        styles.put("grayRowCenter", grayRowCenter);
        
		XSSFCellStyle grayRowLeft = wb.createCellStyle();
		grayRowLeft.setFillForegroundColor(new XSSFColor(Color.decode("0xE8E8E8")));
		grayRowLeft.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		grayRowLeft.setBorderBottom(CellStyle.BORDER_THIN);
		grayRowLeft.setBottomBorderColor(borderColor);
		grayRowLeft.setBorderTop(CellStyle.BORDER_THIN);
		grayRowLeft.setTopBorderColor(borderColor);
		grayRowLeft.setBorderRight(CellStyle.BORDER_THIN);
		grayRowLeft.setRightBorderColor(borderColor);
		grayRowLeft.setBorderLeft(CellStyle.BORDER_THIN);
		grayRowLeft.setLeftBorderColor(borderColor);
		grayRowLeft.setAlignment(CellStyle.ALIGN_LEFT);
        styles.put("grayRowLeft", grayRowLeft);
        
        
		XSSFCellStyle whiteRowCenter = wb.createCellStyle();
		whiteRowCenter.setFillForegroundColor(new XSSFColor(Color.decode("0xFFFFFF")));
		whiteRowCenter.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		whiteRowCenter.setBorderBottom(CellStyle.BORDER_THIN);
		whiteRowCenter.setBottomBorderColor(borderColor);
		whiteRowCenter.setBorderTop(CellStyle.BORDER_THIN);
		whiteRowCenter.setTopBorderColor(borderColor);
		whiteRowCenter.setBorderRight(CellStyle.BORDER_THIN);
		whiteRowCenter.setRightBorderColor(borderColor);
		whiteRowCenter.setBorderLeft(CellStyle.BORDER_THIN);
		whiteRowCenter.setLeftBorderColor(borderColor);
		whiteRowCenter.setAlignment(CellStyle.ALIGN_CENTER);
		styles.put("whiteRowCenter", whiteRowCenter);
		
		XSSFCellStyle whiteRowLeft = wb.createCellStyle();
		whiteRowLeft.setFillForegroundColor(new XSSFColor(Color.decode("0xFFFFFF")));
		whiteRowLeft.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		whiteRowLeft.setBorderBottom(CellStyle.BORDER_THIN);
		whiteRowLeft.setBottomBorderColor(borderColor);
		whiteRowLeft.setBorderTop(CellStyle.BORDER_THIN);
		whiteRowLeft.setTopBorderColor(borderColor);
		whiteRowLeft.setBorderRight(CellStyle.BORDER_THIN);
		whiteRowLeft.setRightBorderColor(borderColor);
		whiteRowLeft.setBorderLeft(CellStyle.BORDER_THIN);
		whiteRowLeft.setLeftBorderColor(borderColor);
		whiteRowLeft.setAlignment(CellStyle.ALIGN_LEFT);
		styles.put("whiteRowLeft", whiteRowLeft);
		
		XSSFCellStyle nameStyle = wb.createCellStyle();
        Font font = wb.createFont();
        font.setFontHeightInPoints((short)24);
        nameStyle.setFont(font);
        styles.put("name", nameStyle);
        
		XSSFCellStyle whiteBold = wb.createCellStyle();
        whiteBold.setFont(headerFont);
        styles.put("whiteBold", whiteBold);
        
        return styles;
    }

    
    
//==========================================================================================================
    
    private static void substitute(File zipfile, File tmpfile, String entry, OutputStream out) throws IOException {
    	
        ZipFile zip = new ZipFile(zipfile);

        ZipOutputStream zos = new ZipOutputStream(out);

        @SuppressWarnings("unchecked")
        Enumeration<ZipEntry> en = (Enumeration<ZipEntry>) zip.entries();
        while (en.hasMoreElements()) {
            ZipEntry ze = en.nextElement();
            if(!ze.getName().equals(entry)){
                zos.putNextEntry(new ZipEntry(ze.getName()));
                InputStream is = zip.getInputStream(ze);
                copyStream(is, zos);
                is.close();
            }
        }
        zos.putNextEntry(new ZipEntry(entry));
        InputStream is = new FileInputStream(tmpfile);
        copyStream(is, zos);
        is.close();

        zos.close();
        zip.close();
    }

    
    
    private static void copyStream(InputStream in, OutputStream out) throws IOException {
        byte[] chunk = new byte[1024];
        int count;
        while ((count = in.read(chunk)) >=0 ) {
          out.write(chunk,0,count);
        }
    }

    
    
    public static class SpreadsheetWriter {
    	
        private final Writer _out;
        private int _rownum;

        
        public SpreadsheetWriter(Writer out){
            _out = out;
        }
        
        
        public void beginSheet(ArrayList<String[]> data) throws IOException {
            _out.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        	_out.write("<worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\"><dimension ref=\"A1\"/>");
        	_out.write("<sheetViews><sheetView workbookViewId=\"0\" showGridLines=\"false\" tabSelected=\"true\"><pane ySplit=\"9\" topLeftCell=\"A10\" activePane=\"bottomLeft\" state=\"frozen\"/></sheetView></sheetViews>");
        	_out.write("<sheetFormatPr defaultRowHeight=\"15.0\"/>");
        	
        	_out.write("<cols>");
        	float[] colWidths = ExcelHelper.calculateColumnWidths(data);	
        	for(int i = 0; i < colWidths.length; i++) {
        		_out.write("<col min=\"" + (i + 1) + "\" max=\"" + (i +1) + "\" width=\"" + colWidths[i] + "\" customWidth=\"true\" bestFit=\"true\"/>");
        	}
        	_out.write("</cols>");
            
        	_out.write("<sheetData>");
        }

        
        public void endSheet(ArrayList<String[]> data) throws Exception {
            _out.write("</sheetData>");
            
            if (data.size() > 1) {
	            String end = new CellReference((data.size() - 1), (data.get(1).length - 1)).formatAsString();
	            _out.write("<autoFilter ref=\"A9:" + end + "\" />");
            }
            
            //_out.write("<drawing r:id=\"rId1\"/>");
            
            _out.write("</worksheet>");
        }

        
        public void insertRow(int rownum) throws IOException {
            _out.write("<row r=\"" + (rownum + 1) + "\">");
            this._rownum = rownum;
        }


        public void endRow() throws IOException {
            _out.write("</row>");
        }


        public void createCell(int columnIndex, String value, int styleIndex) throws IOException {
            String ref = new CellReference(_rownum, columnIndex).formatAsString();
            _out.write("<c r=\"" + ref + "\" t=\"inlineStr\"");
            if(styleIndex != -1) {
            	_out.write(" s=\"" + styleIndex + "\"");
            }
            _out.write(">");
            _out.write("<is><t>" + value + "</t></is>");
            _out.write("</c>");
        }

        
        public void createCell(int columnIndex, String value) throws IOException {
            createCell(columnIndex, value, -1);
        }

        
        public void createCell(int columnIndex, double value, int styleIndex) throws IOException {
            String ref = new CellReference(_rownum, columnIndex).formatAsString();
            _out.write("<c r=\"" + ref + "\" t=\"n\"");
            if(styleIndex != -1) { 
            	_out.write(" s=\"" + styleIndex + "\"");
            }
            _out.write(">");
            _out.write("<v>" + value + "</v>");
            _out.write("</c>");
        }

        
        public void createCell(int columnIndex, double value) throws IOException {
            createCell(columnIndex, value, -1);
        }

        
        public void createCell(int columnIndex, Calendar value, int styleIndex) throws IOException {
            createCell(columnIndex, DateUtil.getExcelDate(value, false), styleIndex);
        }
        
        
        public void writeCustom(String custom) throws IOException {
        	_out.write(custom);
        }
        
        
    }
    
    
    
//==========================================================================================================
    
	public static void log(String msg) throws Exception {
		
		String logPath = path + "/application/logs/java.log";
		
	    Calendar c = new GregorianCalendar();
	    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    String now = format.format(c.getTime());
		
	    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(logPath, true)));
	    out.println(now + "  " + msg);
	    out.close();
    	
	}
	
	
	
    //Query with params function
	public static ArrayList<String[]> query(String q, Map<String, Object> params) throws Exception {
    
		ArrayList<String[]> result = new ArrayList<String[]>();
		
    	NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);

    	MapSqlParameterSource args = new MapSqlParameterSource();
    	
    	for(Map.Entry<String, Object> entry : params.entrySet()) {
    	    String key = entry.getKey();
    	    Object value = entry.getValue();
    		args.addValue(key, value);
    	}
    	
    	SqlRowSet rs = jdbcTemplate.queryForRowSet(q, args);
    	
    	SqlRowSetMetaData meta = rs.getMetaData();
    	int colCount = meta.getColumnCount();
    	
		while(rs.next()) {
			String[] row = new String[colCount];
			for(int i = 0; i < colCount; i++) {
				row[i] = rs.getString(i + 1);
			}
			result.add(row);
		}
		
    	return result;
    	
    }
    
	
	
	//Query without params function
	public static ArrayList<String[]> query(String q) throws Exception {
	    
		ArrayList<String[]> result = new ArrayList<String[]>();
		
    	JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

    	SqlRowSet rs = jdbcTemplate.queryForRowSet(q);
    	
    	SqlRowSetMetaData meta = rs.getMetaData();
    	int colCount = meta.getColumnCount();
    	
		while (rs.next()) {
			String[] row = new String[colCount];
			for (int i = 0; i < colCount; i++) {
				row[i] = rs.getString(i + 1);
			}
			result.add(row);
		}
		
    	return result;
    	
    }

	
	
    //Query with params function, data source passed
	public static ArrayList<String[]> query(String q, Map<String, Object> params, BasicDataSource src) throws Exception {
    
		ArrayList<String[]> result = new ArrayList<String[]>();
		
    	NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(src);

    	MapSqlParameterSource args = new MapSqlParameterSource();
    	
    	for (Map.Entry<String, Object> entry : params.entrySet()) {
    	    String key = entry.getKey();
    	    Object value = entry.getValue();
    		args.addValue(key, value);
    	}
    	
    	SqlRowSet rs = jdbcTemplate.queryForRowSet(q, args);
    	
    	SqlRowSetMetaData meta = rs.getMetaData();
    	int colCount = meta.getColumnCount();
    	
		while (rs.next()) {
			String[] row = new String[colCount];
			for (int i = 0; i < colCount; i++) {
				row[i] = rs.getString(i + 1);
			}
			result.add(row);
		}
		
    	return result;
    	
    }
    
	
	
	//Query without params function, data source passed
	public static ArrayList<String[]> query(String q, BasicDataSource src) throws Exception {
	    
		ArrayList<String[]> result = new ArrayList<String[]>();
		
    	JdbcTemplate jdbcTemplate = new JdbcTemplate(src);

    	SqlRowSet rs = jdbcTemplate.queryForRowSet(q);
    	
    	SqlRowSetMetaData meta = rs.getMetaData();
    	int colCount = meta.getColumnCount();
    	
		while(rs.next()) {
			String[] row = new String[colCount];
			for(int i = 0; i < colCount; i++) {
				row[i] = rs.getString(i + 1);
			}
			result.add(row);
		}
		
    	return result;
    	
    }

	
	
	public static String stripControlChars(String iString) {
	    StringBuffer result = new StringBuffer(iString);
	    int idx = result.length();
	    while (idx-- > 0) {
	        if(result.charAt(idx) < 0x20 && result.charAt(idx) != 0x9 && result.charAt(idx) != 0xA && result.charAt(idx) != 0xD) {
	            result.deleteCharAt(idx);
	        }
	    }
	    return result.toString();
	}	
	
	
    
}
