<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
      html { height: 100% }
      body { height: 100%; margin: 0; padding: 0 }
      #map_canvas { height: 100% }
    </style>
    <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDbo4DQ9GBDfTbkUecAbKPu76D1eIkQmrQ&sensor=false">
    </script>
    <script type="text/javascript">
        
      function initialize() {
      var multimapBaseLink = "";
      var geocoder = new google.maps.Geocoder();

	
	var setLat = 52.239;
	var setLon = -0.881384;   
        var mapOptions = {
          center: mylatlong=new google.maps.LatLng(setLat, setLon),
          zoom: 8,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map_canvas"),
            mapOptions);
            
            
            
            google.maps.event.addListener(map, 'click', function() {
  
    
    var marker = new map.Marker({
    position: mylatlong,
    title:"Hello World!"
});
  
   // alert('dd');
    
    
  });
  
      }
      
      var baseLink = "";
	
      function placeMarker(setLat, setLon) {
	
		var message = "geotagged geo:lat=" + setLat + " geo:lon=" + setLon + " "; 
		document.getElementById("geocode").value = message;
		var messageRoboGEO = setLat + ";" + setLon + ""; 
		document.getElementById("geocodeRoboGEO").value = messageRoboGEO;
	  
		document.getElementById("geocode").focus();
		document.getElementById("geocode").select();

		document.getElementById("maplink").href = baseLink + "?lat=" + setLat + "&lon=" + setLon ;
		document.getElementById("multimap").href = multimapBaseLink + "&lat=" + setLat + "&lon=" + setLon ;
		document.getElementById("frmLat").value = setLat;
		document.getElementById("frmLon").value = setLon;
	  
		//var map = new GMap(document.getElementById("map"));
		
		//map.addControl(new GSmallMapControl()); // added
		//map.addControl(new GMapTypeControl()); // added
		//map.centerAndZoom(new GPoint(setLon, setLat), 12);
		
		var point = new GPoint(setLon, setLat);
		
		map.addOverlay(marker);
                
		map.event.addListener(marker, 'click', function() {
                alert('d');
			if (overlay) {
				map.removeOverlay(overlay);
			} else if (point) {
				map.recenterOrPanToLatLng(point);
				var marker = new GMarker(point);
				map.addOverlay(marker);
				var matchll = /\(([-.\d]*), ([-.\d]*)/.exec( point );
				if ( matchll ) { 
					var lat = parseFloat( matchll[1] );
					var lon = parseFloat( matchll[2] );
					lat = lat.toFixed(6);
					lon = lon.toFixed(6);
					var message = "geotagged geo:lat=" + lat + " geo:lon=" + lon + " "; 
					var messageRoboGEO = lat + ";" + lon + ""; 
				} else { 
					var message = "<b>Error extracting info from</b>:" + point + ""; 
					var messagRoboGEO = message;
				}

				marker.openInfoWindowHtml(message);
				document.getElementById("geocode").value = message;
				document.getElementById("geocodeRoboGEO").value = messageRoboGEO;
				document.getElementById("geocode").focus();
				document.getElementById("geocode").select();

				document.getElementById("maplink").href = baseLink + "?lat=" + lat + "&lon=" + lon ;
				document.getElementById("multimap").href = multimapBaseLink + "&lat=" + lat + "&lon=" + lon ;
				document.getElementById("frmLat").value = lat;
				document.getElementById("frmLon").value = lon;

			}
		});
	}
    </script>
  </head>
  <body onload="initialize()">
    <div id="map_canvas" style="width:500px; height:500px"></div>
    	<script src="https://maps.google.com/maps/api/js?key=AIzaSyDbo4DQ9GBDfTbkUecAbKPu76D1eIkQmrQ&sensor=false" type="text/javascript"></script>-->
		<title>Google Maps Latitude, Longitude Popup</title>
	</head>
	<body>
		<h1>Google Maps Latitude, Longitude Popup</h1>
		<div style="width: 600px;" class="tekst"><b>Simply click  on the map on a location and it will provide you with the latitude and longitude in the callout window.</b></div>
		<div id="map" style="width: 600px; height: 400px"></div>
		<div id="geo" style="width: 300px;position: absolute;left: 620px;top: 100px;" class="tekst">
		<form name="setLatLon" action="googleMapLocation.php">
			<b>* Coordinates:</b><br />
			<table>
				<tr><td>* Lat:</td><td><input type='text' name='lat' id="frmLat"></td></tr>
				<tr><td>* Lon:</td><td><input type='text' name='lon' id="frmLon"></td></tr>
			</table>
			<input type="submit" name="setLatLon" value="Set"><br />
		</form><br />
        <b>* Flickr tags:</b><br />
		<textarea id="geocode" cols="30" rows="2"></textarea><br />
		<br />
        <b>* RoboGEO tags:</b><br />
		<textarea id="geocodeRoboGEO" cols="30" rows="2"></textarea><br />
		* <a href="#" target="_blank" id="multimap">Show location on Multimap</a><br />
		* <a href="#" id="maplink">Permanent Link</a><br /><br />
		
		
	</div>
	<div style="width: 600px;" class="smalltekst">
		
		
	</div>
  </body>
</html>