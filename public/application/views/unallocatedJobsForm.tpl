{if $cancelJobFormFlag}     
    
    
    <div id="UnallocatedJobsResultsPanel" class="SystemAdminFormPanel" >

                    <form id="UnallocatedJobsForm" name="UnallocatedJobsForm" method="post"  action="#" class="inline">

                    <fieldset>
                        <legend title="" >{$form_legend|escape:'html'}</legend>

                            

                            <p>
                                <br>
                                   
                                    {$page['Text']['cancel_confirm_question']|escape:'html'}
                           

                                    <input type="hidden" name="JobID"  value="{$JobID|escape:'html'}" >
                                    
                                    &nbsp;&nbsp;
                                    <input type="submit" name="cancel_job_btn" class="btnConfirm" id="cancel_job_btn"  value="{$page['Buttons']['yes_button']|escape:'html'}" >
                                    &nbsp;&nbsp;                                   
                                    <input type="submit" name="cancel_btn" class="btnCancel" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >

                                    <br>

                                

                            </p>







                    </fieldset>    

                    </form>        


    </div>
    

{else}
<script type="text/javascript" >   
          
          //This function assigns Service Provider details from popup search from. -- starts here....
            function assignServiceProvider ($rowData)
            {

                    if($rowData[0])
                    {
                       
                       
                       
                       if(confirm("{$JobBrandName|escape:'html'} Job No. SL{$JobID|escape:'html'} will be allocated to "+$rowData[1]+".\n\nDo you wish to proceed?"))
                       {     
                        $.colorbox({ 
                                                
                                html:$('#waitDiv').html(), 
                                title:"",
                                width:300,
                                height:300,
                                escKey: false,
                                overlayClose: false,
                                onLoad: function() {
                                    $('#cboxClose').hide();
                                }

                        });    
                            

                         $.post("{$_subdomain}/JobAllocation/ProcessData/Job/assignServiceCentre/"+urlencode($rowData[0])+"/{$JobID|escape:'html'}/",         
                           '',      
                           function(data){
                           
                                    var statusMsg  = eval("(" + data + ")");
                                   if(statusMsg['status']=="SUCCESS")
                                   {    
                                       
                                       
                                       $.colorbox({
                                        title: "{$page['Text']['allocate_page_legend']|escape:'html'}",
                                        html:   "{$page['Text']['data_updated_msg']|escape:'html'}",
                                        fixed:true,
                                    // height:options['popUpFormHeight'],
                                        width:750,
                                        onLoad: function() {
                                            $('#cboxClose').show();
                                        },
                                        onClosed:function(){ window.location.reload(true); } 
                                        }); 
                                   }

                           }); 
                           
                        }   

                    }
                    
                    return false;

            }
           //This function assigns Service Provider details from popup search from. -- ends here..      
        
    
            /**
            *  This is used to change preference column value..
            */
            function preferenceRow(nRow, aData)
            {

                
                
                if (aData[4]!='' && aData[4])
                {  
                    $('td:eq(4)', nRow).html( 'Primary' );
                }
                else
                {
                   $('td:eq(4)', nRow).html( '' );
                }

            }
          
          
          
           $(document).ready(function() {                 
            $('#ServiceProvidersResults'+"{$tableRandNumber}").PCCSDataTable( {

                    displayButtons: "P",
                    "aaSorting": [[ 4, "desc" ]],
                    "aoColumns": [ 

                            /* ServiceProviderID */  { "bSortable": false },
                            /* Service Provider Name */  null,
                            /* Town */ null,
                            /* Phone */ null,
                            /* preference */  { "asSorting": [ "desc" ] }

                    ],
                    searchCloseImage:   '',
                    htmlTablePageId:    'ServiceProvidersPage',
                    htmlTableId:        'ServiceProvidersResults'+"{$tableRandNumber}",
                    pickButtonId:        'pickButtonId_sp',
                    pickCallbackMethod: 'assignServiceProvider',
                    dblclickCallbackMethod: 'assignServiceProvider',
                    tooltipTitle: "{$page['Text']['sp_tooltip_title']|escape:'html'}",
                    colorboxForceClose: false,
                    formViewButton:     'formViewButtonSP',
                    colorboxFormId:     'colorboxFormIdSP',
                    fetchDataUrl:       '{$_subdomain}/Data/getServiceProvidersDTList/'+urlencode($("#NetworkID").val())+"/1/2/{$PostCode|escape:'html'}/"+ Math.random(),
                    popUpFormWidth:     0, //Put 0 for default width
                    popUpFormHeight:    0, //Put 0 for default height
                    frmErrorRules: {                },
                    frmErrorMessages : { },
                    bServerSide: false,
                    fnRowCallback:   'preferenceRow',
                    sDom: 'ft<"#dataTables_command'+"{$tableRandNumber}"+'">rpli',
                    bottomButtonsDivId:'dataTables_command'+"{$tableRandNumber}"
                    

                }); 

                

                });
         </script>       

         <div  id="UnallocatedJobsResultsPanel" class="ServiceProvidersPage" style="width:840px;" >   

                <form id="UnallocatedJobsForm" name="UnallocatedJobsForm" method="post"  action="#" class="inline" >

                   <fieldset>
                    <legend> {$page['Text']['allocate_page_legend']|escape:'html'} </legend>

                         <table id="ServiceProvidersResults{$tableRandNumber}" border="0" cellpadding="0" cellspacing="0" class="browse">
                        <thead>
                                <tr>
                                        <th width="5%" >{$page['Text']['id']|escape:'html'}</th>
                                        <th width="60%"  >{$page['Text']['service_provider_name']|escape:'html'}</th>
                                        <th width="15%" >{$page['Text']['town']|escape:'html'}</th>
                                        <th width="15%" >{$page['Text']['phone']|escape:'html'}</th>
                                        <th width="5%" >{$page['Text']['preference']|escape:'html'}</th>
                                </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        </table>  


                    </fieldset>
                    <input type="hidden" name="NetworkID" id="NetworkID" value="{$NetworkID|escape:'html'}" >
                    
                </form>
         </div> 
    
                 
{/if}
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    
<div id="waitDiv" style="display:none;text-align:center;">
    <img src="{$_subdomain}/images/processing.gif">
</div>
                        
                        
                        
    
 