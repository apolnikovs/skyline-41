{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $PartFaultCodesLookupsPage}
    {$fullscreen=true}
{/block}
{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script>
    <script src="{$_subdomain}/js/jquery.multiselect.js" type="text/javascript"></script>
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" />
     <link href="{$_subdomain}/css/themes/pccs/jquery.multiselect.css" rel="stylesheet" type="text/css" />
{/block}

{block name=scripts}


   
 <script type="text/javascript" src="{$_subdomain}/js/ColReorder.js"></script>
    <script type="text/javascript">
         var $statuses = [
                    {foreach from=$statuses item=st}
                       ["{$st.Name}", "{$st.Code}"],
                    {/foreach}
                    ]; 
     
                 
    
                    
        
    function gotoEditPage($sRow)
    {
        
         
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
       
       
    }

    
    
    function showTablePreferences(){
$.colorbox({ 
 
                       href:"{$_subdomain}/LookupTables/tableDisplayPreferenceSetup/page=partFaultCodesLookups/table=part_fault_code_lookup",
                        title: "Table Display Preferences",
                        opacity: 0.75,
                        overlayClose: false,
                        escKey: false

                });
}
      
 $(document).ready(function() {

 var oTable = $('#PartFaultCodesLookupsResults').dataTable( {
"sDom": 'R<"left"><"top"f>t<"bottom"><"centered"><"right">pli<"clear">',
//"sDom": 'Rlfrtip',

"bServerSide": true,

		fnRowCallback:  function(nRow, aData){
               
                
                },
    "sAjaxSource": "{$_subdomain}/LookupTables/loadPartFaultCodesLookupsTable",
     
                "fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( { "name": "more_data", "value": "my_value" } );
			$.getJSON( sSource, aoData, function (json) { 
				/* Do whatever additional processing you want on the callback, then tell DataTables */
				fnCallback(json)
                                $('#tt-Loader').hide();
                                $('#PartFaultCodesLookupsResults').show();
                               
			} );
                        },
                        
"bPaginate": true,
"sPaginationType": "full_numbers",
"aLengthMenu": [[ 25, 50, 100 , -1], [25, 50, 100, "All"]],
"iDisplayLength" : 25,
 "aoColumns": [ 
			
			
			{for $er=0 to $data_keys|@count-1}
                                {$vis=1}
                               
                               
                            {if $er==0}{$vis=0}{/if}
                            { "bVisible":{$vis} },
			 
                           {/for} 
                               { "bVisible":1,"bSortable":false }
                               
                            
		] 
   
 
        
          
});//datatable end
  
   /* Add a click handler to the rows - this could be used as a callback */
	$("#PartFaultCodesLookupsResults tbody").click(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
                var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
                
    if (anSelected!="")  // null if we clicked on title row
    {
    
                $('#exportType').val(aData[0]);
                }
	});

 /* Get the rows which are currently selected */
function fnGetSelected( oTableLocal )
{
	var aReturn = new Array();
	var aTrs = oTableLocal.fnGetNodes();
	
	for ( var i=0 ; i<aTrs.length ; i++ )
	{
		if ( $(aTrs[i]).hasClass('row_selected') )
		{
			aReturn.push( aTrs[i] );
		}
	}
	return aReturn;
}



    /* Add a click handler for the edit row */
	$('button[id^=edit]').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
        console.log(anSelected);
  //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
$.colorbox({ 
 
                           href:"{$_subdomain}/LookupTables/processPartFaultCodesLookups/id="+anSelected[0].id,
                        title: "Edit PartFaultCodesLookups",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });
    }else{
    alert("Please select row first");
    }
		
	} );                            
             
 /* Add a click handler for the delete row */
	$('button[id^=delete]').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
  //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
if (confirm('Are you sure you want to delete this entry from database?')) {
    window.location="{$_subdomain}/LookupTables/deletePartFaultCodesLookups/id="+anSelected[0].id
} else {
    // Do nothing!
}


    }else{
    alert("Please select row first");
    }
		
	} );                            
             

/* Add a dblclick handler to the rows - this could be used as a callback */
	$("#PartFaultCodesLookupsResults  tbody").dblclick(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
            var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
    
$.colorbox({ 
 
                        href:"{$_subdomain}/LookupTables/processPartFaultCodesLookups/id="+anSelected[0].id,
                        title: "Edit Part Fault Codes",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });
    }else{
    alert("Please select row first");
    }
		
	} );  

$('#serviceProviderSelect').change( function() {
        
        if($(this).val()!="0"){
                 $('#tt-Loader').show();
                                $('#PartFaultCodesLookupsResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/LookupTables/loadPartFaultCodesLookupsTable/supplierid="+$(this).val()+"/");
                 }else{
                  $('#tt-Loader').show();
                                $('#PartFaultCodesLookupsResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/LookupTables/loadPartFaultCodesLookupsTable/");
                 }
	} );  
$('input[id^=inactivetick]').click( function() {
         $('#unaprovedtick').attr("checked",false);
        if($(this).attr("checked")=="checked"){
                 $('#tt-Loader').show();
                                $('#SuppliersResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/LookupTables/loadPartFaultCodesLookupsTable/inactive=1/");
                 }else{
                  $('#tt-Loader').show();
                                $('#SuppliersResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/LookupTables/loadPartFaultCodesLookupsTable/");
                 }
	} );  

    });//doc ready end
function PartFaultCodesLookupsInsert()
{
$.colorbox({ 
 
                        href:"{$_subdomain}/LookupTables/processPartFaultCodesLookups/",
                        title: "Insert Part Fault Codes",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });

}
function PartFaultCodesLookupsEdit()
{
    var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); 
               
$.colorbox({ 
 
                        href:"{$_subdomain}/LookupTables/processPartFaultCodesLookups/id="+aData,
                        title: "Edit Part Fault Codes",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });

}


                    
                    
                
                
             
function filterTable(name){
$('input[type="text"]','#PartFaultCodesLookupsResults_filter').val(name);
e = jQuery.Event("keyup");
e.which = 13;
$('input[type="text"]','#PartFaultCodesLookupsResults_filter').trigger(e);


}

    </script>

    
{/block}


{block name=body}
<div style="float:right">
         <a href="#" onclick="showTablePreferences();">Display Preferences</a>
            </div>
    <div class="breadcrumb" style="width:100%">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/lookupTables" >Lookup Tables</a> {if isset($backtomodel)}/<a href="{$_subdomain}SystemAdmin/index/lookupTables/models" > Models</a> {/if}/ Part Fault Codes Lookups

        </div>
    </div>



    <div class="main" id="home" style="width:100%">

               <div class="ServiceAdminTopPanel" >
                    <form id="PartFaultCodesLookupsTopForm" name="PartFaultCodesLookupsTopForm" method="post"  action="#" class="inline">
                         
                        <fieldset>
                        <legend title="" >Part Fault Codes Lookups</legend>
                        <p>
                            <label>Part Fault Codes are the selectable options that appear when entering part fault codes on a warranty claim.</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  


                  <div class="ServiceAdminResultsPanel" id="PartFaultCodesLookupsResultsPanel" >
                    
                  
                 
                
                     <div style="text-align:center" id="tt-Loader"><img src="{$_subdomain}/images/ajax-loading_medium.gif"></div>
                      
                      {if isset($splist)}
                        <select id="serviceProviderSelect" style="float:left;margin-left:20px;padding: 4px;position:relative;top:-2px">
                            <option value="0">Please  select Service Provider</option>
                        {foreach $splist as $s}
                            <option value="{$s.ServiceProviderID}">{$s.Acronym}</option>
                        {/foreach}
                        </select>
                        {/if}
                    <form method="POST" action="" id="PartFaultCodesLookupsResultsForm" class="dataTableCorrections">
                        <table style="display:none" id="PartFaultCodesLookupsResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                            <thead>
                                    <tr>
                                            
                                        {foreach from=$data_keys key=kk item=vv}
                                  
                                    <th>
                                        {$vv}
                                    </th>
                                  
                                {/foreach}
                           
                                <th style="width:10px;text-align: center"><input type="checkbox"></th>
                                    </tr>
                            </thead>
                            <tbody>
                              
                            
                            </tbody>
                        </table>  
                           
                           
                     </form>
                </div>        

                  <div class="bottomButtonsPanelHolder" style="position: relative;">
                    <div class="bottomButtonsPanel" style="position:absolute;top:-65px;width:200px">
                            <button type="button" id="edit"  class="gplus-blue">Edit</button>
                            <button type="button" onclick="PartFaultCodesLookupsInsert()" class="gplus-blue">Insert</button>
                            <button type="button" id="delete" class="gplus-red">Delete</button>
                    </div>
                </div>  
                 <div class="centerInfoText" id="centerInfoText" style="display:none;" ></div>
               

            <div style="width:100%;text-align: right">
                        <input id="inactivetick"  type="checkbox" > Show Inactive&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                   
                    </div>
               

<hr>
                <button class="gplus-blue" style="float:right" type="button" onclick="window.location='{$_subdomain}/SystemAdmin/index/lookupTables'">Finish</button>
    
    </div>
                 



{/block}



