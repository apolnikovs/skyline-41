<?php

require_once('CustomSmartyController.class.php');

/**
 * Description
 *
 * This class handles all actions of Lookup Tables under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.35
 * 
 * Changes
 * Date        Version Author                 Reason
 * ??/??/2012  1.00    Nageswara Rao Kanteti  Initial Version  
 * 13/04/2012  1.01    Nageswara Rao Kanteti  Created Service Types Page, Some bugs fixed in Data Table and Part of Service Network 
 * 11/05/2012  1.02    Nageswara Rao Kanteti  Product Setup pages (except unit type page new changes)
 * 16/05/2012  1.03    Nageswara Rao Kanteti  3) Finished Product Setup and Organisation Setup Pages
 * 18/05/2012  1.04    Nageswara Rao Kanteti  Applied brand filtering on country and county lookup tables 
 * 21/05/2012  1.05    Simon Tsang	      system user section
 * 27/06/2012  1.06    Nageswara Rao Kanteti  New design changes have been done except open/over due jobs pages, and also test functionality has been done to find service centre on job booking page.
 * 06/07/2012  1.07    Nageswara Rao Kanteti  Job allocation pages have been done by Nag
 * 06/08/2012  1.08    Nageswara Rao Kanteti  Joe's Skyline Test Report 3 has been done
 * 13/08/2012  1.09    Nageswara Rao Kanteti  Service Provider selection done on job booking process.
 * 15/08/2012  1.10    Nageswara Rao Kanteti  Part of Skyline Test Report 6 is done.
 * 12/09/2012  1.11    Nageswara Rao Kanteti  101 Skyline Test Report  - done upto 35 pages.
 * 13/09/2012  1.12    Brian Etherington      Added skin selection to network, client, brand and service centre tables.
 * 21/09/2012  1.13    Nageswara Rao Kanteti  Appointment Diary: Diary – Skills & Postcode is done.
 * 22/10/2012  1.14    Brian Etherington      Replaced files updated in error by Andrew's merge
 * 24/10/2012  1.15    Nageswara Rao Kanteti  Ra statuses, service provider put settings calling and system status (part) - done
 * 29/10/2012  1.16    Nageswara Rao Kanteti  RA Document has been done except few issues which are need clarification from Joe
 * 08/11/2012  1.17    Vykintas Rutkunas      Reports work in progress
 * 13/11/2012  1.18    Nageswara Rao Kanteti  Job booking, open, overdue and RA changes are done
 * 25/11/2012  1.19    Nageswara Rao Kanteti  Courier task and RA enhancements are done.
 * 27/11/2012  1.20    Brian Etherington      Added Inactivity Timeout to User Role.  User's will be forcibly logged out if this inactivity timeout is exceeded.  Note: the inactivity timeout must be less than the configured PHP session garbage collection time. The inactivity timeout does not apply to super admin and is the minimum value of the users allocated roles if they have more than one.  The database is configured to allocate the default PHP garbage collection time of 24 minutes.
 * 18/12/2012  1.21    Vykintas Rutkunas      Changes to RA Status Types functionality
 * 05/02/2013  1.22    Brian Etherington      Update Colorbox  v1.3.34
 * 19/02/2013  1.23    Vykintas Rutkunas      System Admin Courier facility upgraded
 * 21/02/2013  1.24    Vykintas Rutkunas      Samsung secondary logo - urgent
 * 01/03/2013  1.25    Nageswara Rao Kanteti  Samsung Experience Enhancements all tasks are done which are assigned to Nag.
 * 11/03/2013  1.26    Nageswara Rao Kanteti  diary wallboard enhancements are done except permission part.
 * 18/03/2013  1.27    Nageswara Rao Kanteti  Bounces fixes in progress.
 * 22/03/2013  1.28    Nageswara Rao Kanteti  Samsung experience enhancements phase2 - In Progress 
 * 27/03/2013  1.29    Andrew J. Williams     Added System Status Permissions
 * 11/04/2013  1.30    Andris Polnikovs       Added Colour,Currency,Accessory,Product codes lookup tables
 * 11/04/2013  1.31    Andris Polnikovs       Added Part Order Status lookup tables
 * 01/05/2013  1.32    Brian Etherington      Added Packaging Types to Courier Form
 * 05/06/2013  1.33    Andris Polnikovs       Added Job Parts Codes and Job Parts Codes Lookups 
 * 11/06/2013  1.34    Andris Polnikovs       Added  Part Fault Codes and JPart Fault Codes Lookups 
 * 12/06/2013  1.35    Andris Polnikovs       Added Part and Shelf Locations 
 * 01/07/2013          Thirumalesh Bandi      Added page title and page description dynamically for every page.
 * 03/07/2013          Thirumalesh Bandi      Part Fault Codes.
 * 04/07/2013 & 05/07/2013         Thirumalesh Bandi      Part Fault Codes Lookups.
 * **************************************************************************** */
class LookupTablesController extends CustomSmartyController {

    public $config;
    public $session;
    public $skyline;
    public $messages;
    public $lang = 'en';
    public $page;   
    //public $manufacturersList;
    public $statuses = array(
        0 => array('Name' => 'Active', 'Code' => 'Active'),
        1 => array('Name' => 'In-Active', 'Code' => 'In-active')
    );
    public $SkylineBrandID = 1000;

    public function __construct() {

        parent::__construct();

        /* ==========================================
         * Read Application Config file.
         * ==========================================
         */

        $this->config = $this->readConfig('application.ini');

        /* ==========================================
         *  Initialise Session Model
         * ==========================================
         */

        $this->session = $this->loadModel('Session');

        /* ==========================================
         *  Initialise Messages Model
         * ==========================================
         */

        $this->messages = $this->loadModel('Messages');

        //$this->smarty->assign('_theme', 'skyline');

        if (isset($this->session->UserID)) {

            $user_model = $this->loadModel('Users');
            $this->user = $user_model->GetUser($this->session->UserID);
            $this->smarty->assign('loggedin_user', $this->user);

            //$this->log(var_export($this->user, true));

            $this->smarty->assign('name', $this->user->Name);
            $this->smarty->assign('session_user_id', $this->session->UserID);
            $this->smarty->assign('_theme', $this->user->Skin);

            if ($this->user->BranchName) {
                $this->smarty->assign('logged_in_branch_name', " - " . $this->user->BranchName . " " . $this->config['General']['Branch'] . " ");
            } else {
                $this->smarty->assign('logged_in_branch_name', "");
            }

            if ($this->session->LastLoggedIn) {
                $this->smarty->assign('last_logged_in', " - " . $this->config['General']['LastLoggedin'] . " " . date("jS F Y G:i", $this->session->LastLoggedIn));
            } else {
                $this->smarty->assign('last_logged_in', '');
            }

            $topLogoBrandID = $this->user->DefaultBrandID;
        } else {

            $topLogoBrandID = isset($_COOKIE['brand']) ? $_COOKIE['brand'] : 0;

            $this->smarty->assign('session_user_id', '');
            $this->smarty->assign('name', '');
            $this->smarty->assign('last_logged_in', '');
            $this->smarty->assign('logged_in_branch_name', '');
            $this->smarty->assign('_theme', 'skyline');
            $this->redirect('index', null, null);
        }

        if ($topLogoBrandID) {
            $skyline = $this->loadModel('Skyline');
            $topBrandLogo = $skyline->getBrand($topLogoBrandID);

            $this->smarty->assign('_brandLogo', (isset($topBrandLogo[0]['BrandLogo'])) ? $topBrandLogo[0]['BrandLogo'] : '');
        } else {
            $this->smarty->assign('_brandLogo', '');
        }

        $this->smarty->assign('showTopLogoBlankBox', false);

        if (isset($this->session->lang)) {
            $this->lang = $this->session->lang;
        }
    }
    
       //cheks if user got service provider id
    private function checkUserSPID($args=false) {
        //$this->print_d($this->user);
        if ($this->user->ServiceProviderID == "") {
            if ($this->user->SuperAdmin == 1) {
                $this->smarty->assign("SuperAdmin", true);
                $this->user->ServiceProviderID=0;
            } else {

                $this->smarty->assign('NotPermited', true);
                $this->smarty->assign('type', "error");
                $this->smarty->assign('msgTitle', $this->page['Errors']['error']);
                $this->smarty->assign('msgText', $this->page['Errors']['no_service_provider_id_error']);
                $this->smarty->display("popup/UserMessage.tpl");
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * Description
     * 
     * This method is used to process the data i.e fething, updating and inserting.
     * 
     * @param array $args It contains modelname as first element where second element as process type
     * @return void It prints json encoded result where datatable using this data.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
     public function ProcessDataAction($args) {
	 
	$modelName = isset($args[0]) ? $args[0] : "";
	$type = isset($args[1]) ? $args[1] : "";
         
	if($modelName && $modelName != "") {
	    
	    $model = $this->loadModel($modelName);
	    //$this->log($args);
	    if($type == "fetch") {
		$_POST["firstArg"] = isset($args[2]) ? $args[2] : "";
		$_POST["secondArg"] = isset($args[3]) ? $args[3] : "";
		$result = $model->fetch($_POST);
		
		
	    } else if($type == "delete") {
                if(isset($_POST['RepairTypeID']))
                    $result = $model->deleteRepairType($_POST['RepairTypeID']);
                else if(isset($_POST['JobFaultCodeID']))
                    $result = $model->deleteJobFaultCode($_POST['JobFaultCodeID']);
                else if(isset($_POST['JobFaultCodeLookupID']))
                    $result = $model->deleteJobFaultCodeLookup($_POST['JobFaultCodeLookupID']);
	    } else {
		$result = $model->processData($_POST); 
	    }    	    
	    echo json_encode( $result );	    
       }
       
	return;
	
    }
    
     
    
      public function indexAction( /*$args*/ ) { 
          
          
      }
      
      
      
       /**
     * Description
     * 
     * This method is used for to manuplate data of Audit Trail Actions Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
    public function auditTrailActionsAction($args) {


        $functionAction = isset($args[0]) ? $args[0] : '';
        $selectedRowId = isset($args[1]) ? $args[1] : '';



        $Skyline = $this->loadModel('Skyline');

        //If the action is insert then we are getting only skyline brand since user(i.e super admin) can create actions only for skyline (Of course others can copy this action if they want)
        if ($functionAction == "insert" && $selectedRowId == '') {
            $brands = $Skyline->getBrand(1000);
        } else if ($functionAction == "insert" && $selectedRowId != '') {
            $brands = $Skyline->getBrand('', 1000);
        } else { // Getting brands (logged in user can see brands which are assinged to him/her )...
            $brands = $Skyline->getBrand();
        }




        $this->smarty->assign('brands', $brands);
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
        $this->smarty->assign('userBrands', $this->user->Brands); //this value should be from user calss
        $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));

        $accessErrorFlag = false;


        $types = array(
            0 => array('Name' => 'Process'),
            1 => array('Name' => 'Action'),
            2 => array('Name' => 'Edit')
        );



        $this->page = $this->messages->getPage('auditTrailActions', $this->lang);
        
                //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('auditTrailActions');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];

        $this->smarty->assign('page', $this->page);


        $this->smarty->assign('types', $types);



        switch ($functionAction) {

            case 'insert':

                //We are fetching data for the selected row where user clicks on copy button
                $args['AuditTrailActionID'] = $selectedRowId;
                if ($args['AuditTrailActionID'] != '') {
                    $model = $this->loadModel('AuditTrailActions');
                    $datarow = $model->fetchRow($args);
                    $datarow['AuditTrailActionID'] = '';
                    $datarow['BrandID'] = '';
                    $datarow['Status'] = 'Active';
                } else {

                    $datarow = array('AuditTrailActionID' => '', 'ActionCode' => '', 'BrandID' => '', 'Action' => '', 'Type' => '', 'Status' => 'Active');
                }

                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else if (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {
                    if ($datarow['ActionCode'] == '') {
                        $accessErrorFlag = true;
                    }
                }

                $this->smarty->assign('datarow', $datarow);
                $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);



                $htmlCode = $this->smarty->fetch('auditTrailActionsForm.tpl');

                echo $htmlCode;

                break;


            case 'update':

                $args['AuditTrailActionID'] = $selectedRowId;
                $model = $this->loadModel('AuditTrailActions');

                $datarow = $model->fetchRow($args);

                //$this->log(var_export($datarow, true));
                $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);


                //If the super admin updating existing record of skyline he can not change this brand to other brand so we are only fetching skyline for the brand dropdown in the update page.
                if ($datarow['BrandID'] == 1000) {
                    $brands = $Skyline->getBrand(1000);
                    $this->smarty->assign('brands', $brands);
                }


                $this->smarty->assign('datarow', $datarow);

                // $this->log(var_export("test", true));
                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else if (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {
                    //$this->log(var_export($this->user->Permissions, true));

                    if (!isset($this->user->Brands[$datarow['BrandID']])) {
                        $accessErrorFlag = true;
                    }
                }
                // $this->log(var_export($accessErrorFlag, true));

                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);


                $htmlCode = $this->smarty->fetch('auditTrailActionsForm.tpl');

                echo $htmlCode;

                break;

            default:

                $this->smarty->display('auditTrailActions.tpl');
        }
    }

    /**
     * Description
     * 
     * This method is used for to manuplate data of Contact History Actions Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
    public function contactHistoryActionsAction($args) {


        $functionAction = isset($args[0]) ? $args[0] : '';
        $selectedRowId = isset($args[1]) ? $args[1] : '';



        $Skyline = $this->loadModel('Skyline');

        //If the action is insert then we are getting only skyline brand since user(i.e super admin) can create actions only for skyline (Of course others can copy this action if they want)
        if ($functionAction == "insert" && $selectedRowId == '') {
            $brands = $Skyline->getBrand(1000);
        } else if ($functionAction == "insert" && $selectedRowId != '') {
            $brands = $Skyline->getBrand('', 1000);
        } else { // Getting brands (logged in user can see brands which are assinged to him/her )...
            $brands = $Skyline->getBrand();
        }




        $this->smarty->assign('brands', $brands);
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
        $this->smarty->assign('userBrands', $this->user->Brands); //this value should be from user calss
        $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));

        $accessErrorFlag = false;



        $types = array(
            0 => array('Name' => 'User Defined', 'Code' => 'User Defined'),
            1 => array('Name' => 'System Defined', 'Code' => 'System Defined')
        );


        $this->page = $this->messages->getPage('contactHistoryActions', $this->lang);        
        
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('contactHistoryActions');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];

        
        
        $this->smarty->assign('page', $this->page);


        $this->smarty->assign('types', $types);


        switch ($functionAction) {

            case 'insert':

                //We are fetching data for the selected row where user clicks on copy button
                $args['ContactHistoryActionID'] = $selectedRowId;
                if ($args['ContactHistoryActionID'] != '') {
                    $model = $this->loadModel('ContactHistoryActions');
                    $datarow = $model->fetchRow($args);
                    $datarow['ContactHistoryActionID'] = '';
                    $datarow['BrandID'] = '';
                    $datarow['Status'] = 'Active';
                } else {

                    $datarow = array('ContactHistoryActionID' => '', 'ActionCode' => '', 'BrandID' => '', 'Action' => '', 'Type' => '', 'Status' => 'Active');
                }

                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else if (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {
                    if ($datarow['ActionCode'] == '') {
                        $accessErrorFlag = true;
                    }
                }

                $this->smarty->assign('datarow', $datarow);
                $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);



                $htmlCode = $this->smarty->fetch('contactHistoryActionsForm.tpl');

                echo $htmlCode;

                break;


            case 'update':


                $args['ContactHistoryActionID'] = $selectedRowId;
                $model = $this->loadModel('ContactHistoryActions');

                $datarow = $model->fetchRow($args);


                $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                //If the super admin updating existing record of skyline he can not change this brand to other brand so we are only fetching skyline for the brand dropdown in the update page.
                if ($datarow['BrandID'] == 1000) {
                    $brands = $Skyline->getBrand(1000);
                    $this->smarty->assign('brands', $brands);
                }


                $this->smarty->assign('datarow', $datarow);

                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else if (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {

                    if (!isset($this->user->Brands[$datarow['BrandID']])) {
                        $accessErrorFlag = true;
                    }
                }

                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);


                $htmlCode = $this->smarty->fetch('contactHistoryActionsForm.tpl');

                echo $htmlCode;

                break;

            default:

                $this->smarty->display('contactHistoryActions.tpl');
        }
    }

    /**
     * Description
     * 
     * This method is used for to manuplate data of Customer Titles Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */
    public function customerTitlesAction($args) {

        $functionAction = isset($args[0]) ? $args[0] : '';
        $selectedRowId = isset($args[1]) ? $args[1] : '';


        $Skyline = $this->loadModel('Skyline');

        //If the action is insert then we are getting only skyline brand since user(i.e super admin) can create actions only for skyline (Of course others can copy this action if they want)
        if ($functionAction == "insert" && $selectedRowId == '') {
            $brands = $Skyline->getBrand(1000);
        } else if ($functionAction == "insert" && $selectedRowId != '') {
            $brands = $Skyline->getBrand('', 1000);
        } else { // Getting brands (logged in user can see brands which are assinged to him/her )...
            $brands = $Skyline->getBrand();
        }




        $this->smarty->assign('brands', $brands);
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
        $this->smarty->assign('userBrands', $this->user->Brands); //this value should be from user calss
        $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));

        $accessErrorFlag = false;



        $types = array(
            0 => array('Name' => 'Male', 'Code' => 'Male'),
            1 => array('Name' => 'Female', 'Code' => 'Female'),
            2 => array('Name' => 'Unknown', 'Code' => 'Unknown')
        );


        $this->page = $this->messages->getPage('customerTitles', $this->lang);
        
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('customerTitles');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];

        
        
        $this->smarty->assign('page', $this->page);


        $this->smarty->assign('types', $types);


        switch ($functionAction) {

            case 'insert':

                //We are fetching data for the selected row where user clicks on copy button
                $args['CustomerTitleID'] = $selectedRowId;
                if ($args['CustomerTitleID'] != '') {
                    $model = $this->loadModel('CustomerTitles');
                    $datarow = $model->fetchRow($args);
                    $datarow['CustomerTitleID'] = '';
                    $datarow['BrandID'] = '';
                    $datarow['Status'] = 'Active';
                } else {

                    $datarow = array('CustomerTitleID' => '', 'TitleCode' => '', 'BrandID' => '', 'Title' => '', 'Type' => '', 'Status' => 'Active');
                }

                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else if (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {
                    if ($datarow['TitleCode'] == '') {
                        $accessErrorFlag = true;
                    }
                }

                $this->smarty->assign('datarow', $datarow);
                $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);



                $htmlCode = $this->smarty->fetch('customerTitlesForm.tpl');

                echo $htmlCode;

                break;


            case 'update':


                $args['CustomerTitleID'] = $selectedRowId;
                $model = $this->loadModel('CustomerTitles');

                $datarow = $model->fetchRow($args);


                $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                //If the super admin updating existing record of skyline he can not change this brand to other brand so we are only fetching skyline for the brand dropdown in the update page.
                if ($datarow['BrandID'] == 1000) {
                    $brands = $Skyline->getBrand(1000);
                    $this->smarty->assign('brands', $brands);
                }


                $this->smarty->assign('datarow', $datarow);

                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else if (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {

                    if (!isset($this->user->Brands[$datarow['BrandID']])) {
                        $accessErrorFlag = true;
                    }
                }

                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);


                $htmlCode = $this->smarty->fetch('customerTitlesForm.tpl');

                echo $htmlCode;

                break;

            default:

                $this->smarty->display('customerTitles.tpl');
        }
    }

    /**
     * Description
     * 
     * This method is used for to manuplate data of County Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
    public function countyAction($args) {


        $functionAction = isset($args[0]) ? $args[0] : '';
        $selectedRowId = isset($args[1]) ? $args[1] : '';



        $Skyline = $this->loadModel('Skyline');

        //If the action is insert then we are getting only skyline brand since user(i.e super admin) can create actions only for skyline (Of course others can copy this action if they want)
        if ($functionAction == "insert" && $selectedRowId == '') {
            $brands = $Skyline->getBrand(1000);
        } else if ($functionAction == "insert" && $selectedRowId != '') {
            $brands = $Skyline->getBrand('', 1000);
        } else { // Getting brands (logged in user can see brands which are assinged to him/her )...
            $brands = $Skyline->getBrand();
        }



        $this->smarty->assign('brands', $brands);
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
        $this->smarty->assign('userBrands', $this->user->Brands); //this value should be from user calss
        $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));

        $accessErrorFlag = false;


        $this->page = $this->messages->getPage('county', $this->lang);
        
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('county');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];

        
        $this->smarty->assign('page', $this->page);


        $allBrandsCountries = $Skyline->getBrandsCountries();
        $this->smarty->assign('allBrandsCountries', $allBrandsCountries);



        switch ($functionAction) {

            case 'insert':

                //We are fetching data for the selected row where user clicks on copy button
                $args['CountyID'] = $selectedRowId;
                if ($args['CountyID'] != '') {
                    $model = $this->loadModel('County');
                    $datarow = $model->fetchRow($args);
                    $datarow['CountyID'] = '';
                    $datarow['BrandID'] = '';
                    $datarow['Status'] = 'Active';
                } else {

                    $datarow = array('CountyID' => '', 'CountyCode' => '', 'BrandID' => '', 'CountryID' => '', 'Name' => '', 'Status' => 'Active');
                }

                $countries = array();


                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else if (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {
                    if ($datarow['CountyCode'] == '') {
                        $accessErrorFlag = true;
                    }
                }

                $this->smarty->assign('datarow', $datarow);
                $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
                $this->smarty->assign('countries', $countries);


                $htmlCode = $this->smarty->fetch('countyForm.tpl');

                echo $htmlCode;

                break;


            case 'update':


                $args['CountyID'] = $selectedRowId;
                $model = $this->loadModel('County');

                $datarow = $model->fetchRow($args);


                $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                //If the super admin updating existing record of skyline he can not change this brand to other brand so we are only fetching skyline for the brand dropdown in the update page.
                if ($datarow['BrandID'] == 1000) {
                    $brands = $Skyline->getBrand(1000);
                    $this->smarty->assign('brands', $brands);
                }

                $this->smarty->assign('datarow', $datarow);

                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else if (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {

                    if (!isset($this->user->Brands[$datarow['BrandID']])) {
                        $accessErrorFlag = true;
                    }
                }


                if ($datarow['BrandID'] && isset($allBrandsCountries[$datarow['BrandID']])) {
                    $countries = $allBrandsCountries[$datarow['BrandID']];
                } else {
                    $countries = array();
                }

                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
                $this->smarty->assign('countries', $countries);

                $htmlCode = $this->smarty->fetch('countyForm.tpl');

                echo $htmlCode;

                break;

            default:

                $this->smarty->display('county.tpl');
        }
    }



     /**
     * satisfactionary Questionnaires      
     *          
     * Operations                              
     * Inseting New Questionniar to Brand
     * View the Select Questionnair
     * Report which is linked in sitemap page
     * generate Reports as per selected Questinnair Brands
     * Default View list all Questionnaire respective Brands  
     * Tracker log 357 
     * 
     * @author v srinivasa rao <s.vempati@pccsuk.com>
     **************************************************************************/   
     
    public function satisfactionQuestionnaireAction($args) {


        $functionAction = isset($args[0]) ? $args[0] : '';
        $selectedRowId = isset($args[1]) ? $args[1] : '';
        $bId           = isset($args[0]) ? $args[0] : ''; 

        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
        $this->smarty->assign('userBrands', $this->user->Brands); //this value should be from user calss
        $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));

        $accessErrorFlag = false;

        
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('satisfactionQuestionnaire');

        // Over write the page title & description
        
        
        $this->page = $this->messages->getPage('satisfactionQuestionnaire', $this->lang);
        
       $this->page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$this->page['Text']['page_title'];
       $this->page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$this->page['Text']['description'];
          
        
        $this->smarty->assign('page', $this->page);
        
        //SRINVAS :: GETTING TOTAL BRANDS 
        $Skyline              = $this->loadModel('Skyline');
        $brands               = array();   
        $brands               = $Skyline->getBrands($this->user->NetworkID,$this->user->ClientID,$this->user->BranchID,$this->user->ManufacturerID,$this->user->ServiceProviderID );
        $this->smarty->assign('brands', $brands);
        
        // assain brand id
         $this->smarty->assign('bId', $bId);
        //END     

        switch ($functionAction) {

            case 'insert':
              
                $datarow = array(
                    'ease_of_booking' => 'Ease of booking', 
                    'speed_of_service_first_visit' => 'Speed of service (to first visit)', 
                    'speed_of_service_overall' => 'Speed of service (overall)', 
                    'communication' => 'Communication throughout', 
                    'engineer' => 'Engineer (where relevant)', 
                    'Who_suggested_our_services_to_you' => 'Retailer|Insurer/Extended Warrantor|Advertisement|Manufacturer|Web Search|Personal Recommendation'
                    );
  
    
                $bId           = isset($args[1]) ? $args[1] : '';
                $this->smarty->assign('bId', $bId);
                //Allowing all user to access the Satisfaction Questionaires.
                $accessErrorFlag = false;
                
                $this->smarty->assign('brands', $brands);
                $this->smarty->assign('datarow', $datarow);
                $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
                //$this->smarty->assign('countries', $countries);


                $htmlCode = $this->smarty->fetch('SatisfactionQuestionnaireForm.tpl');

                echo $htmlCode;

                break;

             case 'view': 
                $Questionnaire         = $this->loadModel('Questionnaire');
                $datarow               = array();   
                $datarow               = $Questionnaire->getQuestionnaireAttribute($selectedRowId ); 
                
                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else if (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {
                   
                }

                $this->smarty->assign('datarow', $datarow);
                $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
                //$this->smarty->assign('countries', $countries);


                $htmlCode = $this->smarty->fetch('SatisfactionQuestionnaireView.tpl');

                echo $htmlCode;

                break;
                
            case 'report' :
                $bId           = isset($args[1]) ? $args[1] : ''; 
                $this->smarty->assign('bId', $bId);
                $this->smarty->display('SatisfactionQuestionnaireReport.tpl');
                break;
           
              case 'genreport' :
                $list                  = isset($args[1]) ? $args[1] : '';                    
                if($list==''){echo "<script>alert('No Questionnair Selected !');window.close();</script>";exit; }  
                $Questionnaire         = $this->loadModel('Questionnaire');  
                $fileName="";
                $htmlCode ="";
                $list=explode('|',rtrim($list,'|'));                
                if($list=='')
                {
                    echo "<script>alert('No Questionnair Selected !');</script>"; 
                    exit;
                }   
                foreach($list as $val){
                $reportData            = $Questionnaire->generateReports($val);   
                $dateRange             = $Questionnaire->generateReportsDateRange($val);                   
                $this->smarty->assign('dateRange', $dateRange);
                $this->smarty->assign('reportData', $reportData);                
                $htmlCode.= $this->smarty->fetch('SatisfactionQuestionnaireReportExport.tpl');  
                $fileName.=$dateRange['StartDate']."_".$dateRange['EndDate']; 
                }
                $fileName.=".xls";           
                header("Pragma: public");
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Content-Type: application/force-download");
                header("Content-Type: application/octet-stream");
                header("Content-Type: application/download");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $fileName . '"');        
                header("Content-Transfer-Encoding: binary ");             
                echo $htmlCode;
                exit;
                break;
            
            default:               
                $this->smarty->display('SatisfactionQuestionnaire.tpl');
        }
    }    
    
    
    /**
     * Description
     * 
     * This method is used for to manuplate data of Country Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
    public function countryAction($args) {



        $functionAction = isset($args[0]) ? $args[0] : '';
        $selectedRowId = isset($args[1]) ? $args[1] : '';



        $Skyline = $this->loadModel('Skyline');

        //If the action is insert then we are getting only skyline brand since user(i.e super admin) can create actions only for skyline (Of course others can copy this action if they want)
        if ($functionAction == "insert" && $selectedRowId == '') {
            $brands = $Skyline->getBrand(1000);
        } else if ($functionAction == "insert" && $selectedRowId != '') {
            $brands = $Skyline->getBrand('', 1000);
        } else { // Getting brands (logged in user can see brands which are assinged to him/her )...
            $brands = $Skyline->getBrand();
        }




        $this->smarty->assign('brands', $brands);
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
        $this->smarty->assign('userBrands', $this->user->Brands); //this value should be from user calss
        $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));

        $accessErrorFlag = false;


        $this->page = $this->messages->getPage('country', $this->lang);
        
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('country');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];

        
        
        $this->smarty->assign('page', $this->page);



        switch ($functionAction) {

            case 'insert':

                //We are fetching data for the selected row where user clicks on copy button
                $args['CountryID'] = $selectedRowId;
                if ($args['CountryID'] != '') {
                    $model = $this->loadModel('Country');
                    $datarow = $model->fetchRow($args);
                    $datarow['CountryID'] = '';
                    $datarow['BrandID'] = '';
                    $datarow['Status'] = 'Active';
                } else {

                    $datarow = array('CountryID' => '', 'CountryCode' => '', 'BrandID' => '', 'InternationalCode' => '', 'Name' => '', 'Status' => 'Active');
                }

                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else if (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {
                    if ($datarow['CountryCode'] == '') {
                        $accessErrorFlag = true;
                    }
                }

                $this->smarty->assign('datarow', $datarow);
                $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);



                $htmlCode = $this->smarty->fetch('countryForm.tpl');

                echo $htmlCode;

                break;


            case 'update':


                $args['CountryID'] = $selectedRowId;
                $model = $this->loadModel('Country');

                $datarow = $model->fetchRow($args);


                $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);


                //If the super admin updating existing record of skyline he can not change this brand to other brand so we are only fetching skyline for the brand dropdown in the update page.
                if ($datarow['BrandID'] == 1000) {
                    $brands = $Skyline->getBrand(1000);
                    $this->smarty->assign('brands', $brands);
                }


                $this->smarty->assign('datarow', $datarow);

                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else if (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {

                    if (!isset($this->user->Brands[$datarow['BrandID']])) {
                        $accessErrorFlag = true;
                    }
                }

                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);


                $htmlCode = $this->smarty->fetch('countryForm.tpl');

                echo $htmlCode;

                break;

            default:

                $this->smarty->display('country.tpl');
        }
    }

    /**
     * Description
     * 
     * This method is used for to manuplate data of VATRates Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */
    public function VATRatesAction($args) {


        $functionAction = isset($args[0]) ? $args[0] : '';
        $selectedRowId = isset($args[1]) ? $args[1] : '';

        #$this->log('[message] :' . $functionAction );

        $Skyline = $this->loadModel('Skyline');

        //If the action is insert then we are getting only skyline brand since user(i.e super admin) can create actions only for skyline (Of course others can copy this action if they want)
//           if($functionAction=="insert" && $selectedRowId=='')
//           {
//                $brands = $Skyline->getBrand(1000);
//           }
//           else if($functionAction=="insert" && $selectedRowId!='')
//           {
//                $brands = $Skyline->getBrand('',1000);
//           }
//           else // Getting brands (logged in user can see brands which are assinged to him/her )...
//           {
//               $brands = $Skyline->getBrand();
//           }




        $this->smarty->assign('countries', $Skyline->getCountries());
        #$this->smarty->assign('brands', $brands); 
        $this->smarty->assign('cID', $functionAction);
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
        $this->smarty->assign('userBrands', $this->user->Brands); //this value should be from user calss
        $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));

        $accessErrorFlag = false;


        $this->page = $this->messages->getPage('VATRates', $this->lang);
        
           //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('VATRates');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];

       
        
        $this->smarty->assign('page', $this->page);





        switch ($functionAction) {

            case 'insert':

                //We are fetching data for the selected row where user clicks on copy button
                $this->log('CountryID :  ' . $selectedRowId);

                if (isset($args['VatRateID'])) {
                    $model = $this->loadModel('VATRates');
                    $datarow = $model->fetchRow($args);
                    $datarow['VatRateID'] = '';
                    $datarow['Status'] = 'Active';
                } else {

                    $datarow = array('VatRateID' => '', 'VatCode' => '', 'VatRate' => '', 'Code' => '', 'Status' => 'Active');
                }

                $datarow['CountryID'] = $selectedRowId;

                #$this->log( 'datarow' . var_export( $datarow, true ) );
                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else if (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {
                    if ($datarow['VatCode'] == '') {
                        $accessErrorFlag = true;
                    }
                }

                $this->smarty->assign('datarow', $datarow);
                $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);



                $htmlCode = $this->smarty->fetch('VATRatesForm.tpl');

                echo $htmlCode;

                break;


            case 'update':

                $args['VatRateID'] = $selectedRowId;
                $model = $this->loadModel('VATRates');

                $datarow = $model->fetchRow($args);
                $datarow['CountryID'] = $selectedRowId;


                $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                //If the super admin updating existing record of skyline he can not change this brand to other brand so we are only fetching skyline for the brand dropdown in the update page.
//                        if($datarow['BrandID']==1000)
//                        {
//                                $brands = $Skyline->getBrand(1000);
//                                $this->smarty->assign('brands', $brands); 
//                        }


                $this->smarty->assign('datarow', $datarow);

                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else if (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {

//                            if(!isset($this->user->Brands[$datarow['BrandID']]))
//                            {
//                                $accessErrorFlag = true;
//                            }
                }

                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);


                $htmlCode = $this->smarty->fetch('VATRatesForm.tpl');

                echo $htmlCode;

                break;

            default:

                $this->smarty->display('VATRates.tpl');
        }
    }

    /**
     * Description
     * 
     * This method is used for to manuplate data of Job Types Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
        public function jobTypesAction(  $args  ) {
            
            
            $functionAction   = isset($args[0])?$args[0]:'';
            $selectedRowId        = isset($args[1])?$args[1]:'';



            $Skyline     = $this->loadModel('Skyline');

            //If the action is insert then we are getting only skyline brand since user(i.e super admin) can create actions only for skyline (Of course others can copy this action if they want)
            if($functionAction=="insert" && $selectedRowId=='')
            {
                    $brands = $Skyline->getBrand(1000);
            }
            else if($functionAction=="insert" && $selectedRowId!='')
            {
                    $brands = $Skyline->getBrand('',1000);
            }
            else // Getting brands (logged in user can see brands which are assinged to him/her )...
            {
                $brands = $Skyline->getBrand();
            }


           

            $this->smarty->assign('brands', $brands); 
            $this->smarty->assign('statuses', $this->statuses);
            $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin);//this value should be from user calss
            $this->smarty->assign('userPermissions', $this->user->Permissions);//this value should be from user calss
            $this->smarty->assign('userBrands', $this->user->Brands);//this value should be from user calss
            $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));

            $accessErrorFlag = false;
            
            
            $this->page    = $this->messages->getPage('jobTypes', $this->lang);
            
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('jobTypes');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
           
            $this->smarty->assign('page', $this->page);


            switch( $functionAction) {

            case 'insert':

                    //We are fetching data for the selected row where user clicks on copy button
                    $args['JobTypeID']       = $selectedRowId;
                    if($args['JobTypeID']!='')
                    {
                        $model     = $this->loadModel('JobTypes'); 
                        $datarow   = $model->fetchRow($args);
                        $datarow['JobTypeID'] = '';
                        $datarow['BrandID'] = '';
                        $datarow['Status'] = 'Active';
                    }
                    else
                    {

                        $datarow = array('JobTypeID'=>'', 'TypeCode'=>'', 'BrandID'=>'', 'Type'=>'', 'Priority'=>'', 'Status'=>'Active');
                    }

                    //Checking user permissons to display the page.
                    if($this->user->SuperAdmin)
                    {
                        $accessErrorFlag = false;
                    }
                    else if(isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002']))
                    {
                        if($datarow['TypeCode']=='')
                        {
                            $accessErrorFlag = true;
                        }
                    }

                    $this->smarty->assign('datarow', $datarow);
                    $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);



                    $htmlCode = $this->smarty->fetch('jobTypesForm.tpl');

                    echo $htmlCode;

                break;


            case 'update':


                    $args['JobTypeID']  = $selectedRowId;
                    $model     = $this->loadModel('JobTypes');

                    $datarow  = $model->fetchRow($args);


                    $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                    //If the super admin updating existing record of skyline he can not change this brand to other brand so we are only fetching skyline for the brand dropdown in the update page.
                            if($datarow['BrandID']==1000)
                            {
                                    $brands = $Skyline->getBrand(1000);
                                    $this->smarty->assign('brands', $brands); 
                            }


                    $this->smarty->assign('datarow', $datarow);

                    //Checking user permissons to display the page.
                    if($this->user->SuperAdmin)
                    {
                        $accessErrorFlag = false;
                    }
                    else if(isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002']))
                    {

                    if(!isset($this->user->Brands[$datarow['BrandID']]))
                    {
                        $accessErrorFlag = true;
                    }
                    }    

                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);


                    $htmlCode = $this->smarty->fetch('jobTypesForm.tpl');

                echo $htmlCode;    

                break;

                default:

                    $this->smarty->display('jobTypes.tpl'); 

            }  
            
            
        }
      
      
      
    /*
    * Description
    * 
    * This method is used for to manuplate data of RA Status Types Page.
    * 
    * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
    * @return void It prints data to the loaded template.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
    */   
       
    public function RAStatusTypesAction($args) {
        
	$functionAction = isset($args[0]) ? $args[0] : "";
	$selectedRowId = isset($args[1]) ? $args[1] : "";
        
        $Skyline = $this->loadModel("Skyline");
        
        //If the action is insert then we are getting only skyline brand since user(i.e super admin) can create actions only for skyline (Of course others can copy this action if they want)
        if($functionAction == "insert" && $selectedRowId == "") {
	    $brands = $Skyline->getBrand(1000);
        } else if($functionAction == "insert" && $selectedRowId != "") {
	    $brands = $Skyline->getBrand("", 1000);
        } else {				//Getting brands (logged in user can see brands which are assinged to him)
            $brands = $Skyline->getBrand();
        }
        
	$this->smarty->assign("brands", $brands); 
	$this->smarty->assign("statuses", $this->statuses);
	$this->smarty->assign("SupderAdmin", $this->user->SuperAdmin);	    //this value should be from user calss
	$this->smarty->assign("userPermissions", $this->user->Permissions); //this value should be from user calss
	//$this->smarty->assign('userBrands', $this->user->Brands);	    //this value should be from user calss
	$this->smarty->assign("accessDeniedMsg", $this->messages->getError(1023, "default", $this->lang));
        
        $accessErrorFlag = false;
        
	$this->page = $this->messages->getPage("RAStatusTypes", $this->lang);
        
           //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('RAStatusTypes');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
           
        
	$this->smarty->assign("page", $this->page);

	$model = $this->loadModel("RAStatusTypes");
	$getRAStatusTypes = $model->getRAStatusTypes();
	$this->smarty->assign("raStatuses", $getRAStatusTypes);
	
	$sbStatuses = $model->getSBStatuses();
	$this->smarty->assign("sbStatuses", $sbStatuses);
	
        switch($functionAction) {

            case "insert": {

		//We are fetching data for the selected row where user clicks on copy button
                $args["RAStatusID"] = $selectedRowId;
		
                if($args["RAStatusID"] != "") {
		    
		    $datarow = $model->fetchRow($args);
		    $datarow["RAStatusID"] = "";
		    $datarow["BrandID"] = "";
		    $datarow["Status"] = "Active";
		    $this->smarty->assign("copy", true);
		    
		} else {

		    $datarow = [
			"RAStatusID" => "", 
			"RAStatusName" => "",
			"BrandID" => "",
			"RAStatusCode" => "",
			"ListOrder" => "",
			"Colour" => "",
			"Subset" => [],
			"Status" => "Active",
			"SubsetOnly" => "",
			"ServiceBaseStatus" => "",
			"SBAuthorisationStatusID" => "",
			"Final" => ""
		    ];
		    
                }

	        //Checking user permissons to display the page.
                if($this->user->SuperAdmin) {
		    $accessErrorFlag = false;
		} else if(isset($this->user->Permissions["AP7001"]) || isset($this->user->Permissions["AP7002"])) {
		    if($datarow["RAStatusCode"] == "") {
			$accessErrorFlag = true;
                    }
                }

		$this->smarty->assign("datarow", $datarow);
		$this->smarty->assign("form_legend", $this->page["Text"]["insert_page_legend"]);
		$this->smarty->assign("accessErrorFlag", $accessErrorFlag);
		if($selectedRowId == "") {
		    $this->smarty->assign("insert", true);
		    $this->smarty->assign("authTypes", $model->getAuthorisationTypes());
		}
		
                $htmlCode = $this->smarty->fetch("RAStatusTypesForm.tpl");

                echo $htmlCode;

                break;

	    }

	    case "update": {

		$args["RAStatusID"] = $selectedRowId;

                $datarow = $model->fetchRow($args);

		$this->smarty->assign("form_legend", $this->page["Text"]["update_page_legend"]);

                //If the super admin updating existing record of skyline he can not change this brand to other brand so we are only fetching skyline for the brand dropdown in the update page.
                if($datarow["BrandID"] == 1000) {
		    $brands = $Skyline->getBrand(1000);
		    $this->smarty->assign("brands", $brands);
                }

                $this->smarty->assign("datarow", $datarow);

		//Checking user permissons to display the page.
		if($this->user->SuperAdmin) {
		    $accessErrorFlag = false;
		} else if(isset($this->user->Permissions["AP7001"]) || isset($this->user->Permissions["AP7002"])) {
		    if(!isset($this->user->Brands[$datarow["BrandID"]])) {
			$accessErrorFlag = true;
		    }
		}    

                $this->smarty->assign("accessErrorFlag", $accessErrorFlag);

		$htmlCode = $this->smarty->fetch("RAStatusTypesForm.tpl");

                echo $htmlCode;    

                break;
		
	    }
             
	    default: {
		
		$this->smarty->assign("authTypes", $model->getAuthorisationTypes());
		$this->smarty->display("RAStatusTypes.tpl");
		
	    }
	    
	}  
         
    }
	
    
    
    /**
     * Description
     * 
     * This method is used for to manuplate data of Payment Types Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */
    public function paymentTypesAction($args) {



        $functionAction = isset($args[0]) ? $args[0] : '';
        $selectedRowId = isset($args[1]) ? $args[1] : '';



        $Skyline = $this->loadModel('Skyline');

        //If the action is insert then we are getting only skyline brand since user(i.e super admin) can create actions only for skyline (Of course others can copy this action if they want)
        if ($functionAction == "insert" && $selectedRowId == '') {
            $brands = $Skyline->getBrand(1000);
        } else if ($functionAction == "insert" && $selectedRowId != '') {
            $brands = $Skyline->getBrand('', 1000);
        } else { // Getting brands (logged in user can see brands which are assinged to him/her )...
            $brands = $Skyline->getBrand();
        }




        $this->smarty->assign('brands', $brands);
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
        $this->smarty->assign('userBrands', $this->user->Brands); //this value should be from user calss
        $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));

        $accessErrorFlag = false;


        $this->page = $this->messages->getPage('paymentTypes', $this->lang);
        
        
           //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('paymentTypes');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
           
        
        $this->smarty->assign('page', $this->page);





        switch ($functionAction) {

            case 'insert':

                //We are fetching data for the selected row where user clicks on copy button
                $args['PaymentTypeID'] = $selectedRowId;
                if ($args['PaymentTypeID'] != '') {
                    $model = $this->loadModel('PaymentTypes');
                    $datarow = $model->fetchRow($args);
                    $datarow['PaymentTypeID'] = '';
                    $datarow['BrandID'] = '';
                    $datarow['Status'] = 'Active';
                } else {

                    $datarow = array('PaymentTypeID' => '', 'TypeCode' => '', 'BrandID' => '', 'SystemCode' => '', 'Type' => '', 'Status' => 'Active');
                }

                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else if (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {
                    if ($datarow['TypeCode'] == '') {
                        $accessErrorFlag = true;
                    }
                }

                $this->smarty->assign('datarow', $datarow);
                $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);



                $htmlCode = $this->smarty->fetch('paymentTypesForm.tpl');

                echo $htmlCode;

                break;


            case 'update':


                $args['PaymentTypeID'] = $selectedRowId;
                $model = $this->loadModel('PaymentTypes');

                $datarow = $model->fetchRow($args);


                $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);


                //If the super admin updating existing record of skyline he can not change this brand to other brand so we are only fetching skyline for the brand dropdown in the update page.
                if ($datarow['BrandID'] == 1000) {
                    $brands = $Skyline->getBrand(1000);
                    $this->smarty->assign('brands', $brands);
                }

                $this->smarty->assign('datarow', $datarow);

                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else if (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {

                    if (!isset($this->user->Brands[$datarow['BrandID']])) {
                        $accessErrorFlag = true;
                    }
                }

                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);


                $htmlCode = $this->smarty->fetch('paymentTypesForm.tpl');

                echo $htmlCode;

                break;

            default:

                $this->smarty->display('paymentTypes.tpl');
        }
    }

    /**
     * Description
     * 
     * This method is used for to manuplate data of Security Questions Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
    public function securityQuestionsAction($args) {


        $functionAction = isset($args[0]) ? $args[0] : '';
        $selectedRowId = isset($args[1]) ? $args[1] : '';



        $Skyline = $this->loadModel('Skyline');

        //If the action is insert then we are getting only skyline brand since user(i.e super admin) can create actions only for skyline (Of course others can copy this action if they want)
        if ($functionAction == "insert" && $selectedRowId == '') {
            $brands = $Skyline->getBrand(1000);
        } else if ($functionAction == "insert" && $selectedRowId != '') {
            $brands = $Skyline->getBrand('', 1000);
        } else { // Getting brands (logged in user can see brands which are assinged to him/her )...
            $brands = $Skyline->getBrand();
        }




        $this->smarty->assign('brands', $brands);
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
        $this->smarty->assign('userBrands', $this->user->Brands); //this value should be from user calss
        $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));

        $accessErrorFlag = false;

        $this->page = $this->messages->getPage('securityQuestions', $this->lang);
        
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('securityQuestions');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
           
        
        $this->smarty->assign('page', $this->page);





        switch ($functionAction) {

            case 'insert':

                //We are fetching data for the selected row where user clicks on copy button
                $args['SecurityQuestionID'] = $selectedRowId;
                if ($args['SecurityQuestionID'] != '') {
                    $model = $this->loadModel('SecurityQuestions');
                    $datarow = $model->fetchRow($args);
                    $datarow['SecurityQuestionID'] = '';
                    $datarow['BrandID'] = '';
                    $datarow['Status'] = 'Active';
                } else {

                    $datarow = array('SecurityQuestionID' => '', 'QuestionCode' => '', 'BrandID' => '', 'SystemCode' => '', 'Question' => '', 'Status' => 'Active');
                }

                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else if (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {
                    if ($datarow['QuestionCode'] == '') {
                        $accessErrorFlag = true;
                    }
                }

                $this->smarty->assign('datarow', $datarow);
                $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);



                $htmlCode = $this->smarty->fetch('securityQuestionsForm.tpl');

                echo $htmlCode;

                break;


            case 'update':


                $args['SecurityQuestionID'] = $selectedRowId;
                $model = $this->loadModel('SecurityQuestions');

                $datarow = $model->fetchRow($args);


                $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                //If the super admin updating existing record of skyline he can not change this brand to other brand so we are only fetching skyline for the brand dropdown in the update page.
                if ($datarow['BrandID'] == 1000) {
                    $brands = $Skyline->getBrand(1000);
                    $this->smarty->assign('brands', $brands);
                }


                $this->smarty->assign('datarow', $datarow);

                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else if (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {

                    if (!isset($this->user->Brands[$datarow['BrandID']])) {
                        $accessErrorFlag = true;
                    }
                }

                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);


                $htmlCode = $this->smarty->fetch('securityQuestionsForm.tpl');

                echo $htmlCode;

                break;

            default:

                $this->smarty->display('securityQuestions.tpl');
        }
    }


    
    /**
     * Description
     * 
     * This method is used for to manuplate data of System Statuses Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
    public function systemStatusesAction($args) {

        if (!$this->user->SuperAdmin) {
            $this->redirect('SystemAdmin/index/lookupTables', null, null);
        }

        $functionAction = isset($args[0]) ? $args[0] : '';
        $selectedRowId = isset($args[1]) ? $args[1] : '';



        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
        $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));

        $accessErrorFlag = false;

        $this->page = $this->messages->getPage('systemStatuses', $this->lang);
        
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('systemStatuses');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
           
        
        $this->smarty->assign('page', $this->page);

        $TimelineStatusList = array(
            0 => array('Code' => 'booked', 'Name' => 'Booked'),
            1 => array('Code' => 'viewed', 'Name' => 'Viewed'),
            2 => array('Code' => 'in_progress', 'Name' => 'In Progress'),
            3 => array('Code' => 'parts_ordered', 'Name' => 'Parts Ordered'),
            4 => array('Code' => 'parts_received', 'Name' => 'Parts Received'),
            5 => array('Code' => 'site_visit', 'Name' => 'Site Visit'),
            6 => array('Code' => 'revisit', 'Name' => 'Revisit'),
            7 => array('Code' => 'closed', 'Name' => 'Closed')
        );

        $this->smarty->assign('TimelineStatusList', $TimelineStatusList);

        switch ($functionAction) {

            case 'insert':

                //We are fetching data for the selected row where user clicks on copy button
                $args['StatusID'] = $selectedRowId;
                if ($args['StatusID'] != '') {
                    $model = $this->loadModel('SystemStatuses');
                    $datarow = $model->fetchRow($args);
                    $datarow['StatusID'] = '';
                    $datarow['Status'] = 'Active';
                } else {

                    $datarow = array('StatusID' => '', 'StatusName' => '', 'Colour' => '', 'Status' => 'Active', 'TimelineStatus' => '', 'StatusTAT' => '', 'StatusTATType' => '');
                }

                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else {
                    $accessErrorFlag = true;
                }

                $this->smarty->assign('datarow', $datarow);
                $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);



                $htmlCode = $this->smarty->fetch('systemStatusesForm.tpl');

                echo $htmlCode;

                break;


            case 'update':


                $args['StatusID'] = $selectedRowId;
                $model = $this->loadModel('SystemStatuses');

                $datarow = $model->fetchRow($args);


                $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);


                $this->smarty->assign('datarow', $datarow);

                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else {
                    $accessErrorFlag = true;
                }

                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);


                $htmlCode = $this->smarty->fetch('systemStatusesForm.tpl');

                echo $htmlCode;

                break;

            default:

                $this->smarty->display('systemStatuses.tpl');
        }
    }

    /**
     * Description
     * 
     * This method is used for to manuplate data of Mobile Phone Network Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
    public function mobilePhoneNetworkAction($args) {

        if (!$this->user->SuperAdmin) {
            $this->redirect('SystemAdmin/index/lookupTables', null, null);
        }

        $functionAction = isset($args[0]) ? $args[0] : '';
        $selectedRowId = isset($args[1]) ? $args[1] : '';



        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
        $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));

        $skyline_model = $this->loadModel('Skyline');
        $CountriesList = $skyline_model->getCountries();
        $this->smarty->assign('CountriesList', $CountriesList);


        $accessErrorFlag = false;

        $this->page = $this->messages->getPage('mobilePhoneNetwork', $this->lang);
        
                    //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('mobilePhoneNetwork');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
           
        
        $this->smarty->assign('page', $this->page);

        switch ($functionAction) {

            case 'insert':



                $datarow = array('MobilePhoneNetworkID' => '', 'MobilePhoneNetworkName' => '', 'CountryID' => '', 'PhoneNetworkType' => 'Operator', 'Status' => 'Active');


                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else {
                    $accessErrorFlag = true;
                }

                $this->smarty->assign('datarow', $datarow);
                $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);



                $htmlCode = $this->smarty->fetch('mobilePhoneNetworkForm.tpl');

                echo $htmlCode;

                break;


            case 'update':


                $args['MobilePhoneNetworkID'] = $selectedRowId;
                $model = $this->loadModel('MobilePhoneNetwork');

                $datarow = $model->fetchRow($args);


                $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);


                $this->smarty->assign('datarow', $datarow);

                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else {
                    $accessErrorFlag = true;
                }

                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);


                $htmlCode = $this->smarty->fetch('mobilePhoneNetworkForm.tpl');

                echo $htmlCode;

                break;

            default:

                $this->smarty->display('mobilePhoneNetwork.tpl');
        }
    }

    /**
     * Description
     * 
     * This method is used for to manuplate data of SMS Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
    public function smsAction($args) {

        if (!$this->user->SuperAdmin) {
            $this->redirect('SystemAdmin/index/lookupTables', null, null);
        }

        $functionAction = isset($args[0]) ? $args[0] : '';
        $selectedRowId = isset($args[1]) ? $args[1] : '';



        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
        $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));

        $SkylineModel = $this->loadModel('Skyline');

        $brandsList = $SkylineModel->getBrand();
        $this->smarty->assign('brandsList', $brandsList);

        $accessErrorFlag = false;

        $this->page = $this->messages->getPage('sms', $this->lang);
        
                            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('sms');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
           
        
        $this->smarty->assign('page', $this->page);

        switch ($functionAction) {

            case 'insert':



                $datarow = array('BrandID' => '', 'SMSID' => '', 'SMSName' => '', 'SMSBodyText' => '', 'Status' => 'Active');


                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else {
                    $accessErrorFlag = true;
                }

                $this->smarty->assign('datarow', $datarow);
                $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);



                $htmlCode = $this->smarty->fetch('smsForm.tpl');

                echo $htmlCode;

                break;


            case 'update':


                $args['SMSID'] = $selectedRowId;
                $model = $this->loadModel('SMS');

                $datarow = $model->fetchRow($args);


                $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                $datarow['SMSUserName'] = '';

                if (isset($datarow['ModifiedUserID']) && $datarow['ModifiedUserID']) {
                    $users_model = $this->loadModel('Users');
                    $datarow['SMSUserName'] = $users_model->getUserFullName($datarow['ModifiedUserID']);
                }

                $this->smarty->assign('datarow', $datarow);

                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else {
                    $accessErrorFlag = true;
                }

                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);


                $htmlCode = $this->smarty->fetch('smsForm.tpl');

                echo $htmlCode;

                break;

            default:

                $this->smarty->display('sms.tpl');
        }
    }

    /**
     * systemStatusPermissions
     * 
     * Update the system starus permissions
     * 
     * @param array args    Paased Arguments
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function systemStatusPermissionsAction($args) {
        $model = $this->loadModel('StatusPermission');

        if (!$this->user->SuperAdmin) {
            $this->redirect('SystemAdmin/index/lookupTables', null, null);
        }

        $permissionType = isset($args[0]) ? $args[0] : 'user';                      /* First argument is the permisission type (user / branch etc */
        $permissionTypeDesc = $permissionType . '_desc';
        $functionAction = isset($args[1]) ? $args[1] : '';                          /* Second if passed is the function (otherwise display the list */
        $entity_id = isset($args[2]) ? $args[2] : '';                               /* Third (if we are update) is the entity id we wish to edit */

        $this->smarty->assign('permissionType', $permissionType);
        $this->smarty->assign('permissionTypeDesc', $permissionTypeDesc);
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin);

        $permissionTypeList = array(
            'brand' => 'Brand',
            'branch' => 'Branch',
            'client' => 'Client',
            'role' => 'Role',
            'user' => 'User'
        );

        $this->smarty->assign('permissionTypeList', $permissionTypeList);

        $accessErrorFlag = false;
        
        $this->page = $this->messages->getPage('statusPermissions', $this->lang);
       //Getting Page title & Description from database
       $model = $this->loadModel('GroupHeadings');        
       $groupHedingResults = $model->getHeadingPageDetails('systemStatusPermissions');        
       $this->page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$this->page['Text']['page_title'];
       $this->page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$this->page['Text']['description'];
               
        $this->smarty->assign('page', $this->page);

        switch ($functionAction) { /* What do we wnat to do ? */
            case 'update': /* Edit existiing permission type */


                $datarow = $model->fetchByEntity($permissionType, $entity_id);

                $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);
                $this->smarty->assign('datarow', $datarow);

                $this->smarty->display('statusPermissionsForm.tpl');

                break;

            default:
                $this->smarty->display('statusPermissions.tpl');
        }
    }

     /**
     * Open Jobs Status Permissions
     *      
     * 
     * @param array args    Paased Arguments
     * 
     * @return 
     * 
     * @author v.srinivasa rao <s.vempati@pccsuk.com>  
     * ************************************************************************ */
    public function openJobsStatusPermissionsAction($args) {
        $model = $this->loadModel('OpenJobsStatusPermission');

        if (!$this->user->SuperAdmin) {
            $this->redirect('SystemAdmin/index/lookupTables', null, null);
        }

        $permissionType = isset($args[0]) ? $args[0] : 'user';                      /* First argument is the permisission type (user / branch etc */
        $permissionTypeDesc = $permissionType . '_desc';
        $functionAction = isset($args[1]) ? $args[1] : '';                          /* Second if passed is the function (otherwise display the list */
        $entity_id = isset($args[2]) ? $args[2] : '';                               /* Third (if we are update) is the entity id we wish to edit */

        $this->smarty->assign('permissionType', $permissionType);
        $this->smarty->assign('permissionTypeDesc', $permissionTypeDesc);
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin);

        $permissionTypeList = array(
            'brand' => 'Brand',
            'branch' => 'Branch',
            'client' => 'Client',
            'role' => 'Role',
            'user' => 'User'
        );

        $this->smarty->assign('permissionTypeList', $permissionTypeList);

        $accessErrorFlag = false;

       $this->page = $this->messages->getPage('openJobsStatusPermissions', $this->lang);
       //Getting Page title & Description from database
       $model = $this->loadModel('GroupHeadings');        
       $groupHedingResults = $model->getHeadingPageDetails('openJobsStatusPermissions');        
       $this->page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$this->page['Text']['page_title'];
       $this->page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$this->page['Text']['description'];
       
       $this->smarty->assign('page', $this->page);

        switch ($functionAction) { /* What do we wnat to do ? */
            case 'update': /* Edit existiing permission type */


                $datarow = $model->fetchByEntity($permissionType, $entity_id);

                $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);
                $this->smarty->assign('datarow', $datarow);

                $this->smarty->display('openJobStatusPermissionsForm.tpl');

                break;

            default:
                $this->smarty->display('OpenJobStatusPermissions.tpl');
        }
    }
    
    /**
     * StatusPermissionForm 
     * 
     * Maniipulate data on the status permissions form
     * 
     * @param array $args     Arguments (1st EntityType, 2nd Function, 3rd Entity ID 
     * 
     * @return 
     * 
     * @author v srinviasa rao <s.vempati@pccsuk.com>  
     * ************************************************************************ */
    public function openJobsStatusPermissionsFormAction($args) {
        $status_permission_model = $this->loadModel('OpenJobsStatusPermission');

        $entitytype = isset($args[0]) ? $args[0] : 'user';
        $functionAction = isset($args[1]) ? $args[1] : '';
        $entityId = isset($args[2]) ? $args[2] : '';        


        if ($functionAction == 'tagged') {
            $items = $status_permission_model->fetchByEntityTagged($entitytype, $entityId);

            echo json_encode($items);
        } else {


            //$this->smarty->assign('RoleStatus', $RoleStatus);
            $this->smarty->assign('entityType', $entitytype);
            $this->smarty->assign('statuses', $this->statuses);
            $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin);      /* This value should be from user class */
            $this->smarty->assign('userPermissions', $this->user->Permissions); /* This value should be from user class */
            $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));

            $accessErrorFlag = false;


            $this->page = $this->messages->getPage('openJobsStatusPermissions', $this->lang);
            
            $this->smarty->assign('page', $this->page);

            switch ($functionAction) {


                case 'update': /* Update existing permissions */
                    $this->smarty->assign('entitytype', $entitytype);
                    $Tagged = 0;                                            /* Tagged counter */
                    $Untagged = 0;                                          /* Untagged counter */
                    $Both = 0;                                              /* Total counter */

                    $datarow = $status_permission_model->fetchByEntityId($entitytype, $entityId);

                    $permissionsList = $status_permission_model->fetchByEntity($entitytype, $entityId);

                    $tCnt = count($permissionsList['aaData']);

                    for ($i = 0; $i < $tCnt; $i++) {
                        if (intval($permissionsList['aaData'][$i][2])) {
                            $Tagged++;
                        } else {
                            $Untagged++;
                        }
                    }

                    $Both = $Tagged + $Untagged;


                    $this->smarty->assign('Both', $Both);
                    $this->smarty->assign('Tagged', $Tagged);
                    $this->smarty->assign('Untagged', $Untagged);

                    $this->smarty->assign('PermissionTagged', 'Both');

                    //$TotalPermissions =  $Skyline->getPermissions(array('statusFlag'=> false), $args['RoleID']);
                    $TotalPermissions = $status_permission_model->fetchByEntity($entitytype, $entityId);
                    $this->smarty->assign('TotalPermissions', (isset($TotalPermissions['aaData'])) ? $TotalPermissions['aaData'] : false);

                    $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                    $this->smarty->assign('datarow', $datarow);
                    

                    //Checking user permissons to display the page.
                    if ($this->user->SuperAdmin) {
                        $accessErrorFlag = false;
                    }

                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);

                    $htmlCode = $this->smarty->fetch('openJobStatusPermissionsForm.tpl');

                    echo $htmlCode;

                    break;

                default:

                    $this->smarty->display('OpenJobStatusPermissions.tpl');
            }
        }
    }
    
    
    /**
     * StatusPermissionForm 
     * 
     * Maniipulate data on the status permissions form
     * 
     * @param array $args     Arguments (1st EntityType, 2nd Function, 3rd Entity ID 
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function systemStatusPermissionFormAction($args) {
        $status_permission_model = $this->loadModel('StatusPermission');

        $entitytype = isset($args[0]) ? $args[0] : 'user';
        $functionAction = isset($args[1]) ? $args[1] : '';
        $entityId = isset($args[2]) ? $args[2] : '';
        //$RoleStatus = isset($args['RoleStatus'])?$args['RoleStatus']:'Active';  


        if ($functionAction == 'tagged') {
            $items = $status_permission_model->fetchByEntityTagged($entitytype, $entityId);

            echo json_encode($items);
        } else {


            //$this->smarty->assign('RoleStatus', $RoleStatus);
            $this->smarty->assign('entityType', $entitytype);
            $this->smarty->assign('statuses', $this->statuses);
            $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin);      /* This value should be from user class */
            $this->smarty->assign('userPermissions', $this->user->Permissions); /* This value should be from user class */
            $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));

            $accessErrorFlag = false;


            $this->page = $this->messages->getPage('statusPermissions', $this->lang);
            
            $this->smarty->assign('page', $this->page);

            //$allBrandsJobTypes = $Skyline->getBrandsJobTypes();
            //$this->smarty->assign('allBrandsJobTypes', $allBrandsJobTypes);

            /* $userTypes = array(
              'Service Network' => 'Service Network',
              'Client' => 'Client',
              'Branch' => 'Branch',
              'Service Provider' => 'Service Provider',
              'Manufacturer' => 'Manufacturer',
              'Extended Warrantor' => 'Extended Warrantor'
              ); */

            //$this->smarty->assign('UserTypes', $userTypes);

            switch ($functionAction) {


                case 'update': /* Update existing permissions */
                    $this->smarty->assign('entitytype', $entitytype);
                    $Tagged = 0;                                            /* Tagged counter */
                    $Untagged = 0;                                          /* Untagged counter */
                    $Both = 0;                                              /* Total counter */

                    $datarow = $status_permission_model->fetchByEntityId($entitytype, $entityId);

                    $permissionsList = $status_permission_model->fetchByEntity($entitytype, $entityId);

                    $tCnt = count($permissionsList['aaData']);

                    for ($i = 0; $i < $tCnt; $i++) {
                        if (intval($permissionsList['aaData'][$i][2])) {
                            $Tagged++;
                        } else {
                            $Untagged++;
                        }
                    }

                    $Both = $Tagged + $Untagged;


                    $this->smarty->assign('Both', $Both);
                    $this->smarty->assign('Tagged', $Tagged);
                    $this->smarty->assign('Untagged', $Untagged);

                    $this->smarty->assign('PermissionTagged', 'Both');

                    //$TotalPermissions =  $Skyline->getPermissions(array('statusFlag'=> false), $args['RoleID']);
                    $TotalPermissions = $status_permission_model->fetchByEntity($entitytype, $entityId);
                    $this->smarty->assign('TotalPermissions', (isset($TotalPermissions['aaData'])) ? $TotalPermissions['aaData'] : false);

                    $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                    $this->smarty->assign('datarow', $datarow);

                    //Checking user permissons to display the page.
                    if ($this->user->SuperAdmin) {
                        $accessErrorFlag = false;
                    }

                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);

                    $htmlCode = $this->smarty->fetch('statusPermissionsForm.tpl');

                    echo $htmlCode;

                    break;

                default:

                    $this->smarty->display('statusPermissions.tpl');
            }
        }
    }

    /**
     * Description
     * 
     * This method is used for to manuplate data of Access Permissions Page.
     * 
     * @param array $args It contains action type as first element i.e update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
    public function accessPermissionsAction($args) {

        if (!$this->user->SuperAdmin) {
            $this->redirect('SystemAdmin/index/lookupTables', null, null);
        }

        $functionAction = isset($args[0]) ? $args[0] : '';
        $selectedRowId = isset($args[1]) ? $args[1] : '';



        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user class

        $accessErrorFlag = false;

        $this->page = $this->messages->getPage('accessPermissions', $this->lang);
        
           //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('accessPermissions');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
           
        
        $this->smarty->assign('page', $this->page);



        switch ($functionAction) {


            case 'update':


                $args['PermissionID'] = $selectedRowId;
                $model = $this->loadModel('AccessPermissions');

                $datarow = $model->fetchRow($args);


                $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);


                $this->smarty->assign('datarow', $datarow);

                $htmlCode = $this->smarty->fetch('accessPermissionsForm.tpl');

                echo $htmlCode;

                break;

            default:

                $this->smarty->display('accessPermissions.tpl');
        }
    }

    /**
     * Description
     * 
     * This method is used for to manuplate data of Couriers Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
    public function couriersAction($args) {

        if (!$this->user->SuperAdmin) {
            $this->redirect('SystemAdmin/index/lookupTables', null, null);
        }

        $functionAction = isset($args[0]) ? $args[0] : '';
        $selectedRowId = isset($args[1]) ? $args[1] : '';



        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
        $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));

        $accessErrorFlag = false;

        $this->page = $this->messages->getPage('couriers', $this->lang);
        
                   //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('couriers');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
           
        
        $this->smarty->assign('page', $this->page);

        $OrganisationTypes = array(
            0 => array('Code' => 'Brand', 'Name' => 'Retail Brand'),
            1 => array('Code' => 'Manufacturer', 'Name' => 'Manufacturer'),
            2 => array('Code' => 'ServiceProvider', 'Name' => 'Service Provider')
        );

        $this->smarty->assign('OrganisationTypes', $OrganisationTypes);

        switch ($functionAction) {

            case 'insert':



                $datarow = array('CourierID' => '', "CourierName" => "", 'OrganisationType' => 'ServiceProvider', 'BrandID' => '', 'ManufacturerID' => '', 'ServiceProviderID' => '', 'Online' => 'Yes', 'AccountNo' => '', 'IPAddress' => '', 'UserName' => '', 'Password' => '', 'ReportFailureDays' => '', 'Status' => 'Active', 'CommunicateIndividualConsignments' => false);


                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else {
                    $accessErrorFlag = true;
                }

                $this->smarty->assign('datarow', $datarow);
                $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);

                $this->smarty->assign('package_types', array());


                $htmlCode = $this->smarty->fetch('couriersForm.tpl');

                echo $htmlCode;

                break;


            case 'update':


                $args['CourierID'] = $selectedRowId;
                $model = $this->loadModel('Couriers');

                $datarow = $model->fetchRow($args);

                if ($datarow['BrandID']) {
                    $datarow['OrganisationType'] = 'Brand';
                } else if ($datarow['ManufacturerID']) {
                    $datarow['OrganisationType'] = 'Manufacturer';
                } else {
                    $datarow['OrganisationType'] = 'ServiceProvider';
                }




                $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);


                $this->smarty->assign('datarow', $datarow);

                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else {
                    $accessErrorFlag = true;
                }

                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);

                $package_types = $model->getPackagingTypes($selectedRowId);
                $this->smarty->assign('package_types', $package_types);

                $htmlCode = $this->smarty->fetch('couriersForm.tpl');

                echo $htmlCode;

                break;

            default:

                $skyline_model = $this->loadModel('Skyline');
                $serviceProviders = $skyline_model->getServiceProviders();



                $this->smarty->assign('serviceProviders', $serviceProviders);
                $this->smarty->assign('spId', $functionAction);
                $this->smarty->display('couriers.tpl');
        }
    }

    public function reconAction($args) {

        if (empty($args['ID']))
            throw new Exception('Missing CourierID');

        $this->smarty->assign('page', $this->messages->getPage('couriers', $this->lang));
        $this->smarty->assign('courier_id', $args['ID']);
        echo $this->smarty->fetch('popup/ReconTable.tpl');
    }

    public function reconTableDataAction($args) {

        $recon_model = $this->loadModel('Recon');
        echo json_encode($recon_model->Fetch($args['ID'], $_GET));
    }

    /**
     * Description
     * 
     * This method is used for to manuplate data of Service Types Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */
    public function serviceTypesAction($args) {


        $functionAction = isset($args[0]) ? $args[0] : '';
        $selectedRowId = isset($args[1]) ? $args[1] : '';



        $Skyline = $this->loadModel('Skyline');

        //If the action is insert then we are getting only skyline brand since user(i.e super admin) can create actions only for skyline (Of course others can copy this action if they want)
        if ($functionAction == "insert" && $selectedRowId == '') {
            $brands = $Skyline->getBrand(1000);
        } else if ($functionAction == "insert" && $selectedRowId != '') {
            $brands = $Skyline->getBrand('', 1000);
        } else { // Getting brands (logged in user can see brands which are assinged to him/her )...
            $brands = $Skyline->getBrand();
        }



        $this->smarty->assign('brands', $brands);
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
        $this->smarty->assign('userBrands', $this->user->Brands); //this value should be from user calss
        $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));

        $accessErrorFlag = false;


        $this->page = $this->messages->getPage('serviceTypes', $this->lang);
        
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('serviceTypes');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
           
        
        $this->smarty->assign('page', $this->page);


        $allBrandsJobTypes = $Skyline->getBrandsJobTypes();
        $this->smarty->assign('allBrandsJobTypes', $allBrandsJobTypes);



        switch ($functionAction) {

            case 'insert':

                //We are fetching data for the selected row where user clicks on copy button
                $args['ServiceTypeID'] = $selectedRowId;
                if ($args['ServiceTypeID'] != '') {
                    $model = $this->loadModel('ServiceTypes');
                    $datarow = $model->fetchRow($args);
                    $datarow['ServiceTypeID'] = '';
                    $datarow['BrandID'] = '';
                    $datarow['Status'] = 'Active';
                } else {

                    $datarow = array('ServiceTypeID' => '', 'ServiceTypeCode' => '', 'BrandID' => '', 'JobTypeID' => '', 'ServiceTypeName' => '', 'Code' => '', 'Priority' => '', 'Status' => 'Active');
                }

                $jobTypes = array();


                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else if (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {
                    if ($datarow['ServiceTypeCode'] == '') {
                        $accessErrorFlag = true;
                    }
                }

                $this->smarty->assign('datarow', $datarow);
                $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
                $this->smarty->assign('jobTypes', $jobTypes);


                $htmlCode = $this->smarty->fetch('serviceTypesForm.tpl');

                echo $htmlCode;

                break;


            case 'update':


                $args['ServiceTypeID'] = $selectedRowId;
                $model = $this->loadModel('ServiceTypes');

                $datarow = $model->fetchRow($args);


                $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                //If the super admin updating existing record of skyline he can not change this brand to other brand so we are only fetching skyline for the brand dropdown in the update page.
                if ($datarow['BrandID'] == 1000) {
                    $brands = $Skyline->getBrand(1000);
                    $this->smarty->assign('brands', $brands);
                }

                $this->smarty->assign('datarow', $datarow);

                //Checking user permissons to display the page.
                if ($this->user->SuperAdmin) {
                    $accessErrorFlag = false;
                } else if (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {

                    if (!isset($this->user->Brands[$datarow['BrandID']])) {
                        $accessErrorFlag = true;
                    }
                }


                if ($datarow['BrandID'] && isset($allBrandsJobTypes[$datarow['BrandID']])) {
                    $jobTypes = $allBrandsJobTypes[$datarow['BrandID']];
                } else {
                    $jobTypes = array();
                }

                $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
                $this->smarty->assign('jobTypes', $jobTypes);

                $htmlCode = $this->smarty->fetch('serviceTypesForm.tpl');

                echo $htmlCode;

                break;

            default:

                $this->smarty->display('serviceTypes.tpl');
        }
    }

    /**
     *  
     * This method is used for to manuplate data of User Roles Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * 
     * @author Simon Tsang <s.tsang@pccsuk.com>
     */
    public function rolesAction($args) {

        $functionAction = isset($args[0]) ? $args[0] : '';
        $selectedRowId = isset($args[1]) ? $args[1] : '';
        $RoleStatus = isset($args['RoleStatus']) ? $args['RoleStatus'] : 'Active';


        if ($functionAction == 'tagged') {
            $AccessPermissionsModel = $this->loadModel('AccessPermissions');
            $TaggedCount = $AccessPermissionsModel->userRolePermissions($selectedRowId);

            echo json_encode($TaggedCount);
        } else {
            $Skyline = $this->loadModel('Skyline');


            //If the action is insert then we are getting only skyline brand since user(i.e super admin) can create actions only for skyline (Of course others can copy this action if they want)
            if ($functionAction == "insert" && $selectedRowId == '') {
                $brands = $Skyline->getBrand(1000);
            } else if ($functionAction == "insert" && $selectedRowId != '') {
                $brands = $Skyline->getBrand('', 1000);
            } else {
                // Getting brands (logged in user can see brands which are assinged to him/her )..
                $brands = $Skyline->getBrand();
            }


            $this->smarty->assign('RoleStatus', $RoleStatus);

            $this->smarty->assign('brands', $brands);
            $this->smarty->assign('statuses', $this->statuses);
            $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
            $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
            //$this->smarty->assign('userBrands', $this->user->Brands);//this value should be from user calss
            $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));

            $accessErrorFlag = false;


            $this->page = $this->messages->getPage('userRoles', $this->lang);
            
            #$this->log(var_export($this->page, true));
            $this->smarty->assign('page', $this->page);


            $allBrandsJobTypes = $Skyline->getBrandsJobTypes();
            $this->smarty->assign('allBrandsJobTypes', $allBrandsJobTypes);

            $userTypes = array(
                'Service Network' => 'Service Network',
                'Client' => 'Client',
                'Branch' => 'Branch',
                'Service Provider' => 'Service Provider',
                'Manufacturer' => 'Manufacturer',
                'Extended Warrantor' => 'Extended Warrantor'
            );

            $this->smarty->assign('UserTypes', $userTypes);




            switch ($functionAction) {

                case 'insert':

                    //We are fetching data for the selected row where user clicks on copy button
                    $args['RoleID'] = $selectedRowId;
                    if ($args['RoleID'] != '') {
                        $model = $this->loadModel('UserRoles');
                        $datarow = $model->fetchRow($args);
                        $datarow['RoleID'] = '';
                        $datarow['BrandID'] = '';
                        $datarow['Status'] = 'Active';
                    } else {

                        $datarow = array('RoleID' => '', 'Name' => '', 'UserType' => '', 'Status' => 'Active', 'Comment' => '', 'InactivityTimeout' => 24);
                    }

                    $this->smarty->assign('PermissionTagged', 'Both');


                    $TotalPermissions = $Skyline->getPermissions(array('statusFlag' => false), '');

                    $this->smarty->assign('TotalPermissions', (isset($TotalPermissions['aaData'])) ? $TotalPermissions['aaData'] : false);


                    //Checking user permissons to display the page.
                    if ($this->user->SuperAdmin) {
                        $accessErrorFlag = false;
                    } else if (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {
                        if ($datarow['ServiceTypeCode'] == '') {
                            $accessErrorFlag = true;
                        }
                    }

                    $this->smarty->assign('datarow', $datarow);
                    $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);

                    $htmlCode = $this->smarty->fetch('UserRolesForm.tpl');

                    echo $htmlCode;

                    break;


                case 'update':

                    $args['RoleID'] = $selectedRowId;
                    $model = $this->loadModel('UserRoles');

                    $datarow = $model->fetchRow($args);


                    $permissionsList = $Skyline->getPermissions($_POST, $args['RoleID']);

                    $tCnt = count($permissionsList['aaData']);
                    $Tagged = $Untagged = $Both = 0;

                    for ($i = 0; $i < $tCnt; $i++) {
                        if ($permissionsList['aaData'][$i][3]) {
                            $Tagged++;
                        } else {
                            $Untagged++;
                        }
                    }

                    $Both = $Tagged + $Untagged;


                    $this->smarty->assign('Both', $Both);
                    $this->smarty->assign('Tagged', $Tagged);
                    $this->smarty->assign('Untagged', $Untagged);

                    $this->smarty->assign('PermissionTagged', 'Both');

                    #$this->log('rolesAction-> update'.var_export($datarow, true));


                    $TotalPermissions = $Skyline->getPermissions(array('statusFlag' => false), $args['RoleID']);

                    $this->smarty->assign('TotalPermissions', (isset($TotalPermissions['aaData'])) ? $TotalPermissions['aaData'] : false);


                    $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                    //If the super admin updating existing record of skyline he can not change this brand to other brand so we are only fetching skyline for the brand dropdown in the update page.
                    //                        if($datarow['BrandID']==1000) {
                    //                                $brands = $Skyline->getBrand(1000);
                    //                                $this->smarty->assign('brands', $brands); 
                    //                        }

                    $this->smarty->assign('datarow', $datarow);

                    //Checking user permissons to display the page.
                    if ($this->user->SuperAdmin) {
                        $accessErrorFlag = false;
                    } else if (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {

                        //                            if(!isset($this->user->Brands[$datarow['BrandID']])) {
                        //                                $accessErrorFlag = true;
                        //                            }
                    }


                    //                        if($datarow['BrandID'] && isset($allBrandsJobTypes[$datarow['BrandID']])) {
                    //                             $jobTypes = $allBrandsJobTypes[$datarow['BrandID']];
                    //                        } else {
                    //                            $jobTypes = array();
                    //                        }

                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);

                    $htmlCode = $this->smarty->fetch('UserRolesForm.tpl');

                    echo $htmlCode;

                    break;

                default:

                    $this->smarty->display('userRoles.tpl');
            }
        }
    }

    
    
    public function getRAStatusTypesAction() {

        $RASTModel = $this->loadModel("RAStatusTypes");
        $types = $RASTModel->getRAStatusTypes(false, false, $_POST["brandID"], false, true);
        $subset = $RASTModel->getRAStatusSubset($_POST["brandID"], $_POST["statusCode"]);

        $result = (object) ["types" => $types, "subset" => $subset];

        echo(json_encode($result));
    }

    
    
    /* public function accessoryAction($args) {
        $this->page = $this->messages->getPage('accessory', $this->lang);
        
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('accessory');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
           
        
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $model = $this->loadModel('Accessories');
        $datatable = $this->loadModel('DataTable');
        if (!isset($args[0])) {
            $ut = false;
        } else {
            $ut = $args[0];
        }
        $data = $model->getAccessoriesList($ut);
        $this->smarty->assign('data', $data);
        $keys = $datatable->getAllFieldNames('accessory');


        $this->smarty->assign('data_keys', $keys);


        //to use this function we need provide pageid, it can be set in language config file
        $columnStrings = $datatable->getColumnStrings($this->user->UserID, $this->page['config']['pageID']);
        $columnStringsSA = $datatable->getColumnStrings($this->user->UserID, $this->page['config']['pageID'], 'sa');


        if ($columnStrings) {
            $columnDisplayString = explode(",", $columnStrings['ColumnDisplayString']);
            $columnOrderString = $columnStrings['ColumnOrderString'];
            $columnNameString = explode(",", $columnStrings['ColumnNameString']);
            $columnNameStringSA = explode(",", $columnStringsSA['ColumnNameString']);
        } else {
            $columnDisplayString = array();
            $columnOrderString = "";
            $columnNameString = array();
            $columnNameStringSA = array();
        }

        $Skyline = $this->loadModel('Skyline');
        $unitTypes = $Skyline->getUnitTypes();

        $this->smarty->assign('unitTypes', $unitTypes);




        $this->smarty->assign('uId', $ut);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
        $this->smarty->assign('columnDisplayString', $columnDisplayString);
        $this->smarty->assign('columnOrderString', $columnOrderString);
        $this->smarty->assign('columnNameString', $columnNameString);
        $this->smarty->assign('columnNameStringSA', $columnNameStringSA);
        $this->smarty->assign('nId', 0);
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->display('systemAdmin/accessory.tpl');
    }

    public function processAccessoryAction($args) {
        $this->page = $this->messages->getPage('accessoryForm', $this->lang);
        
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('statuses', $this->statuses);
        $model = $this->loadModel('Accessories');
        if (isset($args['id'])) {
            $datarow = $model->getAccessoryData($args['id']);
        } else {
            $datarow = array(
                'AccessoryName' => '',
                'AccessoryID' => '',
                'Status' => 'Active',
                'UnitTypeID' => '',
            );
        }
        $Skyline = $this->loadModel('Skyline');
        $unitTypes = $Skyline->getUnitTypes();

        $this->smarty->assign('unitTypes', $unitTypes);
        $this->smarty->assign('unitTypes', $unitTypes);
        $this->smarty->assign('datarow', $datarow);
        $this->smarty->display('systemAdmin/accessoryForm.tpl');
    }

    public function saveAccessoryAction($args) {        
        $model = $this->loadModel('Accessories');
        if ($_POST['AccessoryID'] != '') {              
            $model->updateAccessory($_POST);           
        } else {
            $model->insertAccessory($_POST);
        }
        //commented by srinvas
        //$this->redirect('LookupTables/accessory/' . $_POST['UnitTypeID']);
        exit;
    }

    public function deleteAccessoryAction($args) {
        $model = $this->loadModel('Accessories');

        $model->deleteAccessory($args['id']);



        $this->redirect('LookupTablesController', 'accessoryAction');
}   */

    public function colourAction($args) {
        $model = $this->loadModel('Colours');
        $datatable = $this->loadModel('DataTable');

        if ($this->user->SuperAdmin == 0) {
            if (array_key_exists("AP8001", $this->user->Permissions)) {
                $this->session->activeUser = "ServiveProvider";
                $this->session->mainTable = "service_provider_colour";
                $this->session->mainPage = "colour";
                $this->session->secondaryTable = "colour";
            }
        } else {
            $this->session->activeUser = "SuperAdmin";
            $this->session->mainTable = "colour";
            $this->session->mainPage = "colour";
            $this->session->secondaryTable = "service_provider_colour";
        }
        if ($this->user->SuperAdmin == 1) {
            $pendingNo = $datatable->getPendingCount($this->session->mainTable);
            $this->smarty->assign('pendingNo', $pendingNo);
        }
        $this->smarty->assign('activeUser', $this->session->activeUser);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin);
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $this->smarty->assign('page', $this->page);


        $keys_data = $datatable->getAllFieldNames($this->session->mainTable, $this->user->UserID, $this->page['config']['pageID']);
        if (isset($keys_data[1])) {
            $keys = $keys_data[1];
        } else {
            $keys = $keys_data;
        }

        $this->smarty->assign('data_keys', $keys);






        $ut = 0;
        $this->smarty->assign('uId', $ut);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss

        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->display('systemAdmin/colour.tpl');
    }

    public function loadColourTableAction($args) {

        $dd = $_GET;

        $datatable = $this->loadModel('DataTable');

        if (isset($args['inactive'])) {
            $inactive = true;
        } else {
            $inactive = false;
        }
        $table = $this->session->mainTable;
        if ($this->user->ServiceProviderID != "") {
            $sp = $this->user->ServiceProviderID;
            $aproved = "";
        } else {
            $sp = false;
            $aproved = "Approved";
        }
        if (isset($args['unaproved'])) {
            $aproved = "NotApproved";
        }
        if (isset($args['pending'])) {
            $aproved = "Pending";
        }
        if (isset($args['supplierid'])) {
//           $sp=$args['supplierid'];
//           $table=$this->session->secondaryTable;
        }
        ($table == "colour") ? $idcol = "ColourID" : $idcol = "ServiceProviderColourID";
        $columns = array($idcol, "ColourName", "Status");
        //$columns="";
        // print_r($columns);
        $data = $datatable->datatableSS($table, $columns, $dd, '', $extraColumns = array('<input type="checkbox">'), 0, false, $inactive, $sp, $aproved);

        echo json_encode($data);
    }

    public function processcolourAction($args) {
        $this->page = $this->messages->getPage('colour', $this->lang);
        
           //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('colour');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
           
        
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('ServiceProviderID', $this->user->ServiceProviderID);
        $this->smarty->assign('SuperAdmin', $this->user->SuperAdmin);

        $ServiceProviders = $this->loadModel('ServiceProviders');







        //edit,copy
        if (isset($args['id'])) {
            $model = $this->loadModel('Colours');
            if (isset($args['copy'])) {

                $this->smarty->assign('mode', "copyNew");
                $data = $model->getData($args['id'], $this->session->secondaryTable);
            } else {
                $this->smarty->assign('mode', "update");

                $data = $model->getData($args['id'], $this->session->mainTable);
            }


            $this->smarty->assign('datarow', $data);
            if ($this->user->ServiceProviderID == "" && isset($data)) {

                $spid = $model->getIDFromMain($data['ColourID']);

                if ($spid) {
                    $ancronym = $ServiceProviders->getServiceCentreAncronym($spid);
                    $this->smarty->assign('ancronym', $ancronym);
                } else {
                    $this->smarty->assign('ancronym', "");
                }
            }
            $this->smarty->assign('activeUser', $this->session->activeUser);

            $this->smarty->display('systemAdmin/colourForm.tpl');
        } else {
            $datarow = [
                "ColourName" => '',
                "Status" => 'Active',
                "ColourID" => ''
            ];
            $this->smarty->assign('datarow', $datarow);
            if ($this->session->activeUser != "SuperAdmin") {
                if (isset($args['new'])) {
                    $this->smarty->assign('mode', "New");

                    $this->smarty->display('systemAdmin/colourForm.tpl');
                } else {
                    $this->smarty->assign('controller', "LookupTables");
                    $this->smarty->display('systemAdmin/insertNewSelector.tpl');
                }
            } else {
                $this->smarty->assign('mode', "insert");
                $this->smarty->display('systemAdmin/colourForm.tpl');
            }
        }
    }

    public function saveColourAction($args) {
        $model = $this->loadModel('Colours');
        $model->saveColour($_POST, $this->session->mainTable, $this->session->secondaryTable);
        $this->redirect('LookupTablesController', 'colourAction');
    }

    public function deletecolourAction($args) {
        $model = $this->loadModel('Colours');

        $model->deletecolour($args['id'], $this->session->mainTable);



        $this->redirect('LookupTablesController', 'colourAction');
    }

    public function tableDisplayPreferenceSetupAction($args) {
        $this->page = $this->messages->getPage($args['page'], $this->lang);
        $this->smarty->assign('page', $this->page);

        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $model = $this->loadModel('Manufacturers');
        $datatable = $this->loadModel('DataTable');

        $columnStrings = $datatable->getColumnStrings($this->user->UserID, $this->page['config']['pageID'], 'user');
        $columnStringsSA = $datatable->getColumnStrings($this->user->UserID, $this->page['config']['pageID'], 'sa');


        $keys = $datatable->getAllFieldNames($args['table']);
        if (isset($keys[1]) && is_array($keys[1])) {
            $keys = $keys[1];
        } else {
            $keys = $keys;
        }
        $this->smarty->assign('data_keys', $keys);

        if ($columnStrings) {
            $columnDisplayString = explode(",", $columnStrings['ColumnDisplayString']);
            $columnOrderString = explode(',', $columnStrings['ColumnOrderString']);
            $columnNameString = explode(",", $columnStrings['ColumnNameString']);
        } else {
            $columnDisplayString = array();
            $columnOrderString = "";
            $columnNameString = array();
        }

        if ($columnStringsSA) {
            $columnDisplayStringSA = explode(",", $columnStringsSA['ColumnDisplayString']);
            $columnOrderStringSA = explode(',', $columnStringsSA['ColumnOrderString']);
            $columnNameStringSA = explode(",", $columnStringsSA['ColumnNameString']);
            $columnStatusStringSA = explode(",", $columnStringsSA['ColumnStatusString']);
            $this->smarty->assign('columnStatusStringSA', $columnStatusStringSA);
        } else {
            $columnDisplayStringSA = array();
            $columnOrderStringSA = "";
            $columnNameStringSA = array();
        }

        $this->smarty->assign('columnDisplayString', $columnDisplayString);
        $this->smarty->assign('columnOrderString', $columnOrderString);
        $this->smarty->assign('columnNameString', $columnNameString);
        $this->smarty->assign('columnDisplayStringSA', $columnDisplayStringSA);
        $this->smarty->assign('columnOrderStringSA', $columnOrderStringSA);
        $this->smarty->assign('columnNameStringSA', $columnNameStringSA);
        $this->smarty->assign('controller', "LookupTables");
        $this->smarty->assign('typeAction', $args['page']);

        $this->smarty->display('systemAdmin/tableDisplayPreferences.tpl');
    }

    public function saveDisplayPreferencesAction($args) {
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $model = $this->loadModel('DataTable');

        $model->saveDisplayPreferences($this->user->UserID, $this->page['config']['pageID'], $_POST);
        $this->redirect('LookupTablesController', $this->session->mainPage . "Action");
    }

    public function resetDisplayPreferencesAction($args) {
        $typeAction = $args['typeAction'];
        $this->page = $this->messages->getPage($typeAction, $this->lang);
        $model = $this->loadModel('DataTable');

        $model->resetDisplayPreferences($this->user->UserID, $this->page['config']['pageID']);
        $this->redirect('LookupTablesController', $typeAction . "Action");
    }

    public function productCodeAction($args) {
        $this->page = $this->messages->getPage('productCode', $this->lang);
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $model = $this->loadModel('ProductCodes');
        $datatable = $this->loadModel('DataTable');
        if (!isset($args[0])) {
            $ut = false;
        } else {
            $ut = $args[0];
        }
        $data = $model->getProductCodeList($ut);
        $this->smarty->assign('data', $data);
        $keys = $datatable->getAllFieldNames('product_code');


        $this->smarty->assign('data_keys', $keys);


        //to use this function we need provide pageid, it can be set in language config file
        $columnStrings = $datatable->getColumnStrings($this->user->UserID, $this->page['config']['pageID']);
        $columnStringsSA = $datatable->getColumnStrings($this->user->UserID, $this->page['config']['pageID'], 'sa');


        if ($columnStrings) {
            $columnDisplayString = explode(",", $columnStrings['ColumnDisplayString']);
            $columnOrderString = $columnStrings['ColumnOrderString'];
            $columnNameString = explode(",", $columnStrings['ColumnNameString']);
            $columnNameStringSA = explode(",", $columnStringsSA['ColumnNameString']);
        } else {
            $columnDisplayString = array();
            $columnOrderString = "";
            $columnNameString = array();
            $columnNameStringSA = array();
        }





        $this->smarty->assign('uId', $ut);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss
        $this->smarty->assign('columnDisplayString', $columnDisplayString);
        $this->smarty->assign('columnOrderString', $columnOrderString);
        $this->smarty->assign('columnNameString', $columnNameString);
        $this->smarty->assign('columnNameStringSA', $columnNameStringSA);
        $this->smarty->assign('nId', 0);
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->display('systemAdmin/productCode.tpl');
    }

    public function loadProductCodeTableAction($args) {

        $dd = $_GET;

        $datatable = $this->loadModel('DataTable');
        $columns = array("ProductCodeID", "ProductCodeName", "Status");
        //$columns="";

        $data = $datatable->datatableSS("product_code", $columns, $dd, $joins = "", $extraColumns = array());

        echo json_encode($data);
    }

    public function loadAccessoryTableAction($args) {

        $dd = $_GET;

        $datatable = $this->loadModel('DataTable');
        $columns = array("ProductCodeID", "ProductCodeName", "Status");
        //$columns="";

        $data = $datatable->datatableSS("accessory", $columns, $dd, $joins = "", $extraColumns = array());

        echo json_encode($data);
    }

    public function processproductcodeAction($args) {
        $this->page = $this->messages->getPage('productcode', $this->lang);
        
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('productcode');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
           
        
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('statuses', $this->statuses);
        $model = $this->loadModel('ProductCodes');
        if (isset($args['id'])) {
            $datarow = $model->getproductcodeData($args['id']);
        } else {
            $datarow = array(
                'ProductCodeName' => '',
                'ProductCodeID' => '',
                'Status' => 'Active',
            );
        }



        $this->smarty->assign('datarow', $datarow);
        $this->smarty->display('systemAdmin/productcodeForm.tpl');
    }

    public function saveproductcodeAction($args) {

        $model = $this->loadModel('ProductCodes');
        if ($_POST['ProductCodeID'] != '') {
            $model->updateproductcode($_POST);
        } else {
            $model->insertproductcode($_POST);
        }
        $this->redirect('LookupTables/productcode/');
    }

    public function deleteproductcodeAction($args) {
        $model = $this->loadModel('ProductCodes');

        $model->deleteproductcode($args['id']);



        $this->redirect('LookupTablesController', 'productcodeAction');
    }

    public function currencyAction($args) {

        $this->page = $this->messages->getPage('currency', $this->lang);
        
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('currency');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
           
        
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $model = $this->loadModel('Currency');
        $datatable = $this->loadModel('DataTable');
        if (!isset($args[0])) {
            $ut = false;
        } else {
            $ut = $args[0];
        }
        
        
        
        
        
        $keys_data = $datatable->getAllFieldNames('currency', $this->user->UserID, $this->page['config']['pageID']);
        $keys = $keys_data[1];

        $this->smarty->assign('data_keys', $keys);

        $this->smarty->assign('uId', $ut);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('userPermissions', $this->user->Permissions); //this value should be from user calss

        $this->smarty->assign('nId', 0);
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->display('systemAdmin/currency.tpl');
    }

    public function loadCurrencyTableAction($args) {

        
        $dd = $_GET;
        $this->page = $this->messages->getPage('currency', $this->lang);
       
        $datatable = $this->loadModel('DataTable');
        $keys_data = $datatable->getAllFieldNames('currency', $this->user->UserID, $this->page['config']['pageID']);
        $columns = $keys_data[0];
       
         if (isset($args['inactive'])) {
            $inactive = true;
        } else {
            $inactive = false;
        }
         $this->checkUserSPID($args);
         $spid=$this->user->ServiceProviderID;
        // $columns=$datatable->getAllFieldNames('currency');
        //$columns="";
        $extraColumns = ['<input type="checkbox">'];
        $data = $datatable->datatableSS("currency", $columns, $dd, $joins = "", $extraColumns,0, false,$inactive );
       
        echo json_encode($data);
    }

    public function processcurrencyAction($args) {
        $this->page = $this->messages->getPage('currency', $this->lang);
        
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('statuses', $this->statuses);
        $model = $this->loadModel('Currency');
        if (isset($args['id'])) {
            $datarow = $model->getcurrencyData($args['id']);
            $datarow['HistoricAverageCost'] = $model->getHistoricValue($args['id']);
        } else {
            $datarow = array(
                'CurrencyName' => '',
                'CurrencyID' => '',
                'Status' => 'Active',
                'CurrencyCode' => '',
                'CurrencySymbol' => '',
                'AutomaticExchangeRateCalculation' => 'Yes',
                'ConversionCalculation' => 'Multiply',
                'ExchangeRate' => '',
                'ExchangeRateLastModified' => '',
                'HistoricAverageCost' => '',
            );
        }



        $this->smarty->assign('datarow', $datarow);
        $this->smarty->display('systemAdmin/currencyForm.tpl');
    }

    public function showCurrencyHistoryAction($args) {
        $this->page = $this->messages->getPage('currency', $this->lang);
        
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('currency');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
          
        
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('statuses', $this->statuses);
        $model = $this->loadModel('Currency');


        $data = $model->getCurrencyHistory($args['id']);
        $this->smarty->assign('data', $data);

        $this->smarty->display('systemAdmin/currencyHistory.tpl');
    }

    public function savecurrencyAction($args) {

        $model = $this->loadModel('Currency');
        if ($_POST['CurrencyID'] != '') {
            $model->updatecurrency($_POST);
        } else {
            $model->insertcurrency($_POST);
        }
        $this->redirect('LookupTables/currency/');
    }

    public function deletecurrencyAction($args) {
        $model = $this->loadModel('Currency');

        $model->deletecurrency($args['id']);



        $this->redirect('LookupTablesController', 'currencyAction');
    }

    public function partOrderStatusAction($args) {

        $this->page = $this->messages->getPage('partOrderStatus', $this->lang);
        
                            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('partOrderStatus');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
          
        
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $model = $this->loadModel('PartOrderStatus');
        $datatable = $this->loadModel('DataTable');


        $keys = [
            "PartStatusID",
            "Status Name",
            "Description",
            "Status",
            "Service Provider",
        ];


        $this->smarty->assign('data_keys', $keys);

        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->display('systemAdmin/partOrderStatus.tpl');
    }

    public function loadPartOrderStatusTableAction($args) {

        $dd = $_GET;

        $datatable = $this->loadModel('DataTable');

        if (isset($args['inactive'])) {
            $inactive = true;
        } else {
            $inactive = false;
        }
        $columns = [

            "PartStatusID",
            "PartStatusName",
            "PartStatusDescription",
            "part_status.Status",
            "sp.Acronym",
        ];
        $extraColumns = ['<input type="checkbox">'];
        //$columns="";
        // print_r($columns);
        $joins = "left join service_provider sp on sp.ServiceProviderID=part_status.ServiceProviderID ";
        $data = $datatable->datatableSS("part_status", $columns, $dd, $joins, $extraColumns, 0, false, $inactive);

        echo json_encode($data);
    }

    public function processPartOrderStatusAction($args) {
        $skyline_model = $this->loadModel('Skyline');
        $this->page = $this->messages->getPage('partOrderStatus', $this->lang);
       
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('statuses', $this->statuses);
        $model = $this->loadModel('PartOrderStatus');
        $serviceProviders = $skyline_model->getServiceProviders();

        $this->smarty->assign('ServiceProviders', $serviceProviders);

        if (isset($args['id'])) {
            $datarow = $model->getPartOrderStatusData($args['id']);
        } else {
            $datarow = array(
                'PartStatusID' => '',
                'PartStatusName' => '',
                'PartStatusDescription' => '',
                'ServiceProviderID' => '',
                'ServiceProviderID' => '',
                'PartStatusDisplayOrder' => '',
                'DisplayOrderSection' => '',
                'Status' => '',
            );
        }



        $this->smarty->assign('datarow', $datarow);
        $this->smarty->display('systemAdmin/partOrderStatusForm.tpl');
    }

    public function savePartOrderStatusAction($args) {

        $model = $this->loadModel('PartOrderStatus');
        if ($_POST['PartStatusID'] != '') {
            $model->updatePartOrderStatus($_POST);
        } else {
            $model->insertPartOrderStatus($_POST);
        }
        $this->redirect('LookupTables/PartOrderStatus/');
    }

    public function deletePartOrderStatusAction($args) {
        $model = $this->loadModel('PartOrderStatus');

        $model->deletePartOrderStatus($args['id']);



        $this->redirect('LookupTables/PartOrderStatus/');
    }

    public function loadColourListAction($args) {

        $dd = $_GET;
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $datatable = $this->loadModel('DataTable');




        $columns = array("ColourID", "ColourName");



        //$joins="";


        $data = $datatable->datatableSS($this->session->secondaryTable, $columns, $dd, "", array(), 0, false, false, false, "Approved");

        echo json_encode($data);
    }
    ///////////TODO 4
    public function jobFaultCodesAction($args) {
	
	//$this->log($args);
	
        $functionAction = isset($args[0]) ? $args[0] : '';
        $selectedRowId = isset($args[1]) ? $args[1] : '';	
		
	//$this->log('Selected Id: '.$selectedRowId);
	
        $this->page = $this->messages->getPage('jobFaultCodes', $this->lang);
        
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('jobFaultCodes');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
          
        
        $this->smarty->assign('page', $this->page);
        $splist =array();
        $spid=isset($this->user->ServiceProviderID)?$this->user->ServiceProviderID:"";	
        $this->smarty->assign('sId', $spid);	 
	
        if ($this->user->SuperAdmin == 1) {
            $ServiceProviders = $this->loadModel('ServiceProviders');
            $splist = $ServiceProviders->getAllActiveServiceProviders();
            $this->smarty->assign('splist', $splist);
        }
        $this->session->mainTable = "job_fault_codes";
        $this->session->mainPage = "jobFaultCodes";
        $this->session->secondaryTable = "job_fault_codes";
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $model = $this->loadModel('JobFaultCodes');
        $repairTypesModel = $this->loadModel('RepairTypes');
        $datatable = $this->loadModel('DataTable');
        $keys_data = $datatable->getAllFieldNames("job_fault_code", $this->user->UserID, $this->page['config']['pageID']);
        $this->smarty->assign('data_keys', $keys_data[1]);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('statuses', $this->statuses);
        $jobFaultCodeLookup_model = $this->loadModel('JobFaultCodesLookups');
        $repairTypesData = $repairTypesModel->getAllActiveRepairTypes($spid);
        $this->smarty->assign('RepairTypes',$repairTypesData);
        $accessErrorFlag = false;
        switch ($functionAction) {
            case 'insert': {
                    $LookupList = array();
                    $datarow = ["JobFaultCodeID"=>"",
                        "ServiceProviderID"=>"",
                        "RepairTypeID"=>"",
                        "JobFaultCode"=>"",
                        "FieldNo"=>"",
                        "FieldType"=>"",
                        "Lookup"=>"",
                        "MainInFault"=>"",
                        "MainOutFault"=>"",
                        "NonFaultCode"=>"",
                        "UseAsGenericFaultDescription"=>"",
                        "RestrictLength"=>"",
                        "MinLength"=>"",
                        "MaxLength"=>"",
                        "ForceFormat"=>"",
                        "RequiredFormat"=>"",
                        "ReplicateToFaultDescription"=>"",
                        "FillFromDOP"=>"",
                        "HideWhenRelatedCodeIsBlank"=>"",
                        "FaultCodeNo"=>"",
                        "HideForThirdPartyJobs"=>"",
                        "NotReqForThirdPartyJobs"=>"",
                        "ReqAtBookingForChargeableJobs"=>"",
                        "ReqAtCompletionForChargeableJobs"=>"",
                        "ReqAtBookingForWarrantyJobs"=>"",
                        "ReqAtCompletionForWarrantyJobs"=>"",
                        "ReqIfExchangeIssued"=>"",
                        "ReqOnlyForRepairType"=>"",
                        "RepairTypeID"=>"",
                        "UseLookup"=>"",
                        "ForceLookup"=>"",
                        "Status"=>"",
			"ApproveStatus"=>""
                    ];
                    $this->smarty->assign('LookupList', $LookupList);
                    $this->smarty->assign('datarow', $datarow);
                    $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
                    $this->smarty->assign('deleteFlag', "No");
                    $htmlCode = $this->smarty->display('systemAdmin/jobFaultCodesForm.tpl');
                    echo $htmlCode;
                    break;
                }
            case 'update': {
                    $jobFaultCodeLookup_model = $this->loadModel('JobFaultCodesLookups');
                    $model = $this->loadModel('JobFaultCodes');
                    $args["JobFaultCodeID"] = $selectedRowId;		    
                    $LookupList = array();
                    if ($this->user->SuperAdmin != 1) {
                        if (isset($args["JobFaultCodeID"])) {
                           $LookupList = $jobFaultCodeLookup_model->getAllSPLookups($this->user->ServiceProviderID, $datarow['JobFaultCodeID']);
                            // $LookupList = $jobFaultCodeLookup_model->getAllSPLookups($args["JobFaultCodeID"]);
                       }
                    }
                              $this->smarty->assign('LookupList', $LookupList);
                    $datarow = $model->fetchRow($args);
                    $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);
                    $this->smarty->assign('datarow', $datarow);
                    if ($this->user->SuperAdmin) {
                        $accessErrorFlag = false;
                    } elseif (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {
                        if (!isset($this->user->Brands[$datarow['brandID']])) {
                            $accessErrorFlag = true;
                        }
                    }
                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
                    $this->smarty->assign('deleteFlag', "No");
                    $htmlCode = $this->smarty->display('systemAdmin/jobFaultCodesForm.tpl');
                    echo $htmlCode;
                    break;
                }
            case 'deleteForm': {
                    $model = $this->loadModel('JobFaultCodes');
                    $args["JobFaultCodeID"] = $selectedRowId;
                    $LookupList = array();
                    $datarow = $model->fetchRow($args);
                    $this->smarty->assign('form_legend', $this->page['Text']['delete_page_legend']);
                    $this->smarty->assign('datarow', $datarow);
                    if ($this->user->SuperAdmin) {
                        $accessErrorFlag = false;
                    } elseif (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {
                        if (!isset($this->user->Brands[$datarow['brandID']])) {
                            $accessErrorFlag = true;
                        }
                    }
                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
                    $this->smarty->assign('deleteFlag', "Yes");
                    $htmlCode = $this->smarty->display('systemAdmin/jobFaultCodesForm.tpl');
                    echo $htmlCode;
                    break;
                }
                
            default: {
                    if ($this->user->SuperAdmin == 1)			
                        $this->smarty->assign('sId', $functionAction);
			$this->smarty->assign('dataStatus', $selectedRowId);
			//$this->smarty->assign('aproveStatus', $selectedRowId);
		    //$this->log('Default:'.$selectedRowId);
                    $this->smarty->display('systemAdmin/jobFaultCodes.tpl');
                }
        }
    }

    public function loadJobFaultCodesTableAction($args) {
        $this->page = $this->messages->getPage('jobFaultCodes', $this->lang);
        $dd = $_GET;

        $datatable = $this->loadModel('DataTable');


        if ($this->user->ServiceProviderID != "") {
            $sp = $this->user->ServiceProviderID;
        } else {
            $sp = false;
            $sp = 0;
        }
        if (isset($args['supplierid'])) {
            $sp = $args['supplierid'];
        }
        $keys_data = $datatable->getAllFieldNames('job_fault_code', $this->user->UserID, $this->page['config']['pageID']);

        $columns = $keys_data[0];
        $extraColumns = ['<input type="checkbox">'];
        //$columns="";
        // print_r($columns);
        $joins = "";
        $data = $datatable->datatableSS("job_fault_code", $columns, $dd, $joins, $extraColumns, 0, false, $inactive, $sp);

        echo json_encode($data);
    }

    public function processJobFaultCodesAction($args) {
        $skyline_model = $this->loadModel('Skyline');
        $this->page = $this->messages->getPage('jobFaultCodes', $this->lang);
        
                    //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('jobFaultCodes');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
          
        //

	$this->smarty->assign('page', $this->page);
        $this->smarty->assign('statuses', $this->statuses);
        $model = $this->loadModel('JobFaultCodes');
        $serviceProviders = $skyline_model->getServiceProviders();

        $this->smarty->assign('ServiceProviders', $serviceProviders);

        if (isset($args['id'])) {
            $datarow = $model->getJobFaultCodesData($args['id']);
        } else {
            $datarow = array();
        }
        $jobFaultCodeLookup_model = $this->loadModel('JobFaultCodesLookups');

        $LookupList = array();

        if ($this->user->SuperAdmin != 1) {
            if (isset($datarow['JobFaultCodeID'])) {
                $LookupList = $jobFaultCodeLookup_model->getAllSPLookups($this->user->ServiceProviderID, $datarow['JobFaultCodeID']);
            }
        }
        $this->smarty->assign('LookupList', $LookupList);


        $this->smarty->assign('datarow', $datarow);
        $this->smarty->display('systemAdmin/jobFaultCodesForm.tpl');
    }

    public function saveJobFaultCodeAction($args) {

        $model = $this->loadModel('JobFaultCodes');
//        echo"<pre>";
//        print_r($_POST);
//        echo"</pre>";
        if ($_POST['JobFaultCodeID'] != '') {
            $model->updateJobFaultCodes($_POST, $this->user->ServiceProviderID);
        } else {
            $model->insertJobFaultCodes($_POST, $this->user->ServiceProviderID);
        }
        $this->redirect('LookupTables/JobFaultCodes/');
    }

    public function deleteJobFaultCodesAction($args) {
        $model = $this->loadModel('JobFaultCodes');

        $model->deleteJobFaultCodes($args['id']);



        $this->redirect('LookupTables/jobFaultCodes/');
    }

    ///////////TODO 5 jobFaultCodesLookups
    public function jobFaultCodesLookupsAction($args) {
	//$this->log($args);
        $functionAction = isset($args[0]) ? $args[0] : '';
        $selectedRowId = isset($args[1]) ? $args[1] : '';
        $this->session->mainTable = "job_fault_code_lookup";
        $this->session->mainPage = "jobFaultCodesLookups";
        $this->session->secondaryTable = "job_fault_code_lookup";
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $model = $this->loadModel('JobFaultCodesLookups');
        $datatable = $this->loadModel('DataTable');
        $manufacturerModel=$this->loadModel('Manufacturers');
        $repairTypesModel = $this->loadModel('RepairTypes');
        $models_model = $this->loadModel('Models');
        if ($this->user->SuperAdmin == 1) {
            $ServiceProviders = $this->loadModel('ServiceProviders');
            $splist = $ServiceProviders->getAllActiveServiceProviders();
            $this->smarty->assign('splist', $splist);
        }
        $keys_data = $datatable->getAllFieldNames($this->session->mainTable, $this->user->UserID, $this->page['config']['pageID']);
        $modelsList = $models_model->getAllAprovedModels();
        $this->smarty->assign('data_keys', $keys_data[1]);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('statuses', $this->statuses);
        $spid=$this->user->ServiceProviderID;
        if($spid!=0)
            $manufacturerList = $manufacturerModel->getAllSpManufacturers($spid);
        else
            $manufacturerList = $manufacturerModel->getAllManufacturers();
        $this->smarty->assign('sId', $spid);
        $this->smarty->assign('manufacturersList', $manufacturerList);
        $repairTypesData = $repairTypesModel->getAllActiveRepairTypes($spid);
        $this->smarty->assign('RepairTypeList',$repairTypesData);
        $this->smarty->assign('modelsList', $modelsList);
        $accessErrorFlag = false;
        switch ($functionAction) {
            case 'insert': {
                    $LookupList = array();
                    $datarow = ["JobFaultCodeLookupID"=>"",
                        "LookupName"=>"",
                        "Description"=>"",
                        "ManufacturerID"=>"",
                        "ExcludeFromBouncerTable"=>"",
                        "ForcePartFaultCode"=>"",
                        "PartFaultCodeNo"=>"",
                        "JobTypeAvailability"=>"",
                        "PromptForExchange"=>"",
                        "RelatedPartFaultCode"=>"",
                        "RepairTypeIDForChargeable"=>"",
                        "RepairTypeIDForWarranty"=>"",
                        "RestrictLookup"=>"",
                        "RestrictLookupTo"=>"",
                        "ServiceProviderID"=>"",
                        "Status"=>"",
			"ApproveStatus"=>""
                    ];
                    $this->smarty->assign('datarow', $datarow);
                    $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
                    $this->smarty->assign('deleteFlag', "No");
                    $htmlCode = $this->smarty->display('systemAdmin/jobFaultCodesLookupsForm.tpl');
                    echo $htmlCode;
                    break;
                }
            case 'update': {
                    $jobFaultCodeLookup_model = $this->loadModel('JobFaultCodesLookups');
                    $args["JobFaultCodeLookupID"] = $selectedRowId;
                    $datarow = $jobFaultCodeLookup_model->fetchRow($args);
                    $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);
                    $this->smarty->assign('datarow', $datarow);
                    if ($this->user->SuperAdmin) {
                        $accessErrorFlag = false;
                    } elseif (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {
                        if (!isset($this->user->Brands[$datarow['brandID']])) {
                            $accessErrorFlag = true;
                        }
                    }
                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
                    $this->smarty->assign('deleteFlag', "No");
                    $htmlCode = $this->smarty->display('systemAdmin/jobFaultCodesLookupsForm.tpl');
                    echo $htmlCode;
                    break;
                }
            case 'deleteForm': {
                    $jobFaultCodeLookup_model = $this->loadModel('JobFaultCodesLookups');
                    $args["JobFaultCodeLookupID"] = $selectedRowId;
                    $datarow = $jobFaultCodeLookup_model->fetchRow($args);
                    $this->smarty->assign('form_legend', $this->page['Text']['delete_page_legend']);
                    $this->smarty->assign('datarow', $datarow);
                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
                    $this->smarty->assign('deleteFlag', "Yes");
                    $htmlCode = $this->smarty->display('systemAdmin/jobFaultCodesLookupsForm.tpl');
                    echo $htmlCode;
                    break;
                }
                
            default: {
                    if ($this->user->SuperAdmin == 1)
                        $this->smarty->assign('sId', $functionAction);
		    $this->smarty->assign('dataStatus', $selectedRowId); /* assigning status value */
                    $this->smarty->display('systemAdmin/jobFaultCodesLookups.tpl');
                }
        }
    }

    public function loadJobFaultCodesLookupsTableAction($args) {
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $dd = $_GET;

        $datatable = $this->loadModel('DataTable');

        if (isset($args['inactive'])) {
            $inactive = true;
        } else {
            $inactive = false;
        }


        if ($this->user->ServiceProviderID != "") {
            $sp = $this->user->ServiceProviderID;
            $manufacturer = "service_provider_manufacturer";
            $manufacturerID = "ServiceProviderManufacturerID";
        } else {

            $sp = 99999999;
            $manufacturer = "manufacturer";
            $manufacturerID = "ManufacturerID";
            $this->user->ServiceProviderID = null;
        }
        if (isset($args['serviceProviderID'])) {
            $sp = $args['serviceProviderID'];
            $this->user->ServiceProviderID = $sp;
        }

        $keys_data = $datatable->getAllFieldNames($this->session->mainTable, $this->user->UserID, $this->page['config']['pageID']);

        $columns = $keys_data[0];
        $extraColumns = ['<input type="checkbox">'];
        //$columns="";
        // print_r($columns);
        $table = $this->session->mainTable;
        $joins = " left join $manufacturer mm on mm.$manufacturerID=$table.ServiceProviderManufacturerID";
        // $joins = " ";
        $data = $datatable->datatableSS($table, $columns, $dd, $joins, $extraColumns, 0, false, $inactive, $sp);

        echo json_encode($data);
    }

    public function processJobFaultCodesLookupsAction($args) {
        $skyline_model = $this->loadModel('Skyline');
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('statuses', $this->statuses);
        $model = $this->loadModel('JobFaultCodesLookups');
        $serviceProviders = $skyline_model->getServiceProviders();
        $models_model = $this->loadModel('Models');
        $manufacturers_model = $this->loadModel('Manufacturers');
        if ($this->user->SuperAdmin == 1) {
            $modelsList = $models_model->getAllAprovedModels();
            $manufacturersList = $manufacturers_model->getAllAprovedManufacturers();
        } else {
            $modelsList = $models_model->getAllSPModels($this->user->ServiceProviderID);
            $manufacturersList = $manufacturers_model->getAllSPManufacturers($this->user->ServiceProviderID);
        }
        $this->smarty->assign('ServiceProviders', $serviceProviders);
        $this->smarty->assign('modelsList', $modelsList);
        $this->smarty->assign('manufacturersList', $manufacturersList);
        $this->smarty->assign('ServiceProviderID', $this->user->ServiceProviderID);

        if (isset($args['id'])) {
            $datarow = $model->getJobFaultCodesLookupsData($args['id']);
        } else {
            $datarow = array();
        }



        $this->smarty->assign('datarow', $datarow);
        $this->smarty->display('systemAdmin/jobFaultCodesLookupsForm.tpl');
    }

    public function saveJobFaultCodesLookupAction($args) {

        $model = $this->loadModel('JobFaultCodesLookups');
//        echo"<pre>";
//        print_r($_POST);
//        echo"</pre>";
        if ($_POST['JobFaultCodeLookupID'] != '') {
            $model->updateJobFaultCodesLookups($_POST, $this->user->ServiceProviderID);
        } else {
            $model->insertJobFaultCodesLookups($_POST, $this->user->ServiceProviderID);
        }
        $this->redirect('LookupTables/JobFaultCodesLookups/');
    }

    public function deleteJobFaultCodesLookupsAction($args) {
        $model = $this->loadModel('JobFaultCodesLookups');

        $model->deleteJobFaultCodesLookups($args['id']);



        $this->redirect('LookupTables/jobFaultCodesLookups/');
    }

    public function addLinkedItemAction($args) {
        switch ($_POST['ItemGroup']) {
            case "restrictedModels": {
                    $model = $this->loadModel('JobFaultCodesLookups');
                    break;
                }
            case "jobpartscoudelookups": {
                    $model = $this->loadModel('JobFaultCodes');
                    break;
                }
            case "PartFaultCodesLookups": {
                    $model = $this->loadModel('PartFaultCodesLookups');
                    break;
                }
        }

        $model->addLinkedItem($_POST['PID'], $_POST['CID']);
    }

    public function loadLinkedItemAction($args) {
        switch ($_POST['ItemGroup']) {
            case "restrictedModels": {
                    $model = $this->loadModel('JobFaultCodesLookups');
                    break;
                }
            case "jobpartscoudelookups": {
                    $model = $this->loadModel('JobFaultCodes');
                    break;
                }
        }

        $data = $model->loadLinkedItem($_POST['PID']);
        echo json_encode($data);
    }

    public function delLinkedItemAction($args) {
        switch ($_POST['ItemGroup']) {
            case "restrictedModels": {
                    $model = $this->loadModel('JobFaultCodesLookups');
                    break;
                }
            case "jobpartscoudelookups": {
                    $model = $this->loadModel('JobFaultCodes');
                    break;
                }
        }

        $model->delLinkedItem($_POST['PID'], $_POST['CID']);
    }
    
     ///////////TODO 6 
    public function partFaultCodesAction($args) {


        $this->session->mainTable = "part_fault_code";
        $this->session->mainPage = "partFaultCodes";
        $this->session->secondaryTable = "part_fault_code";
        $this->page = $this->messages->getPage('partFaultCodes', $this->lang);
        
           //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('partFaultCodes');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
          
        
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $model = $this->loadModel('PartFaultCodes');
        $datatable = $this->loadModel('DataTable');
        if ($this->user->SuperAdmin == 1) {


            $ServiceProviders = $this->loadModel('ServiceProviders');
            $splist = $ServiceProviders->getAllActiveServiceProviders();
            $this->smarty->assign('splist', $splist);
        }


        $keys_data = $datatable->getAllFieldNames("part_fault_code", $this->user->UserID, $this->page['config']['pageID']);



        $this->smarty->assign('data_keys', $keys_data[1]);

        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->display('systemAdmin/partFaultCodes.tpl');
    }

    public function loadPartFaultCodesTableAction($args) {
        $this->page = $this->messages->getPage('partFaultCodes', $this->lang);
        $dd = $_GET;

        $datatable = $this->loadModel('DataTable');

        if (isset($args['inactive'])) {
            $inactive = true;
        } else {
            $inactive = false;
        }


        if ($this->user->ServiceProviderID != "") {
            $sp = $this->user->ServiceProviderID;
        } else {
            $sp = false;
            $sp = 0;
        }
        if (isset($args['supplierid'])) {
            $sp = $args['supplierid'];
        }
        $keys_data = $datatable->getAllFieldNames('part_fault_code', $this->user->UserID, $this->page['config']['pageID']);

        $columns = $keys_data[0];
        
        $extraColumns = ['<input type="checkbox">'];
        //$columns="";
        // print_r($columns);
        //Used left join for getting the Manufacture name from manufacturer table 
          //$pfc="part_fault_code";
        $joins = "LEFT JOIN manufacturer ma on ma.ManufacturerID = part_fault_code.ManufacturerID";
       
        $data = $datatable->datatableSS("part_fault_code", $columns, $dd, $joins, $extraColumns, 0, false, $inactive, $sp);

        echo json_encode($data);
    }

    public function processPartFaultCodesAction($args) {
        $skyline_model = $this->loadModel('Skyline');
        $this->page = $this->messages->getPage('partFaultCodes', $this->lang);
        
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('partFaultCodes');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
          
        
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('statuses', $this->statuses);
        $model = $this->loadModel('PartFaultCodes');
        $serviceProviders = $skyline_model->getServiceProviders();

        $this->smarty->assign('ServiceProviders', $serviceProviders);

        if (isset($args['id'])) {
            $datarow = $model->getPartFaultCodesData($args['id']);
        } else {
            $datarow = array();
        }
        $partFaultCode_model = $this->loadModel('PartFaultCodes');

        $List = array();

        if ($this->user->SuperAdmin != 1) {
            if (isset($datarow['PartFaultCodeID'])) {
                $List = $partFaultCode_model->getAllSP($this->user->ServiceProviderID, $datarow['PartFaultCodeID']);
            }
        }
        $this->smarty->assign('List', $List);

       //manufacturer list
        $manufacturerModel=$this->loadModel('Manufacturers');
        $manufacturerList = $manufacturerModel->getAllManufacturers();        
        $this->smarty->assign('manufacturerList', $manufacturerList);
        
        $this->smarty->assign('datarow', $datarow);
        $this->smarty->display('systemAdmin/partFaultCodesForm.tpl');
    }

    public function savePartFaultCodeAction($args) {

        $model = $this->loadModel('PartFaultCodes');
//        echo"<pre>";
//        print_r($_POST);
//        echo"</pre>";
        if ($_POST['PartFaultCodeID'] != '') {
            $model->updatePartFaultCodes($_POST, $this->user->ServiceProviderID);
        } else {
            $model->insertPartFaultCodes($_POST, $this->user->ServiceProviderID);
        }
        $this->redirect('LookupTables/PartFaultCodes/');
    }

    public function deletePartFaultCodesAction($args) {
        $model = $this->loadModel('PartFaultCodes');

        $model->deletePartFaultCodes($args['id']);



        $this->redirect('LookupTables/partFaultCodes/');
    }
    
    
      ///////////TODO 7 
    public function partFaultCodesLookupsAction($args) {


        $this->session->mainTable = "part_fault_code_lookup";
        $this->session->mainPage = "partFaultCodesLookups";
        $this->session->secondaryTable = "part_fault_code_lookup";
        $this->page = $this->messages->getPage('partFaultCodesLookups', $this->lang);
        
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('partFaultCodesLookups');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
          
        
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $model = $this->loadModel('PartFaultCodesLookups');
        $datatable = $this->loadModel('DataTable');
        if ($this->user->SuperAdmin == 1) {


            $ServiceProviders = $this->loadModel('ServiceProviders');
            $splist = $ServiceProviders->getAllActiveServiceProviders();
            $this->smarty->assign('splist', $splist);
        }


        $keys_data = $datatable->getAllFieldNames($this->session->mainTable, $this->user->UserID, $this->page['config']['pageID']);



        $this->smarty->assign('data_keys', $keys_data[1]);

        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->display('systemAdmin/partFaultCodesLookups.tpl');
    }

    public function loadPartFaultCodesLookupsTableAction($args) {
        $this->page = $this->messages->getPage('partFaultCodesLookups', $this->lang);
        $dd = $_GET;

        $datatable = $this->loadModel('DataTable');

        if (isset($args['inactive'])) {
            $inactive = true;
        } else {
            $inactive = false;
        }


        if ($this->user->ServiceProviderID != "") {
            $sp = $this->user->ServiceProviderID;
        } else {
            $sp = false;
            $sp = 0;
        }
        if (isset($args['supplierid'])) {
            $sp = $args['supplierid'];
        }
        $keys_data = $datatable->getAllFieldNames($this->session->mainTable, $this->user->UserID, $this->page['config']['pageID']);

        $columns = $keys_data[0];
        //$this->log($columns);
        $extraColumns = ['<input type="checkbox">'];
        //$columns="";
        // print_r($columns);
        //$pfcl="part_fault_code_lookup";
        $joins = "LEFT JOIN manufacturer ma on ma.ManufacturerID = part_fault_code_lookup.ManufacturerID";
        $data = $datatable->datatableSS($this->session->mainTable, $columns, $dd, $joins, $extraColumns, 0, false, $inactive, $sp);

        echo json_encode($data);
    }

    public function processPartFaultCodesLookupsAction($args) {
        $skyline_model = $this->loadModel('Skyline');
        $this->page = $this->messages->getPage('partFaultCodesLookups', $this->lang);
        
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('partFaultCodesLookups');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
          
        
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('statuses', $this->statuses);
        $model = $this->loadModel('PartFaultCodesLookups');
        $serviceProviders = $skyline_model->getServiceProviders();

        $this->smarty->assign('ServiceProviders', $serviceProviders);

        if (isset($args['id'])) {
            $datarow = $model->getPartFaultCodesLookupsData($args['id']);
            $modelsList = $model->getPartFaultCodesLookupsModelData($args['id']);
             $this->smarty->assign('modelsList', $modelsList);
        } else {
            $datarow = array();
        }
        $partFaultCode_model = $this->loadModel('PartFaultCodesLookups');

        $List = array();

        if ($this->user->SuperAdmin != 1) {
            if (isset($datarow['PartFaultCodeLookupID'])) {
                
                $List = $partFaultCode_model->getAllSP($this->user->ServiceProviderID, $datarow['PartFaultCodeLookupID']);
        
            }
        }
        $this->smarty->assign('List', $List);
       //manufacturer list
        $manufacturerModel=$this->loadModel('Manufacturers');
        $manufacturerList = $manufacturerModel->getAllManufacturers();        
        $this->smarty->assign('manufacturerList', $manufacturerList);   
       // $pmid="1";
              //  $modelsList = $partFaultCode_model->getPartFaultCodesLookupsModelData($pmid);
       
                       
        $this->smarty->assign('datarow', $datarow);
        $this->smarty->display('systemAdmin/partFaultCodesLookupsForm.tpl');
    }

    public function savePartFaultCodeLookupAction($args) {

        
        $model = $this->loadModel('PartFaultCodesLookups');
//        echo"<pre>";
//        print_r($_POST);
//        echo"</pre>";
        if ($_POST['PartFaultCodeLookupID'] != '') {
            $model->updatePartFaultCodesLookups($_POST, $this->user->ServiceProviderID);
        } else {
            $model->insertPartFaultCodesLookups($_POST, $this->user->ServiceProviderID);
        }
        
        $this->redirect('LookupTables/PartFaultCodesLookups/');
    }

    public function deletePartFaultCodesLookupsAction($args) {
        $model = $this->loadModel('PartFaultCodesLookups');

        $model->deletePartFaultCodesLookups($args['id']);



        $this->redirect('LookupTables/partFaultCodesLookups/');
    }
    
     ///////////TODO 8
    public function partLocationsAction($args) {


        $this->session->mainTable = "service_provider_part_location";
        $this->session->mainPage = "partLocations";
        $this->session->secondaryTable = "service_provider_part_location";
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $model = $this->loadModel('PartLocations');
        $datatable = $this->loadModel('DataTable');
        if ($this->user->SuperAdmin == 1) {


            $ServiceProviders = $this->loadModel('ServiceProviders');
            $splist = $ServiceProviders->getAllActiveServiceProviders();
            $this->smarty->assign('splist', $splist);
        }
        $this->log($this->page['config']['pageID']);

        $keys_data = $datatable->getAllFieldNames($this->session->mainTable, $this->user->UserID, $this->page['config']['pageID']);



        $this->smarty->assign('data_keys', $keys_data[1]);

        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->display('systemAdmin/partLocations.tpl');
    }

    public function loadPartLocationTableAction($args) {
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $dd = $_GET;

        $datatable = $this->loadModel('DataTable');

        if (isset($args['inactive'])) {
            $inactive = true;
        } else {
            $inactive = false;
        }


        if ($this->user->ServiceProviderID != "") {
            $sp = $this->user->ServiceProviderID;
        } else {
            $sp = false;
            $sp = 0;
        }
        if (isset($args['supplierid'])) {
            $sp = $args['supplierid'];
        }
        $keys_data = $datatable->getAllFieldNames($this->session->mainTable, $this->user->UserID, $this->page['config']['pageID']);

        $columns = $keys_data[0];
        $extraColumns = ['<input type="checkbox">'];
        //$columns="";
        // print_r($columns);
        $joins = "";
        $data = $datatable->datatableSS($this->session->mainTable, $columns, $dd, $joins, $extraColumns, 0, false, $inactive, $sp);

        echo json_encode($data);
    }

    public function processPartLocationAction($args) {
        $skyline_model = $this->loadModel('Skyline');
        $this->page = $this->messages->getPage('partLocations', $this->lang);
        
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('partLocations');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
          
        
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('statuses', $this->statuses);
        $model = $this->loadModel('PartLocations');
        $serviceProviders = $skyline_model->getServiceProviders();

        $this->smarty->assign('ServiceProviders', $serviceProviders);

        if (isset($args['id'])) {
            $datarow = $model->getPartLocationsData($args['id']);
        } else {
            $datarow = array();
        }
        $partFaultCode_model = $this->loadModel('PartLocations');

        $List = array();

        if ($this->user->SuperAdmin != 1) {
            if (isset($datarow['ServiceProviderPartLocationID'])) {
                $List = $partFaultCode_model->getAllSP($this->user->ServiceProviderID, $datarow['ServiceProviderPartLocationID']);
            }
        }
        $this->smarty->assign('List', $List);


        $this->smarty->assign('datarow', $datarow);
        $this->smarty->display('systemAdmin/partLocationForm.tpl');
    }

    public function savePartLocationAction($args) {

        $model = $this->loadModel('PartLocations');
//        echo"<pre>";
//        print_r($_POST);
//        echo"</pre>";
        if ($_POST['ServiceProviderPartLocationID'] != '') {
            $model->updatePartLocations($_POST, $this->user->ServiceProviderID);
        } else {
            $model->insertPartLocations($_POST, $this->user->ServiceProviderID);
        }
        $this->redirect('LookupTables/PartLocations/');
    }

    public function deletePartLocationAction($args) {
        $model = $this->loadModel('PartLocations');

        $model->deletePartLocations($args['id']);



        $this->redirect('LookupTables/partLocations/');
    }
    
    ///////////TODO 9
    public function shelfLocationsAction($args) {


        $this->session->mainTable = "service_provider_shelf_location";
        $this->session->mainPage = "shelfLocations";
        $this->session->secondaryTable = "service_provider_shelf_location";
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $model = $this->loadModel('ShelfLocations');
        $datatable = $this->loadModel('DataTable');
        if ($this->user->SuperAdmin == 1) {


            $ServiceProviders = $this->loadModel('ServiceProviders');
            $splist = $ServiceProviders->getAllActiveServiceProviders();
            $this->smarty->assign('splist', $splist);
        }
        $this->log($this->page['config']['pageID']);

        $keys_data = $datatable->getAllFieldNames($this->session->mainTable, $this->user->UserID, $this->page['config']['pageID']);



        $this->smarty->assign('data_keys', $keys_data[1]);

        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->display('systemAdmin/shelfLocations.tpl');
    }

    public function loadShelfLocationTableAction($args) {
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $dd = $_GET;

        $datatable = $this->loadModel('DataTable');

        if (isset($args['inactive'])) {
            $inactive = true;
        } else {
            $inactive = false;
        }


        if ($this->user->ServiceProviderID != "") {
            $sp = $this->user->ServiceProviderID;
        } else {
            $sp = false;
            $sp = 0;
        }
        if (isset($args['supplierid'])) {
            $sp = $args['supplierid'];
        }
        $keys_data = $datatable->getAllFieldNames($this->session->mainTable, $this->user->UserID, $this->page['config']['pageID']);

        $columns = $keys_data[0];
        $extraColumns = ['<input type="checkbox">'];
        //$columns="";
        // print_r($columns);
        $joins = "";
        $data = $datatable->datatableSS($this->session->mainTable, $columns, $dd, $joins, $extraColumns, 0, false, $inactive, $sp);

        echo json_encode($data);
    }

    public function processShelfLocationAction($args) {
        $skyline_model = $this->loadModel('Skyline');
        $this->page = $this->messages->getPage('shelfLocations', $this->lang);
        
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('shelfLocations');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
          
        
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('statuses', $this->statuses);
        $model = $this->loadModel('ShelfLocations');
        $serviceProviders = $skyline_model->getServiceProviders();

        $this->smarty->assign('ServiceProviders', $serviceProviders);

        if (isset($args['id'])) {
            $datarow = $model->getShelfLocationsData($args['id']);
        } else {
            $datarow = array();
        }
        $shelfFaultCode_model = $this->loadModel('ShelfLocations');

        $List = array();

        if ($this->user->SuperAdmin != 1) {
            if (isset($datarow['ServiceProviderShelfLocationID'])) {
                $List = $shelfFaultCode_model->getAllSP($this->user->ServiceProviderID, $datarow['ServiceProviderShelfLocationID']);
            }
        }
        $this->smarty->assign('List', $List);


        $this->smarty->assign('datarow', $datarow);
        $this->smarty->display('systemAdmin/shelfLocationForm.tpl');
    }

    public function saveShelfLocationAction($args) {

        $model = $this->loadModel('ShelfLocations');
//        echo"<pre>";
//        print_r($_POST);
//        echo"</pre>";
        if ($_POST['ServiceProviderShelfLocationID'] != '') {
            $model->updateShelfLocations($_POST, $this->user->ServiceProviderID);
        } else {
            $model->insertShelfLocations($_POST, $this->user->ServiceProviderID);
        }
        $this->redirect('LookupTables/ShelfLocations/');
    }

    public function deleteShelfLocationAction($args) {
        $model = $this->loadModel('ShelfLocations');

        $model->deleteShelfLocations($args['id']);



        $this->redirect('LookupTables/shelfLocations/');
    }
    
    ///////////TODO 10
    public function repairTypesAction($args) {
        $functionAction = isset($args[0]) ? $args[0] : '';
        $selectedRowId = isset($args[1]) ? $args[1] : '';	
        $this->page = $this->messages->getPage('repairTypes', $this->lang);
        
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('repairTypes');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
          
        
        $this->smarty->assign('page', $this->page);
        $this->checkUserSPID($args);
        $spid=$this->user->ServiceProviderID;
        //manufacturer list
        $manufacturerModel=$this->loadModel('Manufacturers');
        if($spid!=0)
            $manufacturerList = $manufacturerModel->getAllSpManufacturers($spid);
        else
            $manufacturerList = $manufacturerModel->getAllManufacturers();
        $this->smarty->assign('sId', $spid);
        $this->smarty->assign('manufacturerList', $manufacturerList);
        $this->session->mainTable = "repair_type";
        $this->session->mainPage = "repairTypes";
        $this->session->secondaryTable = "service_provider_repair_type";
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $model = $this->loadModel('RepairTypes');
        $datatable = $this->loadModel('DataTable');
        if ($this->user->SuperAdmin == 1) {
            $ServiceProviders = $this->loadModel('ServiceProviders');
            $splist = $ServiceProviders->getAllActiveServiceProviders();
            $this->smarty->assign('splist', $splist);
        }
        $this->log($this->page['config']['pageID']);
        $keys_data = $datatable->getAllFieldNames($this->session->mainTable, $this->user->UserID, $this->page['config']['pageID']);
        $this->smarty->assign('data_keys', $keys_data[1]);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('statuses', $this->statuses);
        $categoryTypesArray = array(0=>'Repair',1=>'Modification',2=>'Exchange',3=>'LiquidDamage',4=>'NFF',5=>'BER',6=>'RTM',7=>'RNR',8=>'Excluded',9=>'Exchange Estimate',10=>'Exchange Fee',11=>'Handling Fee');
        $this->smarty->assign('categoryType', $categoryTypesArray);
        $this->smarty->assign('categoryTypeCount', count($categoryTypesArray)-1);
        $accessErrorFlag = false;
        switch ($functionAction) {
            case 'insert': {
                    $datarow = ["RepairTypeID"=>"", 
                        "RepairType"=>"",
                        "ServiceProviderID"=>"",
                        "ManufacturerID"=>"",
                        "JobWeighting"=>"",
                        "RepairIndex"=>"", 
                        "Chargeable"=>"",
                        "Warranty"=>"", 
                        "WarrantyCode"=>"",
                        "CompulsoryFaultCoding"=>"", 
                        "ExcludeFromEDI"=>"",
                        "ForceAdjustmentIfNoPartsUsed"=>"", 
                        "ExcludeFromInvoicing"=>"",
                        "PromptForExchange"=>"", 
                        "NoParts"=>"",
                        "ScrapExchange"=>"", 
                        "ExcludeRRCHandlingFee"=>"",
                        "Unavailable"=>"",
                        "Type"=>"", 
                        "Status"=>""
                    ];
                    $this->smarty->assign('datarow', $datarow);
                    $model = $this->loadModel('AlternativeFieldNames');
                    $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
                    $this->smarty->assign('deleteFlag', "No");
                    $htmlCode = $this->smarty->fetch('systemAdmin/repairTypeForm.tpl');
                    echo $htmlCode;
                    break;
                }
            case 'update': {
                    $args["RepairTypeID"] = $selectedRowId;
                    $model = $this->loadModel('RepairTypes');
                    $datarow = $model->fetchRow($args);
                    $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);
                    $this->smarty->assign('datarow', $datarow);
                    if ($this->user->SuperAdmin) {
                        $accessErrorFlag = false;
                    } elseif (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {
                        if (!isset($this->user->Brands[$datarow['brandID']])) {
                            $accessErrorFlag = true;
                        }
                    }
                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
                    $this->smarty->assign('deleteFlag', "No");
                    $htmlCode = $this->smarty->fetch('systemAdmin/repairTypeForm.tpl');
                    echo $htmlCode;
                    break;
                }
            case 'deleteForm': {
                    $args["RepairTypeID"] = $selectedRowId;
                    $model = $this->loadModel('RepairTypes');
                    $datarow = $model->fetchRow($args);
                    $this->smarty->assign('form_legend', $this->page['Text']['delete_page_legend']);
                    $this->smarty->assign('datarow', $datarow);
                    if ($this->user->SuperAdmin) {
                        $accessErrorFlag = false;
                    } elseif (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {
                        if (!isset($this->user->Brands[$datarow['brandID']])) {
                            $accessErrorFlag = true;
                        }
                    }
                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
                    $this->smarty->assign('deleteFlag', "Yes");
		    $this->smarty->assign('dataStatus', $selectedRowId);
                    $htmlCode = $this->smarty->fetch('systemAdmin/repairTypeForm.tpl');
                    echo $htmlCode;
                    break;
                }
                
            default: {
                    if ($this->user->SuperAdmin == 1)
                        $this->smarty->assign('sId', $functionAction);
                    $this->smarty->assign('mId', $selectedRowId);
		    $this->smarty->assign('dataStatus', $selectedRowId);
                    $this->smarty->display('systemAdmin/repairType.tpl');
                }
        }
    }

    public function loadrepairTypeTableAction($args) {
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $dd = $_GET;

        $datatable = $this->loadModel('DataTable');

        if (isset($args['inactive'])) {
            $inactive = true;
        } else {
            $inactive = false;
        }


        if ($this->user->ServiceProviderID != "") {
            $sp = $this->user->ServiceProviderID;
            $spp="ServiceProvider";
        } else {
            $sp = false;
            $sp = 0;
            $spp="";
        }
        if (isset($args['supplierid'])) {
            $sp = $args['supplierid'];
        }
        $where="";
         if (isset($args['mid'])) {
            $manufac=$args['mid'];//manufacturer id
            
        }else{
            $manufac=0;//if not selected from dropdown
        }
        $where.=" and ".$this->session->mainTable.".".$spp."ManufacturerID=$manufac";
        $keys_data = $datatable->getAllFieldNames($this->session->mainTable, $this->user->UserID, $this->page['config']['pageID']);

        $columns = $keys_data[0];
        $extraColumns = ['<input type="checkbox">'];
        //$columns="";
        // print_r($columns);
        $joins = "";
        $data = $datatable->datatableSS($this->session->mainTable, $columns, $dd, $joins, $extraColumns, 0, false, $inactive, $sp,'','',$where);

        echo json_encode($data);
    }

    public function processrepairTypeAction($args) {
	$this->log($args);
        $skyline_model = $this->loadModel('Skyline');
	$selectedRowId = isset($args[2]) ? $args[2] : '';	
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('statuses', $this->statuses);
        $model = $this->loadModel('RepairTypes');
        $serviceProviders = $skyline_model->getServiceProviders();

        $this->smarty->assign('ServiceProviders', $serviceProviders);

        if (isset($args['id'])) {
            $datarow = $model->getrepairTypeData($args['id']);
        } else {
            $datarow = array();
        }
        $repairTypeFaultCode_model = $this->loadModel('RepairTypes');

        $List = array();

        if ($this->user->SuperAdmin != 1) {
            if (isset($datarow['ServiceProviderRepairTypeID'])) {
                $List = $repairTypeFaultCode_model->getAllSP($this->user->ServiceProviderID, $datarow['ServiceProviderRepairTypeID']);
            }
        }
        $this->smarty->assign('List', $List);


        $this->smarty->assign('datarow', $datarow);
	$this->smarty->assign('dataStatus', $selectedRowId);
        $this->smarty->display('systemAdmin/repairTypeForm.tpl');
    }

    public function saverepairTypeAction($args) {

        $model = $this->loadModel('RepairTypes');
//        echo"<pre>";
//        print_r($_POST);
//        echo"</pre>";
        if ($_POST['ServiceProviderRepairTypeID'] != '') {
            $model->updaterepairType($_POST, $this->user->ServiceProviderID);
        } else {
            $model->insertrepairType($_POST, $this->user->ServiceProviderID);
        }
        $this->redirect('LookupTables/repairType/');
    }

    public function deleterepairTypeAction($args) {
        $model = $this->loadModel('RepairTypes');
        $model->deleterepairType($_POST["id"]);
    }
    
   
    
     ///////////TODO 11
    public function partCategoriesAction($args) {
	$this->checkUserSPID($args);
	$selectedRowId = isset($args[1]) ? $args[1] : '';
	$spid=$this->user->ServiceProviderID;

        $this->session->mainTable = "service_provider_part_category";
        $this->session->mainPage = "partCategories";
        $this->session->secondaryTable = "service_provider_part_category";
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
      
        $datatable = $this->loadModel('DataTable');
        if ($this->user->SuperAdmin == 1) {


            $ServiceProviders = $this->loadModel('ServiceProviders');
            $splist = $ServiceProviders->getAllActiveServiceProviders();
            $this->smarty->assign('splist', $splist);
        }
        $this->log($this->page['config']['pageID']);

        $keys_data = $datatable->getAllFieldNames($this->session->mainTable, $this->user->UserID, $this->page['config']['pageID']);

        $this->smarty->assign('data_keys', $keys_data[1]);

        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('statuses', $this->statuses);
	/* Updated by Praveen Kumar N */
	$this->smarty->assign('dataStatus', $selectedRowId);
	$this->smarty->assign('sId', $spid);
        $this->smarty->display('systemAdmin/partCategory.tpl');
    }

    public function loadPartCategoryTableAction($args) {
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $dd = $_GET;

        $datatable = $this->loadModel('DataTable');

        if (isset($args['inactive'])) {
            $inactive = true;
        } else {
            $inactive = false;
        }
	/* updated by Praveen Kumar N */
	if (isset($args['unapproved'])) {
            $unapproved = true;
        } else {
            $unapproved = false;
        }
	/* end */   

        if ($this->user->ServiceProviderID != "") {
            $sp = $this->user->ServiceProviderID;
        } else {
            $sp = false;
            $sp = 0;
        }
        if (isset($args['supplierid'])) {
            $sp = $args['supplierid'];
        }
        $keys_data = $datatable->getAllFieldNames($this->session->mainTable, $this->user->UserID, $this->page['config']['pageID']);

        $columns = $keys_data[0];
        $extraColumns = ['<input type="checkbox">'];
        //$columns="";
        // print_r($columns);
        $table=$this->session->mainTable;
        $joins = " left join service_provider sp on sp.ServiceProviderID=$table.ServiceProviderID";
        $data = $datatable->datatableSS($table, $columns, $dd, $joins, $extraColumns, 0, false, $inactive, $unapproved, $sp);

        echo json_encode($data);
    }

    public function processPartCategoryAction($args) {
        $skyline_model = $this->loadModel('Skyline');
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('statuses', $this->statuses);
          $model = $this->loadModel('PartCategories');
        $serviceProviders = $skyline_model->getServiceProviders();

        $this->smarty->assign('ServiceProviders', $serviceProviders);

        if (isset($args['id'])) {
            $datarow = $model->getpartCategoryData($args['id']);
        } else {
            $datarow = array();
        }
       

        $List = array();

        if ($this->user->SuperAdmin != 1) {
            if (isset($datarow['ServiceProviderPartCategoryID'])) {
                $List = $model->getAllSP($this->user->ServiceProviderID, $datarow['ServiceProviderPartCategoryID']);
            }
        }
        $this->smarty->assign('List', $List);


        $this->smarty->assign('datarow', $datarow);
        $this->smarty->display('systemAdmin/partCategoryForm.tpl');
    }

    public function savePartCategoryAction($args) {

        $model = $this->loadModel('PartCategories');
//        echo"<pre>";
//        print_r($_POST);
//        echo"</pre>";
        if ($_POST['ServiceProviderPartCategoryID'] != '') {
            $model->updatepartCategory($_POST, $this->user->ServiceProviderID);
        } else {
            $model->insertpartCategory($_POST, $this->user->ServiceProviderID);
        }
        $this->redirect('LookupTables/partCategories/');
    }

    public function deletePartCategoryAction($args) {
        $model = $this->loadModel('PartCategories');

        $model->deletepartCategory($args['id']);



        $this->redirect('LookupTables/partCategories/');
    }
    
    
    ///////////TODO 12
    public function partSubCategoriesAction($args) {
 $this->checkUserSPID($args);
$spid=$this->user->ServiceProviderID;

        $this->session->mainTable = "service_provider_part_sub_category";
        $this->session->mainPage = "partSubCategories";
        $this->session->secondaryTable = "service_provider_part_sub_category";
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); 
      
        $datatable = $this->loadModel('DataTable');
        if ($this->user->SuperAdmin == 1) {


            $ServiceProviders = $this->loadModel('ServiceProviders');
            $splist = $ServiceProviders->getAllActiveServiceProviders();
            $this->smarty->assign('splist', $splist);
        }
        $this->log($this->page['config']['pageID']);

        $keys_data = $datatable->getAllFieldNames($this->session->mainTable, $this->user->UserID, $this->page['config']['pageID']);

        $this->smarty->assign('data_keys', $keys_data[1]);

        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->display('systemAdmin/partSubCategory.tpl');
    }

    public function loadPartSubCategoryTableAction($args) {
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $dd = $_GET;

        $datatable = $this->loadModel('DataTable');

        if (isset($args['inactive'])) {
            $inactive = true;
        } else {
            $inactive = false;
        }


        if ($this->user->ServiceProviderID != "") {
            $sp = $this->user->ServiceProviderID;
        } else {
            $sp = false;
            $sp = 0;
        }
        if (isset($args['supplierid'])) {
            $sp = $args['supplierid'];
        }
        $keys_data = $datatable->getAllFieldNames($this->session->mainTable, $this->user->UserID, $this->page['config']['pageID']);

        $columns = $keys_data[0];
        $extraColumns = ['<input type="checkbox">'];
        //$columns="";
        // print_r($columns);
        $table=$this->session->mainTable;
        $joins = " 
            left join service_provider sp on sp.ServiceProviderID=$table.ServiceProviderID
            left join service_provider_part_category spc on spc.ServiceProviderPartCategoryID=$table.ServiceProviderPartCategoryID 
        ";
        $data = $datatable->datatableSS($table, $columns, $dd, $joins, $extraColumns, 0, false, $inactive, $sp);

        echo json_encode($data);
    }

    public function processPartSubCategoryAction($args) {
        $this->checkUserSPID($args);
	$spid=$this->user->ServiceProviderID;
        $skyline_model = $this->loadModel('Skyline');
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('statuses', $this->statuses);
          $model = $this->loadModel('PartSubCategories');
        $serviceProviders = $skyline_model->getServiceProviders();

        $this->smarty->assign('ServiceProviders', $serviceProviders);

        if (isset($args['id'])) {
            $datarow = $model->getpartSubCategoryData($args['id']);
        } else {
            $datarow = array();
        }
        $categories_model=$this->loadModel('PartCategories');
          $categories = $categories_model-> getAllSPPartCategories($spid);
        $this->smarty->assign('categories', $categories);
       

        $List = array();

        if ($this->user->SuperAdmin != 1) {
            if (isset($datarow['ServiceProviderPartSubCategoryID'])) {
                $List = $model->getAllSP($this->user->ServiceProviderID, $datarow['ServiceProviderPartSubCategoryID']);
            }
        }
        $this->smarty->assign('List', $List);


        $this->smarty->assign('datarow', $datarow);
        $this->smarty->display('systemAdmin/partSubCategoryForm.tpl');
    }

    public function savePartSubCategoryAction($args) {

        $model = $this->loadModel('PartSubCategories');
//        echo"<pre>";
//        print_r($_POST);
//        echo"</pre>";
        if ($_POST['ServiceProviderPartSubCategoryID'] != '') {
            $model->updatePartSubCategory($_POST, $this->user->ServiceProviderID);
        } else {
            $model->insertPartSubCategory($_POST, $this->user->ServiceProviderID);
        }
        $this->redirect('LookupTables/partSubCategories/');
    }

    public function deletePartSubCategoryAction($args) {
        $model = $this->loadModel('PartSubCategories');

        $model->deletePartSubCategory($args['id']);



        $this->redirect('LookupTables/partSubCategories/');
    }
    
   ///////////TODO 13
    public function partStatusColoursAction($args) {
 $this->checkUserSPID($args);
$spid=$this->user->ServiceProviderID;

        $this->session->mainTable = "sp_part_status_colour";
        $this->session->mainPage = "partStatusColours";
        $this->session->secondaryTable = "sp_part_status_colour";
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); 
      
        $datatable = $this->loadModel('DataTable');
        if ($this->user->SuperAdmin == 1) {


            $ServiceProviders = $this->loadModel('ServiceProviders');
            $splist = $ServiceProviders->getAllActiveServiceProviders();
            $this->smarty->assign('splist', $splist);
        }
        $this->log($this->page['config']['pageID']);

        
         $columns = [
            "Status ID",
            "Status Name",
            "Status Description",
            "Colour",
            "Status"
        ];
        $this->smarty->assign('data_keys', $columns);

        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user calss
        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->display('systemAdmin/PartStatusColours.tpl');
    }

    public function loadPartStatusColourTableAction($args) {
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $dd = $_GET;

        $datatable = $this->loadModel('DataTable');

        if (isset($args['inactive'])) {
            $inactive = true;
        } else {
            $inactive = false;
        }


        if ($this->user->ServiceProviderID != "") {
            $sp = $this->user->ServiceProviderID;
        } else {
            $sp = false;
            $sp = 0;
        }
        if (isset($args['supplierid'])) {
            $sp = $args['supplierid'];
        }
         $table="part_status";

        $columns = [
            "$table.PartStatusID",
            "PartStatusName",
            "PartStatusDescription",
            "Colour",
            "$table.Status"
        ];
        $extraColumns =[];
        //$columns="";
        // print_r($columns);
       
        $joins = " 
            left join sp_part_status_colour ps on ps.PartStatusID=$table.PartStatusID and ps.ServiceProviderID=$sp
           
        ";
        $data = $datatable->datatableSS($table, $columns, $dd, $joins, $extraColumns, 0, false, $inactive, false,false,false);

        echo json_encode($data);
    }

    public function processPartStatusColourAction($args) {
        $this->checkUserSPID($args);
        $spid=$this->user->ServiceProviderID;
        $skyline_model = $this->loadModel('Skyline');
        $this->page = $this->messages->getPage($this->session->mainPage, $this->lang);
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('statuses', $this->statuses);
          $model = $this->loadModel('PartStatusColours');
        

       

        if (isset($args['id'])) {
            $datarow = $model->getPartStatusColourData($args['id']);
        } else {
            $datarow = array();
        }
       
    


        $this->smarty->assign('datarow', $datarow);
        $this->smarty->display('systemAdmin/PartStatusColoursForm.tpl');
    }

    public function savePartStatusColourAction($args) {

        $model = $this->loadModel('PartStatusColours');
//        echo"<pre>";
//        print_r($_POST);
//        echo"</pre>";
        if ($_POST['SpPartStatusColourID'] != '') {
            $model->updatePartStatusColour($_POST, $this->user->ServiceProviderID);
        } else {
            $model->insertPartStatusColour($_POST, $this->user->ServiceProviderID);
        }
        $this->redirect('LookupTables/PartStatusColours/');
    }

    public function deletePartStatusColourAction($args) {
        $model = $this->loadModel('PartStatusColours');

        $model->deletePartStatusColour($args['id']);



        $this->redirect('LookupTables/PartStatusColours/');
    }
     
    
        /**
     * Description
     * 
     * This method is used for to manuplate data of Group Headings Page.
     * Fetching the page title and description of selected page.
     * @param array $args It contains action type as first element i.e update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * @author Thirumalesh Bandi <t.bandi@pccsuk.com> 
     */
    public function groupHeadingsAction($args) {

        if (!$this->user->SuperAdmin) {
            $this->redirect('SystemAdmin/index/lookupTables', null, null);
        }

        $functionAction = isset($args[0]) ? $args[0] : '';
        $selectedRowId = isset($args[1]) ? $args[1] : '';



        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin); //this value should be from user class

        $accessErrorFlag = false;
        
        $this->page = $this->messages->getPage('groupHeadings', $this->lang);
        
        //Getting Page title & Description from database
        $model = $this->loadModel('GroupHeadings');        
        $groupHedingResults = $model->getHeadingPageDetails('groupHeadings');
        
        // Over write the page title & description
        $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
        $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];
        
        $this->smarty->assign('page', $this->page);



        switch ($functionAction) {


            case 'update':


                $args['GroupID'] = $selectedRowId;
                $model = $this->loadModel('GroupHeadings');

                $datarow = $model->fetchRow($args);


                $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);


                $this->smarty->assign('datarow', $datarow);

                $htmlCode = $this->smarty->fetch('groupHeadingsForm.tpl');
                
                echo $htmlCode;

                break;

            default:
             
                $this->smarty->display('groupHeadings.tpl');
            
        }
    }
    

}

?>
