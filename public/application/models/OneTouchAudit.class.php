<?php
/**
 * OneTouchAudit.class.php
 * 
 * Routines for interaction with the one_touch_audit table
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2013 PC Control Systems
 * @link       
 * @version    1.0
 * 
 * Changes
 * Date        Version Author                Reason
 * 02/05/2013  1.00    Andrew J. Williams    Initial Version (Trackerbase VMS Log 254 - One Touch Booking audit log)
 ******************************************************************************/

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

class OneTouchAudit extends CustomModel { 
    private $table;                                                             /* For Table Factory Class */
    private $conn;                                                              /* Database Connection */
    
    public function __construct($Controller) {
                  
        parent::__construct($Controller);
        
        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] ); 
        
        $this->table = TableFactory::OneTouchAudit();
    }
    
    /**
     * create 
     *  
     * Create an one touch audit record
     * 
     * @param array $args   Associative array of field values for the creation of
     *                      the audit record
     * 
     * @return array    (status - Status Code, message - Status message, id - Id of inserted item
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function create($args) {
        $cmd = $this->table->insertCommand( $args );
        if ($this->Execute($this->conn, $cmd, $args)) {
            $result =  array(                                                   /* Job successfully created */
                             'status' => 'SUCCESS',
                             'id' => $this->conn->lastInsertId()                /* Return the newly created item's ID */
                            );
        } else {
            $result = array(
                            'status' => 'FAIL',
                            'id' => 0,                                          /* Not created no ID to return */
                            'message' => $this->lastPDOError()                  /* Return the error */
                           );
        }
        return $result;
    }
    
}

?>