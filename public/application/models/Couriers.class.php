<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Couriers Page in Lookup Tables section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.05
 *  
 * Changes
 * Date        Version  Author                 Reason
 * 25/11/2012  1.00     Nageswara Rao Kanteti  Initial Version
 * 19/02/2013  1.01     Vykintas Rutkunas      System Admin Courier facility upgraded
 * 27/03/2013  1.02     Brian Etherington      Complete Courier Table admin datatable + CRUD changes	
 * 03/04/2013  1.03     Andrew J. Williams     Issue 290 - Courier Defaults move to Courier Table
 * 26/04/2013  1.04     Brian Etherington      Fix bug in Courier search terms OrganisationType and OrgansiationName 
 * 01/05/2013  1.05     Brian Etherington      Added Packaging Type Table
 ******************************************************************************/

class Couriers extends CustomModel {
    
    private $conn;
    //private $dbColumns = ['CourierID', "CourierName", 'AccountNo', 'Online', 'Status'];
    private $dbColumns = ['c.CourierID', 
                          array('if(c.ServiceProviderID is null,if(c.ManufacturerID is null, "Retail Brand", "Manufacturer"),"Service Provider")',
                                'if(c.ServiceProviderID is null,if(c.ManufacturerID is null, "Retail Brand", "Manufacturer"),"Service Provider") as OrganisationType'), 
                          array('if(c.ServiceProviderID is null, if(c.ManufacturerID is null, b.BrandName, m.ManufacturerName ), sp.CompanyName)',
                                'if(c.ServiceProviderID is null, if(c.ManufacturerID is null, b.BrandName, m.ManufacturerName ), sp.CompanyName) as OrganisationName'),  
                          'c.CourierName', 
                          'c.ReportFailureDays', 
                          'c.Online', 
                          'c.Status'];
    private $table     = "courier";
    private $tbl;                                                               /* Used by TableFactory */
    private $PackagingTypeTable;
    
    #public $debug = true;
    
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       
        
        $this->tbl = TableFactory::Courier();
        $this->PackagingTypeTable = TableFactory::PackagingType();
    }
    
    public function Select( $sql, $params=null ) {
        return $this->Query( $this->conn, $sql, $params );
    }
    
   
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->tables
     * @global $this->dbColumns
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    public function fetch($args) {
               
          if($args['firstArg']!='')
          {
              //$args['where'] =  "ServiceProviderID=".$this->conn->quote($args['firstArg']);
              switch($args['firstArg']) {
                  case 'ServiceProvider': // ServiceProvider
                      $args['where'] =  "c.ServiceProviderID is not null";
                      break;
                  case 'Brand':
                      $args['where'] =  "c.BrandID is not null and c.ServiceProviderID is null and c.ManufacturerID is null";
                      break;
                  case 'Manufacturer':
                      $args['where'] =  "c.ManufacturerID is not null and c.ServiceProviderId is null and c.BrandID is null";
                      break;
              }
              
          }
          
          $table = 'courier as c
                    left join service_provider as sp on sp.ServiceProviderID=c.ServiceProviderID
                    left join manufacturer as m on m.ManufacturerID=c.ManufacturerID
                    left join brand as b on b.BrandID=c.BrandID';
      
           $output = $this->ServeDataTables($this->conn, $table, $this->dbColumns, $args);
        
        
            return  $output;
        
     }
    
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
    
     public function processData($args) {
         
         if(!isset($args['CourierID']) || !$args['CourierID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
     
     
    
    
    
    /**
     * Description
     * 
     * This method is used for to validate status name.
     *
     * @param interger $AccountNo  
     * @param interger $BrandID
     * @param interger $ManufacturerID
     * @param interger $ServiceProviderID 
     * @param interger $CourierID  
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValidAction($AccountNo, $BrandID, $ManufacturerID, $ServiceProviderID, $CourierID) {
        
         
         if(!is_null($BrandID))
         {
            /* Execute a prepared statement by passing an array of values */
            $sql = 'SELECT CourierID FROM '.$this->table.' WHERE AccountNo=:AccountNo AND BrandID=:BrandID AND CourierID!=:CourierID';
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':AccountNo' => $AccountNo, ':BrandID' => $BrandID, ':CourierID' => $CourierID));
        
         }  
         else if(!is_null($ManufacturerID))
         {
            /* Execute a prepared statement by passing an array of values */
            $sql = 'SELECT CourierID FROM '.$this->table.' WHERE AccountNo=:AccountNo AND  ManufacturerID=:ManufacturerID  AND CourierID!=:CourierID';
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':AccountNo' => $AccountNo,  ':ManufacturerID' => $ManufacturerID,  ':CourierID' => $CourierID));
        
         } 
         else
         {
            /* Execute a prepared statement by passing an array of values */
            $sql = 'SELECT CourierID FROM '.$this->table.' WHERE AccountNo=:AccountNo AND ServiceProviderID=:ServiceProviderID AND CourierID!=:CourierID';
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':AccountNo' => $AccountNo, ':ServiceProviderID' => $ServiceProviderID,  ':CourierID' => $CourierID));
        
         }
         
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['CourierID'])
        {
                return false;
        }
        
        return true;
    
    }
    
    
    
    /**
    * Description
    * 
    * This method is used for to insert data into database.
    *
    * @param array $args  
    * @global $this->table 
    * @return array It contains status of operation and message.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
    
    public function create($args) {
        
	if($args['OrganisationType'] == "Brand") {
	    $args['BrandID'] = $args['CompanyID'];
	    $args['ManufacturerID'] = NULL;
	    $args['ServiceProviderID'] = NULL;
        } else if($args['OrganisationType'] == "Manufacturer") {
	    $args['BrandID'] = NULL;
	    $args['ManufacturerID'] = $args['CompanyID'];
	    $args['ServiceProviderID'] = NULL;
        } else {
	    $args['BrandID'] = NULL;
	    $args['ManufacturerID'] = NULL;
	    $args['ServiceProviderID'] = $args['CompanyID'];
        }  
        
        //if($this->isValidAction($args['AccountNo'], $args['BrandID'], $args['ManufacturerID'], $args['ServiceProviderID'], 0)) {
            

	    $sql = 'INSERT INTO	' . $this->table . ' 
				(
				    CourierName,
				    BrandID,
				    ManufacturerID, 
				    ServiceProviderID, 
				    Online, 
				    AccountNo, 
				    IPAddress, 
				    UserName, 
				    Password, 
				    ReportFailureDays, 
				    Status, 
				    CreatedDate, 
				    ModifiedUserID, 
				    ModifiedDate,
                                    CommunicateIndividualConsignments
				)
		    VALUES
				(
				    :CourierName,
				    :BrandID, 
				    :ManufacturerID, 
				    :ServiceProviderID, 
				    :Online, 
				    :AccountNo, 
				    :IPAddress, 
				    :UserName, 
				    :Password, 
				    :ReportFailureDays, 
				    :Status, 
				    :CreatedDate, 
				    :ModifiedUserID, 
				    :ModifiedDate,
                                    :CommunicateIndividualConsignments
				)
		';
        
            $insertQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            
            $insertQuery->execute([
		":CourierName" => $args["CourierName"],
                ':AccountNo' => $args['AccountNo'], 
                ':BrandID' => ($args['BrandID'] != '') ? $args['BrandID'] : NULL, 
                ':ManufacturerID' => ($args['ManufacturerID'] != '') ? $args['ManufacturerID'] : NULL,
                ':ServiceProviderID' => ($args['ServiceProviderID'] != '') ? $args['ServiceProviderID'] : NULL,
                ':Online' => $args['Online'],
                ':AccountNo' => $args['AccountNo'],
                ':IPAddress' => $args['IPAddress'],
                ':UserName' => $args['UserName'],
                ':Password' => $args['Password'],
                ':ReportFailureDays' => empty($args['ReportFailureDays']) ? 0 : intval($args['ReportFailureDays']),
                ':Status' => $args['Status'],
                ':CreatedDate' => date("Y-m-d H:i:s"),
                ':ModifiedUserID' => $this->controller->user->UserID,
                ':ModifiedDate' => date("Y-m-d H:i:s"),
                ':CommunicateIndividualConsignments' => isset($args['CommunicateIndividualConsignments']) ? 'Yes' : 'No'
	    ]);
            
            $CourierID = $this->conn->lastInsertId();
            
            if (isset($args['PackageType'])) {
                 $this->updatePackagingTypes( $CourierID, $args['PackageType'] );
             }
        
	    return [
		'status' => 'OK',
		'message' => $this->controller->page['Text']['data_inserted_msg']
	    ];
	    
        /*} else {
            
            return [
		'status' => 'ERROR',
		'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang)
	    ];
	    
        }*/
	
    }
    
    
    
    /**
    * Description
    * 
    * This method is used for to fetch a row from database.
    *
    * @param array $args
    * @global $this->table  
    * @return array It contains row of the given primary key.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
    
    public function fetchRow($args) {
	
        $sql = 'SELECT	CourierID, 
			CourierName, 
			BrandID, 
			ManufacturerID, 
			ServiceProviderID, 
			Online, 
			AccountNo, 
			IPAddress, 
			UserName, 
			Password, 
			ReportFailureDays, 
			Status, 
			EndDate,
                        CommunicateIndividualConsignments
			
		FROM	' . $this->table . ' 
		    
		WHERE	CourierID = :CourierID
		';
	
        $fetchQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
	
        $fetchQuery->execute([':CourierID' => $args['CourierID']]);
	
        $result = $fetchQuery->fetch();
	
        return $result;
	
    }
    
    
    
    /**
     * Description
     * 
     * This method is used for to fetch all couriers for given organization id.
     *
     * @param int $BrandID default false
     * @param int $ManufacturerID default false
     * @param int $ServiceProviderID default false
     * 
     * @global $this->table  
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function fetchAll($BrandID=false, $ManufacturerID=false, $ServiceProviderID=false) {
        
        
        if($BrandID)
        {    
            $sql        = 'SELECT CourierID, AccountNo FROM '.$this->table.' WHERE BrandID=:BrandID AND Status=:Status ORDER BY AccountNo';
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':BrandID' => $BrandID, ':Status' => 'Active'));
        }
        else if($ManufacturerID)
        {    
            $sql        = 'SELECT CourierID, AccountNo FROM '.$this->table.' WHERE ManufacturerID=:ManufacturerID AND Status=:Status ORDER BY AccountNo';
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':ManufacturerID' => $ManufacturerID, ':Status' => 'Active'));
        }
        else if($ServiceProviderID)
        {    
            $sql        = 'SELECT CourierID, AccountNo FROM '.$this->table.' WHERE ServiceProviderID=:ServiceProviderID AND Status=:Status ORDER BY AccountNo';
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':ServiceProviderID' => $ServiceProviderID, ':Status' => 'Active'));
        }
        else
        {    
            $sql        = 'SELECT CourierID, AccountNo FROM '.$this->table.' WHERE Status=:Status ORDER BY AccountNo';
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':Status' => 'Active'));
        }
        
        $result = $fetchQuery->fetchAll();
        return $result;
    }
    
    /**
     * fetchWhere
     * 
     * Return data based on a passed sql query where.
     * 
     * @param string $where WHERE clause for query
     * 
     * @return array        Matching results
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    public function fetchWhere($where) {
        $sql = "
                SELECT
			*
		FROM
			`courier`
		WHERE
                        $where
               ";
    
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result);                                                    /* Curier Record exists so return Service Provider Details */
        } else {
            return(null);                                                       /* Not found return null */
        }
        
    }
    
      /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
     * @global $this->table   
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
        
        if($args['OrganisationType']=="Brand")
        {
            
           $args['BrandID']           = $args['CompanyID'];
           $args['ManufacturerID']    = NULL;
           $args['ServiceProviderID'] = NULL;
            
        }  
        else if($args['OrganisationType']=="Manufacturer")
        {
            
           $args['BrandID']           = NULL;
           $args['ManufacturerID']    = $args['CompanyID'];
           $args['ServiceProviderID'] = NULL;
            
        }  
        else 
        {
            
           $args['BrandID']           = NULL;
           $args['ManufacturerID']    = NULL;
           $args['ServiceProviderID'] = $args['CompanyID'];
            
        }  
            
            
        
        
        
        
        if($this->isValidAction($args['AccountNo'], $args['BrandID'], $args['ManufacturerID'], $args['ServiceProviderID'], $args['CourierID']))
        {        
            
            $EndDate = "0000-00-00 00:00:00";
            $row_data = $this->fetchRow($args);
            if($this->controller->statuses[1]['Code']==$args['Status'])
            {
                if($row_data['Status']!=$args['Status'])
                {
                        $EndDate = date("Y-m-d H:i:s");
                }
                else
                {
                        $EndDate = $row_data['EndDate'];
                }
            }
            
               /* Execute a prepared statement by passing an array of values */
              $sql = '	UPDATE	' . $this->table . ' 
			
			SET	CourierName = :CourierName,
				BrandID = :BrandID, 
				ManufacturerID = :ManufacturerID, 
				ServiceProviderID = :ServiceProviderID, 
				Online = :Online, 
				AccountNo = :AccountNo, 
				IPAddress = :IPAddress, 
				UserName = :UserName, 
				Password = :Password, 
				ReportFailureDays = :ReportFailureDays, 
				Status = :Status, 
				EndDate = :EndDate, 
				ModifiedUserID = :ModifiedUserID, 
				ModifiedDate = :ModifiedDate,
                                CommunicateIndividualConsignments = :CommunicateIndividualConsignments

			WHERE	CourierID = :CourierID
		     ';
        
	    $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	    $updateQuery->execute([
		"CourierName" => $args["CourierName"],
		':AccountNo' => $args['AccountNo'], 
		':BrandID' => ($args['BrandID']!='')?$args['BrandID']:NULL, 
		':ManufacturerID' => ($args['ManufacturerID']!='')?$args['ManufacturerID']:NULL,
		':ServiceProviderID' => ($args['ServiceProviderID']!='')?$args['ServiceProviderID']:NULL,
		':Online' => $args['Online'],
		':AccountNo' => $args['AccountNo'],
		':IPAddress' => $args['IPAddress'],
		':UserName' => $args['UserName'],
		':Password' => $args['Password'],
		':ReportFailureDays' => empty($args['ReportFailureDays']) ? 0 : intval($args['ReportFailureDays']),  
		':Status' => $args['Status'],
		':EndDate' => $EndDate,
		':ModifiedUserID' => $this->controller->user->UserID,
		':ModifiedDate' => date("Y-m-d H:i:s"),
                ':CommunicateIndividualConsignments' => isset($args['CommunicateIndividualConsignments']) ? 'Yes' : 'No',
		':CourierID' => $args['CourierID']
             ]);
            
             if (isset($args['PackageType'])) {
                 $this->updatePackagingTypes( $args['CourierID'], $args['PackageType'] );
             } else {
                 $this->updatePackagingTypes( $args['CourierID'], array() );
             }
        
                
               return array('status' => 'OK',
                        'message' => $this->controller->page['Text']['data_updated_msg']);
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    
    /**
     * updateByID
     *  
     * Update fields passed in argument based upon the CourierID provided
     * 
     * @param array $args   Associative array of field values for the update.
     *                      The fields of the array must match the fields in the
     *                      database. Additionally the primary key CourierID
     *                      must also be provided.
     * 
     * @return array    (status - Status Code, message - Status message, rows_affected - the number of roes affected
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function updateByID($args) {
        $cmd = $this->tbl->updateCommand( $args );
        
        $rows_affected = $this->Execute($this->conn, $cmd, $args);
        
        if ( $rows_affected == 0 ) {                                            /* No rows affected may be error */
            $code = $this->conn->errorCode();
            $message = $this->conn->errorInfo();
        } else {
            $code = 'OK';
            $message = 'Updated';
        }
        
        return (
                array(
                      'status' => $code,
                      'message' => $message,
                      'rows_affected' => $rows_affected
                     )
               );
    }
    
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['Text']['data_deleted_msg']);
    }
    
    
    
    public function getBrandCouriers($brandID) {
	
	$q = "SELECT * FROM courier WHERE BrandID = :brandID";
	$values = ["brandID" => $brandID];
	$result = $this->query($this->conn, $q, $values);
	return $result;
	
    }
    
    public function getServiceProviderCouriers($ServiceProviderID) {
	
	$q = "SELECT * FROM courier WHERE ServiceProviderID = :ServiceProviderID";
	$values = ["ServiceProviderID" => $ServiceProviderID];
	$result = $this->query($this->conn, $q, $values);
	return $result;
	
    }
    
    
    
    public function updateJobShippingData($data, $jobID) {
	
	$q = "SELECT * FROM shipping WHERE JobID = :jobID";
	$values = ["jobID" => $jobID];
	$result = $this->query($this->conn, $q, $values);
	
	$values = [
	    "courierID" => ((isset($data["CourierID"]) && $data["CourierID"] != "") ? $data["CourierID"] : null),
	    "consignmentNo" => ((isset($data["ConsignmentNo"])) ? strtoupper($data["ConsignmentNo"]) : null),
	    "consignmentDate" => ((isset($data["ConsignmentDate"]) && $data["ConsignmentDate"] != "") ? $data["ConsignmentDate"] : null),
	    "userID" => $this->controller->user->UserID,
	    "jobID" => $jobID
	];
	
	if(count($result) > 0) {
	    
	    $q = "  UPDATE  shipping
		    SET	    CourierID = :courierID,
			    ConsignmentNo = :consignmentNo,
			    ConsignmentDate = :consignmentDate,
			    ModifiedUserID = :userID
		    WHERE   JobID = :jobID
		 ";
	    
	} else {
	    
	    $q = "  INSERT INTO shipping
				(
				    JobID,
				    CourierID,
				    ConsignmentNo,
				    ConsignmentDate,
				    ModifiedUserID
				)
		    VALUES	
				(
				    :jobID,
				    :courierID,
				    :consignmentNo,
				    :consignmentDate,
				    :userID
				)
		 ";
	    
	}
	
	$result = $this->execute($this->conn, $q, $values);
	
    }
    
    public function getPackagingTypes( $CourierID ) {
        
        $sql = "select PackagingType from packaging_type where CourierID=:CourierID";
        $params = array( 'CourierID' => $CourierID );
        $result =  $this->Query( $this->conn, $sql, $params );
        
        $PackagingTypes = array();
        foreach($result as $row) {
            $PackagingTypes[] = $row['PackagingType'];
        }
        return $PackagingTypes;
    }
    
    public function updatePackagingTypes( $CourierID, $PackagingTypes ) {
        
        // First delete all existing Paramater Types..
        $where = "CourierID = $CourierID";
        $this->DeleteRow( $this->conn, $this->PackagingTypeTable, $where );
        
        // And Replace with the new ones...
        foreach($PackagingTypes as $PackagingType) {
            $params = array( 'CourierID' => $CourierID, 'PackagingType' => $PackagingType );
            $this->InsertRow( $this->conn, $this->PackagingTypeTable, $params );
        }
    }
    
    
    
}
?>