<?php

require_once('CustomModel.class.php');

class Reports extends CustomModel {

    
    private $conn;
    private $schemaConn;
    private $mysqli;
    
    
    public function __construct($controller) {
    
        parent::__construct($controller); 
        
	$this->conn = $this->Connect( 
	    $this->controller->config['DataBase']['Conn'],
	    $this->controller->config['DataBase']['Username'],
	    $this->controller->config['DataBase']['Password'] 
	);  
        
	$this->schemaConn = $this->Connect(
	    "mysql:host=localhost;dbname=information_schema",
	    $this->controller->config['DataBase']['Username'],
	    $this->controller->config['DataBase']['Password'] 
	);
	
	//Initialize MySQLi connection
        $dbArr = explode("=",$this->controller->config['DataBase']['Conn']);
        $dbName = $dbArr[2];
        $dbHostArr = explode(";",$dbArr[1]);
        $dbHost = $dbHostArr[0];
        $this->mysqli = new mysqli( 
	    $dbHost,
	    $this->controller->config['DataBase']['Username'],
	    $this->controller->config['DataBase']['Password'],
	    $dbName
	);
        
    }

    
    
    public function getJobsBooked($args=null) {
	
	if($args["dateFrom"]) {
	    $args["dateFrom"] = str_replace("/", ".", $args["dateFrom"]);
	    $dateFrom = date("Y-m-d", strtotime($args["dateFrom"]));
	} else {
	    $dateFrom = "0000-00-00";
	}
	if($args["dateTo"]) {
	    $args["dateTo"] = str_replace("/", ".", $args["dateTo"]);
	    $dateTo = date("Y-m-d", strtotime($args["dateTo"]));
	} else {
	    $dateTo = "9999-99-99";
	}
	
	$q = '	SELECT	    job.JobID		    AS "Job ID",
			    job.RMANumber	    AS "RMA Number",
			    job.NetworkRefNo	    AS "Network Ref No",
			    job.AgentRefNo	    AS "Agent Ref No",
			    product.ProductNo	    AS "Catalogue Number",
			    manufacturer.ManufacturerName AS "Manufacturer Name",
			    model.ModelNumber	    AS "Model Number",
			    unit.UnitTypeName	    AS "Unit Type",
			    service.CompanyName	    AS "Service Center",
			    stype.ServiceTypeName   AS "Service Type",
			    CONCAT( customer.ContactFirstName, " ", customer.ContactLastName) AS "Customer Name",
			    customer.PostalCode	    AS "Customer Postcode",
			    
			    /*
			    DATE_FORMAT(job.DateBooked, "%d/%m/%Y")	    AS "Date Booked",
			    DATE_FORMAT(job.DateUnitReceived, "%d/%m/%Y")   AS "Date Unit Received",
			    DATE_FORMAT(job.RepairCompleteDate, "%d/%m/%Y") AS "Repair Compelted Date",
			    DATE_FORMAT(job.ClosedDate, "%d/%m/%Y")	    AS "Closed Date",
			    */
			    
			    job.DateBooked	    AS "Date Booked",
			    job.DateUnitReceived    AS "Date Unit Received",
			    job.RepairCompleteDate  AS "Repair Compelted Date",
			    job.ClosedDate	    AS "Closed Date",
			    
			    status.StatusName	    AS "Status",
			    job.AgentStatus	    AS "Agent Status",
			    job.RepairDescription   AS "Repair Description",
			    
			    (
				SELECT  CONCAT(	"DATE: ", DATE_FORMAT(contact.ContactDate, "%d/%m/%Y"), 
						" SUBJECT: ", contact.Subject, 
						" NOTE: ", contact.Note)
				FROM    contact_history AS contact
				WHERE	contact.ContactDate = (	
								SELECT	MAX(contact.ContactDate) 
								FROM	contact_history AS contact
								WHERE	contact.JobID = job.JobID 
								LIMIT	1
							      )
					AND contact.JobID = job.JobID
				LIMIT	1
			    )			    AS "Contact History",
			    
			    job.Notes		    AS "Notes"
		
		FROM	    job
		
		LEFT JOIN   product		    ON job.ProductID = product.ProductID
		LEFT JOIN   manufacturer	    ON job.ManufacturerID = manufacturer.ManufacturerID
		LEFT JOIN   model		    ON product.ModelID = model.ModelID
		LEFT JOIN   unit_type AS unit	    ON product.UnitTypeID = unit.UnitTypeID
		LEFT JOIN   service_provider AS service ON job.ServiceProviderID = service.ServiceProviderID
		LEFT JOIN   customer		    ON job.CustomerID = customer.CustomerID
		LEFT JOIN   service_type AS stype   ON job.ServiceTypeID = stype.ServiceTypeID
		LEFT JOIN   status		    ON job.StatusID = status.StatusID 
		
		WHERE	    job.DateBooked >= :dateFrom AND job.DateBooked <= :dateTo
	     ';
	
	$values = ["dateFrom" => $dateFrom, "dateTo" => $dateTo];
	
	$result = $this->query($this->conn, $q, $values);
	
	return $result;
	
    }
    
    
    
    public function sendASCServiceProviderReport() {
	
	$sql = file_get_contents("application/sql/asc_sp_report.sql");

	$q = "SELECT * FROM network";
	$networks = $this->query($this->conn, $q);
	
	foreach($networks as $network) {
	    
	    $network["CompanyName"] = strtoupper($network["CompanyName"]);
	    
	    $networkID = $network["NetworkID"];
	    $q = "  SELECT	sp.* 
		    FROM	network_service_provider AS nsp
		    LEFT JOIN	service_provider AS sp ON nsp.ServiceProviderID = sp.ServiceProviderID
		    LEFT JOIN	network ON network.NetworkID = nsp.NetworkID
		    WHERE	nsp.NetworkID = :networkID AND
				sp.sendASCReport = 'Yes' AND
				sp.Status = 'Active' AND
				network.sendASCReport = 'Yes' AND
				network.Status = 'Active'
		 ";
	    $values = ["networkID" => $networkID];
	    $providers = $this->query($this->conn, $q, $values);

	    foreach($providers as $provider) {

		$provider["CompanyName"] = strtoupper($provider["CompanyName"]);
		
		$spID = $provider["ServiceProviderID"];
		$q = str_replace(["[networkID]", "[serviceProviderID]"], [$networkID, $spID], $sql);

		//Run MySQLi multi query
		$this->mysqli->multi_query($q);
		$result = [];
		//Process multiple results
		do {
		    if($res = $this->mysqli->store_result()) {
			$result[] = $res->fetch_all(MYSQLI_ASSOC);
			$res->free();
		    }
		} while ($this->mysqli->more_results() && $this->mysqli->next_result());        
		
		if($this->mysqli->errno) {
		    $this->controller->log("MySQLi batch execution prematurely ended.");
		    $this->controller->log($this->mysqli->error);
		} 		
		
		if(count($result[0]) == 0) {
		    continue;
		}
		
		$byClient = "";
		$clients = $result[0];
		$clientLessThan7Total = 0;
		$clientFrom7to14Total = 0;
		$clientMoreThan14Total = 0;
		$clientMoreThan30Total = 0;
		$clientTotal = 0;
		foreach($clients as $client) {
		    $client["name"] = strtoupper($client["name"]);
		    $byClient .= "  <tr>	
					<td style='text-align:left;'>{$client["name"]}</td>
					<td>{$client["lessThan7"]}</td>
					<td>{$client["from7to14"]}</td>
					<td style='color:red'>{$client["moreThan14"]}</td>
					<td>{$client["total"]}</td>
					<td style='width:50px; border:none; background-color:white;'>&nbsp;</td>
					<td style='color:red;'>{$client["moreThan30"]}</td>
				    </tr>";
		    $clientLessThan7Total += $client["lessThan7"];
		    $clientFrom7to14Total += $client["from7to14"];
		    $clientMoreThan14Total += $client["moreThan14"];
		    $clientMoreThan30Total += $client["moreThan30"];
		    $clientTotal += $client["total"];
		}
		$byClient .= "  <tr style='background:#D9D9D9'>	
				    <td style='text-align:left;'>TOTAL</td>
				    <td>$clientLessThan7Total</td>
				    <td>$clientFrom7to14Total</td>
				    <td style='color:red'>$clientMoreThan14Total</td>
				    <td>$clientTotal</td>
				    <td style='width:50px; border:none; background-color:white;'>&nbsp;</td>
				    <td style='color:red;'>$clientMoreThan30Total</td>
				</tr>";

		$byStatus = "";
		$statuses = $result[1];
		$statusLessThan7Total = 0;
		$statusFrom7to14Total = 0;
		$statusMoreThan14Total = 0;
		$statusMoreThan30Total = 0;
		$statusTotal = 0;
		foreach($statuses as $status) {
		    $byStatus .= "  <tr>	
					<td style='text-align:left;'>{$status["stat"]}</td>
					<td>{$status["lessThan7"]}</td>
					<td>{$status["from7to14"]}</td>
					<td style='color:red'>{$status["moreThan14"]}</td>
					<td>{$status["total"]}</td>
					<td style='width:50px; border:none; background-color:white;'>&nbsp;</td>
					<td style='color:red'>{$status["moreThan30"]}</td>
				    </tr>";
		    $statusLessThan7Total += $status["lessThan7"];
		    $statusFrom7to14Total += $status["from7to14"];
		    $statusMoreThan14Total += $status["moreThan14"];
		    $statusMoreThan30Total += $status["moreThan30"];
		    $statusTotal += $status["total"];
		}
		$byStatus .= "  <tr style='background:#D9D9D9'>	
				    <td style='text-align:left;'>TOTAL</td>
				    <td>$statusLessThan7Total</td>
				    <td>$statusFrom7to14Total</td>
				    <td style='color:red'>$statusMoreThan14Total</td>
				    <td>$statusTotal</td>
				    <td style='width:50px; border:none; background-color:white;'>&nbsp;</td>
				    <td style='color:red'>$statusMoreThan30Total</td>
				</tr>";

		//{$provider["ServiceManagerForename"]} & {$provider["AdminSupervisorForename"]}
		if($provider["ServiceManagerForename"] && $provider["AdminSupervisorForename"]) {
		    $names = $provider["ServiceManagerForename"] . " & " . $provider["AdminSupervisorForename"];
		} else if($provider["ServiceManagerForename"] && !$provider["AdminSupervisorForename"]) {
		    $names = $provider["ServiceManagerForename"];
		} else if(!$provider["ServiceManagerForename"] && $provider["AdminSupervisorForename"]) {
		    $names = $provider["AdminSupervisorForename"];
		}
		
		$html = "   <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
			    <html xmlns='http://www.w3.org/1999/xhtml'>
			    <head>
				<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
				<title>Critical Information – Open Jobs Analysis</title>
				<style>
				    table {
					border-collapse: collapse;
				    }
				    td {
					border: 1px solid black;
					padding: 3px 10px;
					text-align:center;
				    }
				    body {
					font-family: Arial, Helvetica, sans-serif;
				    }
				</style>
			    </head>
			    <body>
				<p>Hi $names</p>

				<p>
				    Please find below today's analysis of {$network["CompanyName"]} jobs currently in progress at 
				    {$provider["Acronym"]}.
				    <br/>It is extremely important that you manage these jobs to ensure that your TAT is within scope. 
				    <br/>You should be aware that your performance is being monitored by our Clients and 
				    {$provider["Acronym"]} will be challenged if your performance falls below agreed standards. 
				</p>

				<p>
				    Please ensure that all jobs with a TAT between 7 & 14 days are reviewed and addressed as soon as 
				    possible.
				</p>

				<p style='color:red;'>
				    Any job with a TAT of more than 14 Days should be urgently reviewed. If a job has been completed 
				    and returned to the customer it must be closed down today so that it does not appear on tomorrow's 
				    report. 
				</p>

				<p>
				    Any job with a TAT greater than 30 days should be assessed to see if it has actually been completed 
				    but is still 'live' due to an administration error such as failure to fully despatch.
				</p>

				<p>
				    Kind Regards
				    <br/>{$network["NetworkManagerForename"]} {$network["NetworkManagerSurname"]}
				    <br/>Network Manager
				    <br/>{$network["CompanyName"]}
				</p>
				
				<table>
				    <tbody>
					<tr>
					    <td colspan='5' style='background:#808080; text-align:left;'>
						{$provider["CompanyName"]} - OPEN JOB ANALYSIS &nbsp;&nbsp;&nbsp; REPORT DATE " . date("d/m/Y H:i") . "
					    </td>
					    <td style='width:50px; border:none; background-color:white;'>&nbsp;</td>
					    <td style='background:#808080;'>CRITICAL ALERT</td>
					</tr>
					<tr style='background:#D9D9D9'>
					    <td style='font-weight:bold; text-align:left;'>BY CLIENTS</td>
					    <td>&lt; 7 Days</td>
					    <td>7 to 14 Days</td>
					    <td>&gt; 14 Days</td>
					    <td>TOTAL</td>
					    <td style='width:50px; border:none; background-color:white;'>&nbsp;</td>
					    <td>&gt; 30 Days</td>
					</tr>
					{$byClient}
					<tr>
					    <td colspan='6' style='border:none;'>&nbsp;</td>
					</tr>
					<tr>
					    <td colspan='6' style='border:none;'>&nbsp;</td>
					</tr>
					<tr style='background:#D9D9D9'>
					    <td style='font-weight:bold; text-align:left;'>BY STATUS</td>
					    <td>&lt; 7 Days</td>
					    <td>7 to 14 Days</td>
					    <td>&gt; 14 Days</td>
					    <td>TOTAL</td>
					    <td style='width:50px; border:none; background-color:white;'>&nbsp;</td>
					    <td>&gt; 30 Days</td>
					</tr>
					{$byStatus}
				    </tbody>
				</table>
			    </body>
			    </html>
		";
		
		$ccEmail = $network["NetworkManagerEmail"];
		$bcc = "j.berry@pccsuk.com";
		
		$subject = "Critical {$provider["Acronym"]} Report - {$network["CompanyName"]} Open Jobs Analysis";
		
		$emailModel = $this->loadModel("Email");
		$emailModel->Send(
		    "Skyline",				//from name
		    "no-reply@skylinecms.co.uk",	//from email
		    $provider["ServiceManagerEmail"],	//to email
		    $subject,				//subject
		    $html,				//body
		    $ccEmail,				//cc emails
		    $network["NetworkManagerEmail"],	//reply email
		    false,				//reply name
		    true,				//bcc flag
		    $bcc				//bcc email
		);
		
	    }
	    
	}
	
    }

    
    
    public function sendASCNetworkReport() {
	
	$sql = file_get_contents("application/sql/asc_network_report.sql");

	$q = "SELECT * FROM network WHERE SendNetworkReport = 'Yes'";
	$networks = $this->query($this->conn, $q);
	
	foreach($networks as $network) {
	    
	    $network["CompanyName"] = strtoupper($network["CompanyName"]);
		    
	    $networkID = $network["NetworkID"];

	    $q = str_replace("[networkID]", $networkID, $sql);

	    //Run MySQLi multi query
	    $this->mysqli->multi_query($q);
	    $result = [];
	    //Process multiple results
	    do {
		if($res = $this->mysqli->store_result()) {
		    $result[] = $res->fetch_all(MYSQLI_ASSOC);
		    $res->free();
		}
	    } while ($this->mysqli->more_results() && $this->mysqli->next_result());        

	    if($this->mysqli->errno) {
		$this->controller->log("MySQLi batch execution prematurely ended.");
		$this->controller->log($this->mysqli->error);
	    } 		

	    if(count($result[0]) == 0) {
		continue;
	    }
		
	    $clients = $result[0];
	    $clientLessThan7Total = 0;
	    $clientFrom7to14Total = 0;
	    $clientMoreThan14Total = 0;
	    $clientMoreThan30Total = 0;
	    $clientTotal = 0;
	    $table = "";
	    $count = 0;
	    
	    $statuses = $result[1];
	    $statusLessThan7Total = 0;
	    $statusFrom7to14Total = 0;
	    $statusMoreThan14Total = 0;
	    $statusMoreThan30Total = 0;
	    $statusTotal = 0;
	    $tableStatuses = "";
	    $countStatuses = 0;
	    
	    foreach($clients as $client) {
		$client["clientName"] = strtoupper($client["clientName"]);
		$client["serviceProviderName"] = strtoupper($client["serviceProviderName"]);
		$table .= " <tr>	
				<td style='text-align:left;'>{$client["serviceProviderName"]}</td>
				<td style='text-align:left;'>{$client["clientName"]}</td>
				<td>{$client["lessThan7"]}</td>
				<td>{$client["from7to14"]}</td>
				<td style='color:red'>{$client["moreThan14"]}</td>
				<td>{$client["total"]}</td>
				<td style='width:50px; border:none; background-color:white;'>&nbsp;</td>
				<td style='color:red'>{$client["moreThan30"]}</td>
			    </tr>";
		$count++;
		$clientLessThan7Total += $client["lessThan7"];
		$clientFrom7to14Total += $client["from7to14"];
		$clientMoreThan14Total += $client["moreThan14"];
		$clientMoreThan30Total += $client["moreThan30"];
		$clientTotal += $client["total"];
	    }
	    $table .= "	<tr style='background:#D9D9D9'>	
			    <td style='text-align:left;'>TOTAL</td>
			    <td>$count</td>
			    <td>$clientLessThan7Total</td>
			    <td>$clientFrom7to14Total</td>
			    <td style='color:red'>$clientMoreThan14Total</td>
			    <td>$clientTotal</td>
			    <td style='width:50px; border:none; background-color:white;'>&nbsp;</td>
			    <td style='color:red'>$clientMoreThan30Total</td>
			</tr>";
	    
	    foreach($statuses as $status) {
		$status["stat"] = strtoupper($status["stat"]);
		$tableStatuses .= " <tr>	
				    <td style='text-align:left;'>{$status["stat"]}</td>
				    <td>{$status["lessThan7"]}</td>
				    <td>{$status["from7to14"]}</td>
				    <td style='color:red'>{$status["moreThan14"]}</td>
				    <td>{$status["total"]}</td>
				    <td style='width:50px; border:none; background-color:white;'>&nbsp;</td>
				    <td style='color:red'>{$status["moreThan30"]}</td>
				</tr>";
		$countStatuses++;
		$statusLessThan7Total += $status["lessThan7"];
		$statusFrom7to14Total += $status["from7to14"];
		$statusMoreThan14Total += $status["moreThan14"];
		$statusMoreThan30Total += $status["moreThan30"];
		$statusTotal += $status["total"];
	    }
	    $tableStatuses .= "	<tr style='background:#D9D9D9'>	
				    <td style='text-align:left;'>TOTAL</td>
				    <td>$statusLessThan7Total</td>
				    <td>$statusFrom7to14Total</td>
				    <td style='color:red'>$statusMoreThan14Total</td>
				    <td>$statusTotal</td>
				    <td style='width:50px; border:none; background-color:white;'>&nbsp;</td>
				    <td style='color:red'>$statusMoreThan30Total</td>
				</tr>";
	    
	    
	    $html = "   <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
			<html xmlns='http://www.w3.org/1999/xhtml'>
			<head>
				<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
				<title>Critical Information – Open Jobs Analysis</title>
				<style>
				    table {
					border-collapse: collapse;
				    }
				    td {
					border: 1px solid black;
					padding: 3px 10px;
					text-align:center;
				    }
				    body {
					font-family: Arial, Helvetica, sans-serif;
				    }
				</style>
			</head>
			<body>
			    <table>
				<tbody>
				    <tr>
					<td colspan='6' style='background:#808080; text-align:left;'>
					    NETWORK: {$network["CompanyName"]} - OPEN JOB ANALYSIS &nbsp;&nbsp;&nbsp; REPORT DATE " . date("d/m/Y H:i") . "
					</td>
					<td style='width:50px; border:none; background-color:white;'>&nbsp;</td>
					<td style='background:#808080;'>CRITICAL ALERT</td>
				    </tr>
				    <tr style='background:#D9D9D9'>
					<td style='text-align:left;'>SERVICE PROVIDER</td>
					<td>CLIENT</td>
					<td>&lt; 7 Days</td>
					<td>7 to 14 Days</td>
					<td>&gt; 14 Days</td>
					<td>TOTAL</td>
					<td style='width:50px; border:none; background-color:white;'>&nbsp;</td>
					<td>&gt; 30 Days</td>
				    </tr>
				    {$table}
				</tbody>
			    </table>
			    <br/><br/>
			    <table>
				<tbody>
				    <tr style='background:#D9D9D9'>
					<td style='font-weight:bold; text-align:left;'>BY STATUS</td>
					<td>&lt; 7 Days</td>
					<td>7 to 14 Days</td>
					<td>&gt; 14 Days</td>
					<td>TOTAL</td>
					<td style='width:50px; border:none; background-color:white;'>&nbsp;</td>
					<td>&gt; 30 Days</td>
				    </tr>
				    {$tableStatuses}
				</tbody>
			    </table>
			</body>
			</html>
	    ";
		
	    $false = false;
	    $bcc = "j.berry@pccsuk.com";

	    $subject = "Critical Information - {$network["CompanyName"]} Open Jobs Analysis";
	    
	    $emailModel = $this->loadModel("Email");
	    $emailModel->Send(
		"Skyline",			    //from name
		"no-reply@skylinecms.co.uk",	    //from email
		$network["ServiceManagerEmail"],    //to email
		$subject,			    //subject
		$html,				    //body
		$false,				    //cc emails
		false,				    //reply email
		false,				    //reply name
		true,				    //bcc flag
		$bcc				    //bcc email
	    );
	}
	
    }



    public function samsungTransactionReport() {
	
	require_once("libraries/PHPExcel/PHPExcel.php");
	require_once("libraries/PHPExcel/PHPExcel/Writer/Excel2007.php");
	
	set_time_limit(10000);
	ini_set("memory_limit", "2048M");
	
	$args = $_REQUEST;
	
	if($args["dateFrom"]) {
	    $args["dateFrom"] = str_replace("/", ".", $args["dateFrom"]);
	    $dateFrom = date("Y-m-d", strtotime($args["dateFrom"]));
	} else {
	    $dateFrom = "0000-00-00";
	}
	if($args["dateTo"]) {
	    $args["dateTo"] = str_replace("/", ".", $args["dateTo"]);
	    $dateTo = date("Y-m-d",strtotime($args["dateTo"]));
	} else {
	    $dateTo = "9999-99-99";
	}
	
	
	$q = file_get_contents("application/sql/transaction_report.sql");
	
	
	$values = ["dateFrom" => $dateFrom, "dateTo" => $dateTo];
	
	if($this->controller->user->UserType == "Network") {
	    $q .= " AND job.NetworkID = :networkID";
	    $values["networkID"] = $this->controller->user->NetworkID;
	}
	if($this->controller->user->UserType == "Client") {
	    $q .= " AND job.ClientID = :clientID";
	    $values["clientID"] = $this->controller->user->ClientID;
	}
	if($this->controller->user->UserType == "Branch") {
	    $q .= " AND job.BranchID = :branchID";
	    $values["branchID"] = $this->controller->user->BranchID;
	}
	if($this->controller->user->UserType == "ServiceProvider") {
	    $q .= " AND job.ServiceProviderID = :serviceProviderID";
	    $values["serviceProviderID"] = $this->controller->user->ServiceProviderID;
	}
	
	if(isset($args["network"]) && $args["network"] != "") {
	    $q .= " AND job.NetworkID = :networkID";
	    $values["networkID"] = $args["network"];
	}
	if(isset($args["client"]) && $args["client"] != "") {
	    $q .= " AND job.ClientID = :clientID";
	    $values["clientID"] = $args["client"]; 
	}
	if(isset($args["branches"]) && $args["branches"] != "" && $args["branches"] != "[]") {
	    $branches = json_decode($args["branches"]);
	    $q .= " AND job.BranchID IN(";
	    for($i = 0; $i < count($branches); $i++) {
		if($i == 0) {
		    $q .= $branches[$i];
		} else {
		    $q .= ", " . $branches[$i];
		}
	    }
	    $q .= ")";
	}
	
	$report = $this->query($this->conn, $q, $values);
	
	//creating report progress record in the database
	$q = "INSERT INTO report_progress VALUES (:hash, :status, :totalRows, :currentRow)";
	$values = [
	    "hash" => $args["hash"],
	    "status" => "running",
	    "totalRows" => count($report),
	    "currentRow" => 0
	];
	$result = $this->execute($this->conn, $q, $values);
	
	
	//generating excel file
	$excel = new PHPExcel();
	
	$excel->setActiveSheetIndex(0);
	
	$helperModel = $this->loadModel("Helpers");

	$excel->getDefaultStyle()->getFont()->setSize(10); 
	
	
	if(count($report) == 0) {
	    
	    $excel->getActiveSheet()->SetCellValue("A1", "No jobs found.");
	    
	} else {
	
	    $top = 10;
	    $left = 1;
	    
	    $header = array_keys($report[0]);

	    $colCount = count($header) - 1;

	    //header styling
	    $headerStyle = [
		'fill' => [
		    'type' => PHPExcel_Style_Fill::FILL_SOLID,
		    'color' => ['rgb'=>'0070C0']
		],
		'font' => [
		    'bold' => true,
		    'color' => ['rgb'=>'FFFFFF']
		],
		'borders' => [
		    'outline' => [
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => ['rgb' => 'D0D0D0']
		    ]
		],
		"alignment" => [
		    "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
		    "vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
		]
	    ];
	    

	    //table headers
	    $colNum = $left;
	    foreach($header as $name) {
		if($name == "TAT") {
		    continue;
		}
		$colName = $helperModel->getExcelColName($colNum);
		$excel->getActiveSheet()->getStyle($colName . ($top - 1))->applyFromArray($headerStyle);
		$colNum++;
	    }
	    $excel->getActiveSheet()->SetCellValue("B9", "Skyline No");
	    $excel->getActiveSheet()->SetCellValue("C9", "Branch");
	    $excel->getActiveSheet()->SetCellValue("D9", "Customer Surname");
	    $excel->getActiveSheet()->SetCellValue("E9", "Customer Title & First Name");
	    $excel->getActiveSheet()->SetCellValue("F9", "Manufacturer");
	    $excel->getActiveSheet()->SetCellValue("G9", "Product Type");
	    $excel->getActiveSheet()->SetCellValue("H9", "Model No.");
	    $excel->getActiveSheet()->SetCellValue("I9", "Serial No.");
	    $excel->getActiveSheet()->SetCellValue("J9", "IMEI No.");
	    $excel->getActiveSheet()->SetCellValue("K9", "Referral No.");
	    $excel->getActiveSheet()->SetCellValue("L9", "Service Type");
	    $excel->getActiveSheet()->SetCellValue("M9", "Booked");
	    $excel->getActiveSheet()->SetCellValue("N9", "Time");
	    $excel->getActiveSheet()->SetCellValue("O9", "Completed");
	    $excel->getActiveSheet()->SetCellValue("P9", "TAT Days");
	    $excel->getActiveSheet()->SetCellValue("Q9", "Current Status");
	    $excel->getActiveSheet()->SetCellValue("R9", "Service Request");
	    $excel->getActiveSheet()->SetCellValue("S9", "Service Report");
	    $excel->getActiveSheet()->SetCellValue("U9", "Software Update");
	    $excel->getActiveSheet()->SetCellValue("V9", "Education");
	    $excel->getActiveSheet()->SetCellValue("W9", "Setup");
	    $excel->getActiveSheet()->SetCellValue("X9", "Damaged");
	    $excel->getActiveSheet()->SetCellValue("Y9", "Faulty Acc");
	    $excel->getActiveSheet()->SetCellValue("Z9", "Non-UK Mod");
	    $excel->getActiveSheet()->SetCellValue("AA9", "Unresolvable");
	    $excel->getActiveSheet()->SetCellValue("AB9", "Branch Repair");
	    $excel->getActiveSheet()->SetCellValue("AC9", "Repair Required");
	    $excel->getActiveSheet()->SetCellValue("AD9", "Repair Center");
	    $excel->getActiveSheet()->SetCellValue("AE9", "Courier");
	    $excel->getActiveSheet()->SetCellValue("AF9", "Consignment No.");
	    $excel->getActiveSheet()->SetCellValue("AG9", "Labour Charge");
	    $excel->getActiveSheet()->SetCellValue("AH9", "Parts Charge");
	    $excel->getActiveSheet()->SetCellValue("AI9", "Delivery Charge");
	    $excel->getActiveSheet()->SetCellValue("AJ9", "VAT");
	    $excel->getActiveSheet()->SetCellValue("AK9", "Total");
	    
	    //grey row
	    $greyRow = [
		'fill' => [
		    'type' => PHPExcel_Style_Fill::FILL_SOLID,
		    'color' => ['rgb'=>'E8E8E8']
		],
		'borders' => [
		    'outline' => [
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => ['rgb' => 'D0D0D0']
		    ]
		]
	    ];

	    //standard row
	    $whiteRow = [
		'borders' => [
		    'outline' => [
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => ['rgb' => 'D0D0D0']
		    ]
		]
	    ];
	    
	    //grey font
	    $greyFont = [
		'font' => [
		    'color' => ['rgb'=>'737373']
		]
	    ];
	    
	    //blue cell
	    $blueCell = [
		'fill' => [
		    'type' => PHPExcel_Style_Fill::FILL_SOLID,
		    'color' => ['rgb'=>'0070C0']
		]
	    ];

	    //alignment
	    $excel->getActiveSheet()->getRowDimension("2")->setRowHeight(20);
	    $excel->getActiveSheet()->getRowDimension("9")->setRowHeight(25);
	    $excel->getActiveSheet()->getColumnDimension("A")->setWidth(3);
	    $excel->getActiveSheet()->getStyle("T:AC")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	    //title
	    $titleStyle = [
		"font" => [
		    "bold" => true,
		    "size" => 16
		],
		"alignment" => [
		    "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
		    "vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
		]
	    ];	  
	    $excel->getActiveSheet()->mergeCells("B2:D2");	    
	    $excel->getActiveSheet()->SetCellValue("B2", "Transaction Report");
	    $excel->getActiveSheet()->getStyle("B2")->applyFromArray($titleStyle);
	    
	    //client name
	    if(isset($this->controller->user->ClientID) && $this->controller->user->ClientID != "") {
		$excel->getActiveSheet()->SetCellValue("B4", "Client:");
		$q = "SELECT ClientID, ClientName FROM client WHERE ClientID = :clientID";
		$values = ["clientID" => $this->controller->user->ClientID];
		$client = $this->query($this->conn, $q, $values);
		$excel->getActiveSheet()->mergeCells("C4:D4");	    
		$excel->getActiveSheet()->SetCellValue("C4", $client[0]["ClientName"]);
	    }
	    
	    //branch name
	    if(isset($this->controller->user->BranchID) && $this->controller->user->BranchID != "") {
		$excel->getActiveSheet()->SetCellValue("B5", "Branch:");
		$q = "SELECT BranchID, BranchName FROM branch WHERE BranchID = :branchID";
		$values = ["branchID" => $this->controller->user->BranchID];
		$branch = $this->query($this->conn, $q, $values);
		$excel->getActiveSheet()->mergeCells("C5:D5");
		$excel->getActiveSheet()->SetCellValue("C5", $branch[0]["BranchName"]);
	    }
	    
	    //date from
	    if($args["dateFrom"]) {
		$excel->getActiveSheet()->SetCellValue("B6", "Date From:");
		$excel->getActiveSheet()->SetCellValue("C6", date("Y-m-d",strtotime($args["dateFrom"])));
	    }
	    
	    //date to
	    if($args["dateTo"]) {
		$excel->getActiveSheet()->SetCellValue("B7", "Date To:");
		$excel->getActiveSheet()->SetCellValue("C7", date("Y-m-d",strtotime($args["dateTo"])));
	    }
	    
	    //date created
	    $excel->getActiveSheet()->SetCellValue("B8", "Date Created:");
	    $excel->getActiveSheet()->SetCellValue("C8", date("Y-m-d"));
	    
	    //service action
	    $excel->getActiveSheet()->mergeCells("U8:AC8");	    
	    $serviceStyle = [
		'fill' => [
		    'type' => PHPExcel_Style_Fill::FILL_SOLID,
		    'color' => ['rgb'=>'E8E8E8']
		],
		'borders' => [
		    'outline' => [
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => ['rgb' => 'D0D0D0']
		    ]
		],
		"font" => [
		    "bold" => true,
		    "size" => 11
		],
		"alignment" => [
		    "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		    "vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
		]
	    ];
	    $excel->getActiveSheet()->getStyle("U8:AC8")->applyFromArray($serviceStyle);
	    $excel->getActiveSheet()->SetCellValue("U8", "Service Action");
	    
	    $softwareTotal = 0;
	    $educationTotal = 0;
	    $setupTotal = 0;
	    $repairTotal = 0;
			
	    //table data
	    foreach($report as $rowNum => $job) {
		
		$colNum = $left;
		foreach($job as $fieldName => $field) {
		    
		    if($fieldName == "TAT") {
			//exlude standard TAT which is not shown
			continue;
		    }
		    
		    $colName = $helperModel->getExcelColName($colNum);
		    $cellName = $colName . ($rowNum + $top);
		    
		    if($fieldName == "SoftwareUpdate" || $fieldName == "CustomerEducation" || $fieldName == "Setup" 
		       || $fieldName == "RepairRequired" || $fieldName == "DaysTAT" ||
		       $fieldName == "DamagedBeyondRepair" || $fieldName == "FaultyAccessory" ||
		       $fieldName == "NonUKModel" || $fieldName == "Unresolvable" || $fieldName == "BranchRepair") {
			$excel->getActiveSheet()->SetCellValue($cellName, $field);
			$excel->getActiveSheet()->getStyle($cellName)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    } else if($fieldName == "ChargeableLabourCost" || $fieldName == "ChargeablePartsCost" ||
			      $fieldName == "ChargeableDeliveryCost" || $fieldName == "ChargeableVATCost" || 
			      $fieldName == "ChargeableTotalCost" || $fieldName == "Count"
			    ) {
			$excel->getActiveSheet()->SetCellValueExplicit($cellName, $field, PHPExcel_Cell_DataType::TYPE_NUMERIC);
		    } else {
			$excel->getActiveSheet()->setCellValueExplicit($cellName, $field, PHPExcel_Cell_DataType::TYPE_STRING);
		    }

		    if($rowNum % 2) {
			$excel->getActiveSheet()->getStyle($cellName)->applyFromArray($greyRow);
		    } else {
			$excel->getActiveSheet()->getStyle($cellName)->applyFromArray($whiteRow);
		    }
		    
		    if($fieldName == "DaysTAT" && $field > $job["TAT"]) {
			$excel->getActiveSheet()->getStyle($cellName)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);			
		    }
		    
		    if($fieldName == "DaysTAT" || $fieldName == "DateBooked" || $fieldName == "ClosedDate") {
			$excel->getActiveSheet()->getStyle($cellName)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		    }
		    
		    if($fieldName == "SoftwareUpdate" && $field == 1) {
			$softwareTotal++;
		    }
		    if($fieldName == "CustomerEducation" && $field == 1) {
			$educationTotal++;
		    }
		    if($fieldName == "Setup" && $field == 1) {
			$setupTotal++;
		    }
		    if($fieldName == "RepairRequired" && $field == 1) {
			$repairTotal++;
		    }
		    
		    if($rowNum % 10 == 0) {
			$q = "UPDATE report_progress SET CurrentRow = :currentRow WHERE Hash = :hash";
			$values = ["currentRow" => $rowNum, "hash" => $args["hash"]];
			$result = $this->execute($this->conn, $q, $values);
		    }
		    
		    //checking for kill switch in the database
		    if($rowNum % 100 == 0) {
			$q = "SELECT * FROM report_progress WHERE Hash = :hash";
			$values = ["hash" => $args["hash"]];
			$result = $this->query($this->conn, $q, $values);
			if(isset($result[0]) && $result[0]["Status"] == "abort") {
			   throw new Exception("Report generation aborted by admin");
			   exit();
			}
		    }
		    
		    $colNum++;
		}
		
		//release session file lock every 10 rows and sleep for a microsecond so that update progress ajax call can kick in 
		//to get progress response data
		if($rowNum % 10 == 0) {
		    $id = session_id();
		    session_write_close();
		    usleep(1);
		    session_start($id);
		}
		
	    }

	    
	    $rowCount = count($report) + $top - 1;
	    
	    $excel->getActiveSheet()->SetCellValue("S" . ($rowCount + 2), "Filtered Records");
	    $excel->getActiveSheet()->SetCellValue("S" . ($rowCount + 3), "Total Records");
	    $excel->getActiveSheet()->setCellValue("T" . ($rowCount + 2), "=SUBTOTAL(2,T10:T$rowCount)");
	    $excel->getActiveSheet()->setCellValue("T" . ($rowCount + 3), "=SUM(T10:T$rowCount)");
	    
	    $excel->getActiveSheet()->setCellValue("U" . ($rowCount + 2), "=SUBTOTAL(2,U10:U$rowCount)");
	    $excel->getActiveSheet()->setCellValue("V" . ($rowCount + 2), "=SUBTOTAL(2,V10:V$rowCount)");
	    $excel->getActiveSheet()->setCellValue("W" . ($rowCount + 2), "=SUBTOTAL(2,W10:W$rowCount)");
	    $excel->getActiveSheet()->setCellValue("X" . ($rowCount + 2), "=SUBTOTAL(2,X10:X$rowCount)");
	    $excel->getActiveSheet()->setCellValue("Y" . ($rowCount + 2), "=SUBTOTAL(2,Y10:Y$rowCount)");
	    $excel->getActiveSheet()->setCellValue("Z" . ($rowCount + 2), "=SUBTOTAL(2,Z10:Z$rowCount)");
	    $excel->getActiveSheet()->setCellValue("AA" . ($rowCount + 2), "=SUBTOTAL(2,AA10:AA$rowCount)");
	    $excel->getActiveSheet()->setCellValue("AB" . ($rowCount + 2), "=SUBTOTAL(2,AB10:AB$rowCount)");
	    $excel->getActiveSheet()->setCellValue("AC" . ($rowCount + 2), "=SUBTOTAL(2,AC10:AC$rowCount)");

	    
	    $excel->getActiveSheet()->setCellValue("U" . ($rowCount + 3), "=U" . ($rowCount + 2) . "/T" . ($rowCount + 3));
	    $excel->getActiveSheet()->setCellValue("V" . ($rowCount + 3), "=V" . ($rowCount + 2) . "/T" . ($rowCount + 3));
	    $excel->getActiveSheet()->setCellValue("W" . ($rowCount + 3), "=W" . ($rowCount + 2) . "/T" . ($rowCount + 3));
	    $excel->getActiveSheet()->setCellValue("X" . ($rowCount + 3), "=X" . ($rowCount + 2) . "/T" . ($rowCount + 3));
	    $excel->getActiveSheet()->setCellValue("Y" . ($rowCount + 3), "=Y" . ($rowCount + 2) . "/T" . ($rowCount + 3));
	    $excel->getActiveSheet()->setCellValue("Z" . ($rowCount + 3), "=Z" . ($rowCount + 2) . "/T" . ($rowCount + 3));
	    $excel->getActiveSheet()->setCellValue("AA" . ($rowCount + 3), "=AA" . ($rowCount + 2) . "/T" . ($rowCount + 3));
	    $excel->getActiveSheet()->setCellValue("AB" . ($rowCount + 3), "=AB" . ($rowCount + 2) . "/T" . ($rowCount + 3));
	    $excel->getActiveSheet()->setCellValue("AC" . ($rowCount + 3), "=AC" . ($rowCount + 2) . "/T" . ($rowCount + 3));
	    
	    $excel->getActiveSheet()->getStyle("U" . ($rowCount + 3) . ":" . "AC" . ($rowCount + 3))->getNumberFormat()->setFormatCode("0.0%");

	    $excel->getActiveSheet()->setCellValue("AG" . ($rowCount + 2), "=SUBTOTAL(9,AG10:AG$rowCount)");
	    $excel->getActiveSheet()->setCellValue("AH" . ($rowCount + 2), "=SUBTOTAL(9,AH10:AH$rowCount)");
	    $excel->getActiveSheet()->setCellValue("AI" . ($rowCount + 2), "=SUBTOTAL(9,AI10:AI$rowCount)");
	    $excel->getActiveSheet()->setCellValue("AJ" . ($rowCount + 2), "=SUBTOTAL(9,AJ10:AJ$rowCount)");
	    $excel->getActiveSheet()->setCellValue("AK" . ($rowCount + 2), "=SUBTOTAL(9,AK10:AK$rowCount)");
	    
	    $rowCount++;
	    $rowCount++;
	    
	    $excel->getActiveSheet()->getStyle("S" . $rowCount)->applyFromArray($headerStyle);
	    $excel->getActiveSheet()->getStyle("T" . $rowCount)->applyFromArray($headerStyle);
	    $excel->getActiveSheet()->getStyle("U" . $rowCount)->applyFromArray($headerStyle);
	    $excel->getActiveSheet()->getStyle("V" . $rowCount)->applyFromArray($headerStyle);
	    $excel->getActiveSheet()->getStyle("W" . $rowCount)->applyFromArray($headerStyle);
	    $excel->getActiveSheet()->getStyle("X" . $rowCount)->applyFromArray($headerStyle);
	    $excel->getActiveSheet()->getStyle("Y" . $rowCount)->applyFromArray($headerStyle);
	    $excel->getActiveSheet()->getStyle("Z" . $rowCount)->applyFromArray($headerStyle);
	    $excel->getActiveSheet()->getStyle("AA" . $rowCount)->applyFromArray($headerStyle);
	    $excel->getActiveSheet()->getStyle("AB" . $rowCount)->applyFromArray($headerStyle);
	    $excel->getActiveSheet()->getStyle("AC" . $rowCount)->applyFromArray($headerStyle);

	    $excel->getActiveSheet()->getStyle("S" . ($rowCount + 1))->applyFromArray($headerStyle);
	    $excel->getActiveSheet()->getStyle("T" . ($rowCount + 1))->applyFromArray($headerStyle);
	    $excel->getActiveSheet()->getStyle("U" . ($rowCount + 1))->applyFromArray($headerStyle);
	    $excel->getActiveSheet()->getStyle("V" . ($rowCount + 1))->applyFromArray($headerStyle);
	    $excel->getActiveSheet()->getStyle("W" . ($rowCount + 1))->applyFromArray($headerStyle);
	    $excel->getActiveSheet()->getStyle("X" . ($rowCount + 1))->applyFromArray($headerStyle);
	    $excel->getActiveSheet()->getStyle("Y" . ($rowCount + 1))->applyFromArray($headerStyle);
	    $excel->getActiveSheet()->getStyle("Z" . ($rowCount + 1))->applyFromArray($headerStyle);
	    $excel->getActiveSheet()->getStyle("AA" . ($rowCount + 1))->applyFromArray($headerStyle);
	    $excel->getActiveSheet()->getStyle("AB" . ($rowCount + 1))->applyFromArray($headerStyle);
	    $excel->getActiveSheet()->getStyle("AC" . ($rowCount + 1))->applyFromArray($headerStyle);
	    
	    $excel->getActiveSheet()->getStyle("AG" . $rowCount)->applyFromArray($headerStyle);
	    $excel->getActiveSheet()->getStyle("AH" . $rowCount)->applyFromArray($headerStyle);
	    $excel->getActiveSheet()->getStyle("AI" . $rowCount)->applyFromArray($headerStyle);
	    $excel->getActiveSheet()->getStyle("AJ" . $rowCount)->applyFromArray($headerStyle);
	    $excel->getActiveSheet()->getStyle("AK" . $rowCount)->applyFromArray($headerStyle);
	    
	    $excel->getActiveSheet()->getStyle("S" . $rowCount . ":" . "AC" . ($rowCount + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	    //set number format
	    $excel->getActiveSheet()->getStyle("AG" . $top . ":AK" . ($rowCount + 2))->getNumberFormat()->setFormatCode("0.00");		    
	    $excel->getActiveSheet()->getStyle("AG" . $top . ":AK" . ($rowCount + 2))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	    
	    
	    //set auto width to columns
	    $colNum = $left;
	    foreach($header as $name) {
		$colName = $helperModel->getExcelColName($colNum);
		$excel->getActiveSheet()->getColumnDimension($colName)->setAutoSize(true);
		$colNum++;
	    }
	    
	    /*
	    //this is total brute force - had to edit the library itself to change $_columnDimensions and $_width into public vars
	    $columnDimensions = $excel->getActiveSheet()->calculateColumnWidths()->_columnDimensions;
	    $colNum = $left;
	    foreach($header as $name) {
		$colName = $helperModel->getExcelColName($colNum);
		$excel->getActiveSheet()->getColumnDimension($colName)->setAutoSize(false);
		switch ($colName) {
		    case "B": {
			$excel->getActiveSheet()->getColumnDimension($colName)->setAutoSize(false);
			$width = $columnDimensions[$colName]->_width - ($columnDimensions[$colName]->_width * 0.1);
			$excel->getActiveSheet()->getColumnDimension($colName)->setWidth($width);
		    }
		    case "D": {
			$excel->getActiveSheet()->getColumnDimension($colName)->setAutoSize(false);
			$width = $columnDimensions[$colName]->_width - ($columnDimensions[$colName]->_width * 0.1);
			$excel->getActiveSheet()->getColumnDimension($colName)->setWidth($width);
		    }
		    case "F": {
			$excel->getActiveSheet()->getColumnDimension($colName)->setAutoSize(false);
			$width = $columnDimensions[$colName]->_width - ($columnDimensions[$colName]->_width * 0.1);
			$excel->getActiveSheet()->getColumnDimension($colName)->setWidth($width);
		    }
		    case "H": {
			$excel->getActiveSheet()->getColumnDimension($colName)->setAutoSize(false);
			$width = $columnDimensions[$colName]->_width - ($columnDimensions[$colName]->_width * 0.1);
			$excel->getActiveSheet()->getColumnDimension($colName)->setWidth($width);
		    }
		    case "I": {
			$excel->getActiveSheet()->getColumnDimension($colName)->setAutoSize(false);
			$width = $columnDimensions[$colName]->_width - ($columnDimensions[$colName]->_width * 0.1);
			$excel->getActiveSheet()->getColumnDimension($colName)->setWidth($width);
		    }
		    case "K": {
			$excel->getActiveSheet()->getColumnDimension($colName)->setAutoSize(false);
			$width = $columnDimensions[$colName]->_width - ($columnDimensions[$colName]->_width * 0.1);
			$excel->getActiveSheet()->getColumnDimension($colName)->setWidth($width);
		    }
		    case "L": {
			$excel->getActiveSheet()->getColumnDimension($colName)->setAutoSize(false);
			$width = $columnDimensions[$colName]->_width - ($columnDimensions[$colName]->_width * 0.2);
			$excel->getActiveSheet()->getColumnDimension($colName)->setWidth($width);
		    }
		    case "M": {
			$excel->getActiveSheet()->getColumnDimension($colName)->setAutoSize(false);
			$width = $columnDimensions[$colName]->_width + ($columnDimensions[$colName]->_width * 0.1);
			$excel->getActiveSheet()->getColumnDimension($colName)->setWidth($width);
		    }
		    case "Q": {
			$excel->getActiveSheet()->getColumnDimension($colName)->setAutoSize(false);
			$width = $columnDimensions[$colName]->_width + ($columnDimensions[$colName]->_width * 0.2);
			$excel->getActiveSheet()->getColumnDimension($colName)->setWidth($width);
		    }
		}

		$colNum++;
	    }
	    */	    
	    
	    $excel->getActiveSheet()->getColumnDimension("C")->setAutoSize(false);
	    $excel->getActiveSheet()->getColumnDimension("C")->setWidth(12);
	    
	    $excel->getActiveSheet()->getColumnDimension("E")->setAutoSize(false);
	    $excel->getActiveSheet()->getColumnDimension("E")->setWidth(26);
	    
	    $excel->getActiveSheet()->getColumnDimension("G")->setAutoSize(false);
	    $excel->getActiveSheet()->getColumnDimension("G")->setWidth(22);
	    
	    $excel->getActiveSheet()->getColumnDimension("K")->setAutoSize(false);
	    $excel->getActiveSheet()->getColumnDimension("K")->setWidth(13);
	    
	    $excel->getActiveSheet()->getColumnDimension("M")->setAutoSize(false);
	    $excel->getActiveSheet()->getColumnDimension("M")->setWidth(20.5);
	    
	    $excel->getActiveSheet()->getColumnDimension("O")->setAutoSize(false);
	    $excel->getActiveSheet()->getColumnDimension("O")->setWidth(22.5);
	    
	    $excel->getActiveSheet()->getColumnDimension("N")->setAutoSize(false);
	    $excel->getActiveSheet()->getColumnDimension("N")->setWidth(10);
	    
	    $excel->getActiveSheet()->getColumnDimension("P")->setAutoSize(false);
	    $excel->getActiveSheet()->getColumnDimension("P")->setWidth(30);
	    
	    $excel->getActiveSheet()->getColumnDimension("S")->setAutoSize(false);
	    $excel->getActiveSheet()->getColumnDimension("S")->setWidth(30);
	    
	    $excel->getActiveSheet()->getColumnDimension("T")->setAutoSize(false);
	    $excel->getActiveSheet()->getColumnDimension("T")->setWidth(5);
	    
	    $excel->getActiveSheet()->getColumnDimension("U")->setAutoSize(false);
	    $excel->getActiveSheet()->getColumnDimension("U")->setWidth(16.5);
	    
	    $excel->getActiveSheet()->getColumnDimension("W")->setAutoSize(false);
	    $excel->getActiveSheet()->getColumnDimension("W")->setWidth(8);
	    
	    $excel->getActiveSheet()->getColumnDimension("X")->setAutoSize(false);
	    $excel->getActiveSheet()->getColumnDimension("X")->setWidth(10.5);
	    
	    $excel->getActiveSheet()->getColumnDimension("Z")->setAutoSize(false);
	    $excel->getActiveSheet()->getColumnDimension("Z")->setWidth(13);
	    
	    $excel->getActiveSheet()->getColumnDimension("AC")->setAutoSize(false);
	    $excel->getActiveSheet()->getColumnDimension("AC")->setWidth(16);
	    
	    $excel->getActiveSheet()->getColumnDimension("AH")->setAutoSize(false);
	    $excel->getActiveSheet()->getColumnDimension("AH")->setWidth(13);
	    
	    $excel->getActiveSheet()->getColumnDimension("AI")->setAutoSize(false);
	    $excel->getActiveSheet()->getColumnDimension("AI")->setWidth(15.5);
	    
	    $excel->getActiveSheet()->getColumnDimension("AJ")->setAutoSize(false);
	    $excel->getActiveSheet()->getColumnDimension("AJ")->setWidth(8);
	    
	    //enable filtering on columns
	    $excel->getActiveSheet()->setAutoFilter("C9:AK" . ($rowCount - 2));
	    
	    
	    //logo
	    if(isset($branch[0]["branchID"]) && $branch[0]["branchID"]) {
		
		$q = "  SELECT	    brand.BrandLogo 
			FROM	    brand_branch
			LEFT JOIN   brand ON brand_branch.BrandID = brand.BrandID
			WHERE	    brand_branch.BranchID = :branchID
		     ";
		$values = ["branchID" => $branch[0]["branchID"]];
		$result = $this->query($this->conn, $q, $values);
		
		if(isset($result[0]["BrandLogo"]) && $result[0]["BrandLogo"]) {
	    
		    $logo = new PHPExcel_Worksheet_Drawing();
		    $logo->setName("Logo");
		    $logo->setDescription("Logo");
		    $logo->setPath("images/brandLogos/" . $result[0]["BrandLogo"]);
		    $logo->setCoordinates("D2");
		    $logo->setOffsetY(10);
		    $logo->setWorksheet($excel->getActiveSheet());
		    
		}
		
	    }	
	    
	    
	    //hide the gridlines
	    $excel->getActiveSheet()->setShowGridlines(false);
	    
	    //Sticky header
	    $excel->getActiveSheet()->freezePane("A10");
	    
	}
	
	$excelWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
	//$excelWriter->save("application/downloads/reports_temp/{$args["filename"]}.xlsx"); 
	$excelWriter->save("reports/{$args["filename"]}.xlsx"); 
	
	$q = "UPDATE report_progress SET Status = 'finished' WHERE Hash = :hash";
	$values = ["hash" => $args["hash"]];
	$result = $this->execute($this->conn, $q, $values);
	
	exit();
    }
    
    
    
    public function reportProgress() {
	
	$data = $_REQUEST;
	
	$q = "SELECT * FROM report_progress WHERE Hash = :hash";
	$values = ["hash" => $data["hash"]];
	$result = $this->query($this->conn, $q, $values);
	
	if(count($result) == 0) {
	    //generator still initiating
	    $return = '{"status": "initiating", "total": 0, "current": 0}';
	} else {
	    //generator in progress
	    $return = '{"status": "' . $result[0]["Status"] . '", "total": ' . $result[0]["TotalRows"] . ', "current": ' . ($result[0]["CurrentRow"] + 1) . '}';
	}
	
	return $return;
	
    }
    
    
    
    public function deleteReportHash($hash) {
	
	$q = "DELETE FROM report_progress WHERE Hash = :hash";
	$values = ["hash" => $hash];
	$result = $this->execute($this->conn, $q, $values);
	
    }
    
    
    
    public function getUserReports() {
	
	$q = "	SELECT	UserReportID	AS '0',
			Name		AS '1',
			Description	AS '2',
			NULL		AS '3'
		FROM	user_reports
		WHERE	UserID = :userID
	     ";
	$values = ["userID" => $this->controller->user->UserID];
	$result = $this->query($this->conn, $q, $values);
	return $result;
	
    }
    
    
    
    public function getPrimaryTables() {
	
	$q = "	SELECT	TABLE_NAME AS name,
			TABLE_COMMENT AS comment
		FROM	TABLES
		WHERE	TABLE_SCHEMA = :db
	     ";
	
	$db = explode("=", $this->controller->config['DataBase']['Conn'])[2];
	
	$values = ["db" => $db];
	
	$result = $this->query($this->schemaConn, $q, $values);

	$tables = [];
	
	foreach ($result as $table) {
	    $json = json_decode($table["comment"]);
	    if ($json != null && isset($json->Primary) && $json->Primary && isset($json->Active) && $json->Active) {
		switch ($this->controller->user->UserType) {
		    case "Admin": {
			$tables[] = ["SystemName" => $table["name"], "Name" => $json->Name];
			break;
		    }
		    case "Network": {
			if ($json->UserTypes->Network) {
			    $tables[] = ["SystemName" => $table["name"], "Name" => $json->Name];
			}
			break;
		    }
		    case "Client": {
			if ($json->UserTypes->Client) {
			    $tables[] = ["SystemName" => $table["name"], "Name" => $json->Name];
			}
			break;
		    }
		    case "Branch": {
			if ($json->UserTypes->Branch) {
			    $tables[] = ["SystemName" => $table["name"], "Name" => $json->Name];
			}
			break;
		    }
		    case "ServiceProvider": {
			if ($json->UserTypes->ServiceProvider) {
			    $tables[] = ["SystemName" => $table["name"], "Name" => $json->Name];
			}
			break;
		    }
		    case "Manufacturer": {
			if ($json->UserTypes->Manufacturer) {
			    $tables[] = ["SystemName" => $table["name"], "Name" => $json->Name];
			}
			break;
		    }
		}
	    }
	}
	
	return $tables;
	
    }
    
    
    
    public function getSecondaryTables($table) {

	$q = "	SELECT	    col.TABLE_NAME AS name,
			    tbls.TABLE_COMMENT AS comment
		FROM	    KEY_COLUMN_USAGE AS col
		LEFT JOIN   TABLES AS tbls ON tbls.TABLE_NAME = col.TABLE_NAME AND 
			    tbls.TABLE_SCHEMA = col.CONSTRAINT_SCHEMA
		WHERE	    col.REFERENCED_TABLE_NAME = :table AND 
			    col.CONSTRAINT_SCHEMA = :db AND
			    tbls.TABLE_SCHEMA = :db
	     ";
	$db = explode("=", $this->controller->config['DataBase']['Conn'])[2];
	$values = ["table" => $table, "db" => $db];
	$result = $this->query($this->schemaConn, $q, $values);
	$multiTables = [];
	foreach($result as $table) {
	    $json = json_decode($table["comment"]);
	    if($json != null && isset($json->Active) && $json->Active) {
		switch ($this->controller->user->UserType) {
		    case "Admin": {
			$multiTables[] = ["SystemName" => $table["name"], "Name" => $json->Name];
			break;
		    }
		    case "Network": {
			if ($json->UserTypes->Network) {
			    $multiTables[] = ["SystemName" => $table["name"], "Name" => $json->Name];
			}
			break;
		    }
		    case "Client": {
			if ($json->UserTypes->Client) {
			    $multiTables[] = ["SystemName" => $table["name"], "Name" => $json->Name];
			}
			break;
		    }
		    case "Branch": {
			if ($json->UserTypes->Branch) {
			    $multiTables[] = ["SystemName" => $table["name"], "Name" => $json->Name];
			}
			break;
		    }
		    case "ServiceProvider": {
			if ($json->UserTypes->ServiceProvider) {
			    $multiTables[] = ["SystemName" => $table["name"], "Name" => $json->Name];
			}
			break;
		    }
		    case "Manufacturer": {
			if ($json->UserTypes->Manufacturer) {
			    $multiTables[] = ["SystemName" => $table["name"], "Name" => $json->Name];
			}
			break;
		    }
		}
	    }
	}

	$q = "	SELECT	    col.REFERENCED_TABLE_NAME AS name,
			    tbls.TABLE_COMMENT AS comment
		FROM	    KEY_COLUMN_USAGE AS col
		LEFT JOIN   TABLES AS tbls ON tbls.TABLE_NAME = col.REFERENCED_TABLE_NAME AND 
			    tbls.TABLE_SCHEMA = col.CONSTRAINT_SCHEMA
		WHERE	    col.TABLE_NAME = :table AND 
			    col.CONSTRAINT_SCHEMA = :db AND
			    tbls.TABLE_SCHEMA = :db AND
			    col.CONSTRAINT_NAME != 'PRIMARY'
		GROUP BY    col.REFERENCED_TABLE_NAME
	     ";
	$result = $this->query($this->schemaConn, $q, $values);
	$secondaryTables = [];
	foreach($result as $table) {
	    $json = json_decode($table["comment"]);
	    if($json != null && isset($json->Active) && $json->Active) {
		switch ($this->controller->user->UserType) {
		    case "Admin": {
			$secondaryTables[] = ["SystemName" => $table["name"], "Name" => $json->Name];
			break;
		    }
		    case "Network": {
			if ($json->UserTypes->Network) {
			    $secondaryTables[] = ["SystemName" => $table["name"], "Name" => $json->Name];
			}
			break;
		    }
		    case "Client": {
			if ($json->UserTypes->Client) {
			    $secondaryTables[] = ["SystemName" => $table["name"], "Name" => $json->Name];
			}
			break;
		    }
		    case "Branch": {
			if ($json->UserTypes->Branch) {
			    $secondaryTables[] = ["SystemName" => $table["name"], "Name" => $json->Name];
			}
			break;
		    }
		    case "ServiceProvider": {
			if ($json->UserTypes->ServiceProvider) {
			    $secondaryTables[] = ["SystemName" => $table["name"], "Name" => $json->Name];
			}
			break;
		    }
		    case "Manufacturer": {
			if ($json->UserTypes->Manufacturer) {
			    $secondaryTables[] = ["SystemName" => $table["name"], "Name" => $json->Name];
			}
			break;
		    }
		}
	    }
	}
	
	$return = ["multiple" => $multiTables, "secondary" => $secondaryTables];
	
	return $return;
	
    }
    
    
    
    public function getTableColumns($tableName) {
	
	$q = "	SELECT	COLUMN_NAME AS ColumnName, 
			TABLE_NAME AS TableName,
			COLUMN_COMMENT AS comment
		FROM	COLUMNS 
		WHERE	TABLE_SCHEMA = :db AND
			TABLE_NAME = :table
	     ";
	
	$db = explode("=", $this->controller->config["DataBase"]["Conn"])[2];
	
	$values = ["db" => $db, "table" => $tableName];
	
	$result = $this->query($this->schemaConn, $q, $values);

	$columns = [];
	
	foreach($result as $col) {
	    $json = json_decode($col["comment"]);
	    if($json != null && isset($json->Active) && $json->Active) {
		switch ($this->controller->user->UserType) {
		    case "Admin": {
			$columns[] = ["SystemName" => $col["ColumnName"], "ColumnName" => $json->Name, "TableName" => $col["TableName"]];
			break;
		    }
		    case "Network": {
			if ($json->UserTypes->Network) {
			    $columns[] = ["SystemName" => $col["ColumnName"], "ColumnName" => $json->Name, "TableName" => $col["TableName"]];
			}
			break;
		    }
		    case "Client": {
			if ($json->UserTypes->Client) {
			    $columns[] = ["SystemName" => $col["ColumnName"], "ColumnName" => $json->Name, "TableName" => $col["TableName"]];
			}
			break;
		    }
		    case "Branch": {
			if ($json->UserTypes->Branch) {
			    $columns[] = ["SystemName" => $col["ColumnName"], "ColumnName" => $json->Name, "TableName" => $col["TableName"]];
			}
			break;
		    }
		    case "ServiceProvider": {
			if ($json->UserTypes->ServiceProvider) {
			    $columns[] = ["SystemName" => $col["ColumnName"], "ColumnName" => $json->Name, "TableName" => $col["TableName"]];
			}
			break;
		    }
		    case "Manufacturer": {
			if ($json->UserTypes->Manufacturer) {
			    $columns[] = ["SystemName" => $col["ColumnName"], "ColumnName" => $json->Name, "TableName" => $col["TableName"]];
			}
			break;
		    }
		}
	    }
	}
	
	return $columns;
	
    }
    

    
    public function serviceActionTrackingReport($data) {
	
	$file = json_encode($data);

	$input = $data["hash"]; 
	
	//create json file here
	$jsonArr = [
	    "networkID" => $this->controller->user->NetworkID,
	    "clientID" => $this->controller->user->ClientID,
	    "clientName" => $this->controller->user->ClientName,
	    "branches" => json_decode($data["branches"]),
	    "branchID" => $this->controller->user->BranchID,
	    "branchName" => ($this->controller->user->BranchName ? $this->controller->user->BranchName : null),
	    "logo" => $this->controller->user->BrandLogo,
	    "dateFrom" => $data["dateFrom"],
	    "dateTo" => $data["dateTo"]
	];
	
	$json = json_encode($jsonArr);
	file_put_contents("temp/" . $input . ".json", $json);
	
	$path = str_replace("/index.php", "/jar/service_action_tracking_report.jar", $_SERVER["SCRIPT_FILENAME"]);
	
	$result = exec("java -jar " . $path . " " . $input, $output, $return);
	
	return isset($output[0]) ? $output[0] : null;
	
    }
    
    
    
    public function saveUserReport($data, $id, $schedule, $scheduleStatus) {
	
	$structure = new stdClass(); 
	$structure->primaryTable = $data->primaryTable;
	$structure->columns = $data->columns;
	$structure->multiTable = $data->multiTable;
	$s = \json_encode($structure);
	
	$values = [
	    "name" => $data->name,
	    "description" => $data->description,
	    "structure" => $s,
	    "schedule" => $schedule,
	    "scheduleStatus" => $scheduleStatus,
	    "userID" => $this->controller->user->UserID,
	    "userType" => $this->controller->user->UserType
	];
	
	switch ($this->controller->user->UserType) {
	    case "Admin": {
		$values["userTypeID"] = null;
		break;
	    }
	    case "Network": {
		$values["userTypeID"] = $this->controller->user->NetworkID;
		break;
	    }
	    case "Client": {
		$values["userTypeID"] = $this->controller->user->ClientID;
		break;
	    }
	    case "Branch": {
		$values["userTypeID"] = $this->controller->user->BranchID;
		break;
	    }
	    case "ServiceProvider": {
		$values["userTypeID"] = $this->controller->user->ServiceProviderID;
		break;
	    }
	    case "Manufacturer": {
		$values["userTypeID"] = $this->controller->user->ManufacturerID;
		break;
	    }
	}
	
	if ($id) {
	    
	    $q = "  UPDATE  user_reports
		    SET	    Name = :name,
			    Description = :description,
			    Structure = :structure,
			    Schedule = :schedule,
			    ScheduleStatus = :scheduleStatus,
			    ModifiedUserID = :userID,
			    UserType = :userType,
			    UserTypeID = :userTypeID
		    WHERE   UserReportID = :id
		 ";

	    $values["id"] = $id;
	    
	    $result = $this->execute($this->conn, $q, $values);
	    
	} else {
	    
	    $q = "  INSERT INTO user_reports
				(
				    UserID,
				    UserType,
				    UserTypeID,
				    Name,
				    Description,
				    Structure,
				    Schedule,
				    ScheduleStatus,
				    ModifiedUserID
				)
		    VALUES
				(
				    :userID,
				    :userType,
				    :userTypeID,
				    :name,
				    :description,
				    :structure,
				    :schedule,
				    :scheduleStatus,
				    :userID
				)
		 ";
	    
	    $result = $this->execute($this->conn, $q, $values);
	    
	}
	    
	return $result;
	
    }
    
    
    
    public function runUserReport($reportID, $filter = null) {
	
	$input = md5(microtime());
	
	$jsonArr = new stdClass();
	$jsonArr->reportID = $reportID;
	$jsonArr->filter = $filter;
	
	//scope
	switch ($this->controller->user->UserType) {
	    case "Admin": {
		$jsonArr->userType = "Admin";
		break;
	    }
	    case "Network": {
		$jsonArr->userType = "Network";
		$jsonArr->userTypeID = $this->controller->user->NetworkID;
		break;
	    }
	    case "Client": {
		$jsonArr->userType = "Client";
		$jsonArr->userTypeID = $this->controller->user->ClientID;
		break;
	    }
	    case "Branch": {
		$jsonArr->userType = "Branch";
		$jsonArr->userTypeID = $this->controller->user->BranchID;
		break;
	    }
	    case "ServiceProvider": {
		$jsonArr->userType = "ServiceProvider";
		$jsonArr->userTypeID = $this->controller->user->ServiceProviderID;
		break;
	    }
	    case "Manufacturer": {
		$jsonArr->userType = "Manufacturer";
		$jsonArr->userTypeID = $this->controller->user->ManufacturerID;
		break;
	    }
	}
	
	
	$json = \json_encode($jsonArr);
	
	file_put_contents("temp/" . $input . ".json", $json);
	
	$path = str_replace("/index.php", "/jar/run_user_report.jar", $_SERVER["SCRIPT_FILENAME"]);
	
	$result = \exec("java -jar " . $path . " " . $input, $output, $return);
	
	return isset($output[0]) ? $output[0] : null;
	
    }
    
    
    
    public function getReportRowCount($data = null, $id = null, $filter = null) {
	
	$input = md5(microtime());
	
	$jsonArr = new stdClass();
	
	if($data) {
	    $jsonArr->structure = $data;
	} else if ($id) {
	    $jsonArr->structure = self::getReportStructure($id);
	} else {
	    throw new Exception("Missing report structure - can't call Java application without it");
	}

	$jsonArr->getCount = true;
	$jsonArr->userID = $this->controller->user->UserID;
	$jsonArr->reportID = $id;
	$jsonArr->filter = $filter;
	
	//scope
	switch ($this->controller->user->UserType) {
	    case "Admin": {
		$jsonArr->userType = "Admin";
		break;
	    }
	    case "Network": {
		$jsonArr->userType = "Network";
		$jsonArr->userTypeID = $this->controller->user->NetworkID;
		break;
	    }
	    case "Client": {
		$jsonArr->userType = "Client";
		$jsonArr->userTypeID = $this->controller->user->ClientID;
		break;
	    }
	    case "Branch": {
		$jsonArr->userType = "Branch";
		$jsonArr->userTypeID = $this->controller->user->BranchID;
		break;
	    }
	    case "ServiceProvider": {
		$jsonArr->userType = "ServiceProvider";
		$jsonArr->userTypeID = $this->controller->user->ServiceProviderID;
		break;
	    }
	    case "Manufacturer": {
		$jsonArr->userType = "Manufacturer";
		$jsonArr->userTypeID = $this->controller->user->ManufacturerID;
		break;
	    }
	}
	
	
	$json = \json_encode($jsonArr);
	
	file_put_contents("temp/" . $input . ".json", $json);
	
	$path = str_replace("/index.php", "/jar/run_user_report.jar", $_SERVER["SCRIPT_FILENAME"]);
	
	$result = \exec("java -jar " . $path . " " . $input, $output, $return);
	
	return isset($output[0]) ? $output[0] : null;
	
    }
    
    
    
    public function getReportData($reportID) {
	
	$q = "SELECT * FROM user_reports WHERE UserReportID = :reportID";
	$values = ["reportID" => $reportID];
	$result = $this->query($this->conn, $q, $values);
	return isset($result[0]) ? $result[0] : null;
	
    }
    
    
    
    public function getReportSchedule($reportID) {
	
	$q = "SELECT Schedule, ScheduleStatus FROM user_reports WHERE UserReportID = :id";
	$values = ["id" => $reportID];
	$result = $this->query($this->conn, $q, $values);
	if (isset($result[0])) {
	    $return = new stdClass();
	    $return->status = $result[0]["ScheduleStatus"];
	    $return->data = ($result[0]["Schedule"] == null) ? null : json_decode($result[0]["Schedule"]);
	    return $return;
	} else {
	    return null;
	}
	
    }    
    
    
    
    public function getReportStructure($reportID) {
	
	$data = self::getReportData($reportID);
	return isset($data["Structure"]) ? json_decode($data["Structure"]) : null;
	
    }
    
    
    
    public function getTableComment($table) {
	
	$q = "	SELECT	TABLE_COMMENT 
		FROM	TABLES 
		WHERE	TABLE_SCHEMA = :db AND
			TABLE_NAME = :table
	     ";
	$db = explode("=", $this->controller->config["DataBase"]["Conn"])[2];
	$values = ["db" => $db, "table" => $table];
	
	$result = $this->query($this->schemaConn, $q, $values);
	
	return isset($result[0]["TABLE_COMMENT"]) ? json_decode($result[0]["TABLE_COMMENT"]) : null;
	
    }
    
    
    
    public function getColumnData($column, $table, $reportID) {
	
	$q = "	SELECT	COLUMN_COMMENT AS comment
		FROM	COLUMNS 
		WHERE	COLUMNS.TABLE_SCHEMA = :db AND 
			COLUMNS.TABLE_NAME = :table AND 
			COLUMNS.COLUMN_NAME = :column
	     "; 
	
	$db = explode("=", $this->controller->config["DataBase"]["Conn"])[2];
	
	$values = [
	    "db" => $db,
	    "table" => $table,
	    "column" => $column
	];
	
	$result = $this->query($this->schemaConn, $q, $values);
	
	if(count($result) != 0 && $result[0]["comment"]) {
	    
	    $data = json_decode($result[0]["comment"]);
	    
	    if($data->RefTable && $data->RefTable->RefTableCol && $data->RefTable->RefTableID && $data->RefTable->RefTableName) {
		
		$json = self::getTableComment($data->RefTable->RefTableName);
		
		if (!$json && $this->controller->user->UserType != "Admin") {
		    return json_encode([]);
		}
		
		
		//if result count is over 500 return empty array - otherwise it will be too much for the browser (firefox crashes)
		$q = "SELECT COUNT(*) AS count FROM {$data->RefTable->RefTableName}";
		$values = [];
		
		switch ($this->controller->user->UserType) {
		    case "Network": {
			$q .= isset($json->Scope->Network->Join) ? (" " . $json->Scope->Network->Join) : "";
			if (isset($json->Scope->Network->Where)) {
			    $q .= " WHERE " . $json->Scope->Network->Where;
			    $values["id"] = $this->controller->user->NetworkID;
			}
			if (isset($json->Scope->Network->GroupBy)) {
			    $q .= " GROUP BY " . $json->Scope->Network->GroupBy;
			}
			break;
		    }
		    case "Client": {
			$q .= isset($json->Scope->Client->Join) ? (" " . $json->Scope->Client->Join) : "";
			if (isset($json->Scope->Client->Where)) {
			    $q .= " WHERE " . $json->Scope->Client->Where;
			    $values["id"] = $this->controller->user->ClientID;
			}
			if (isset($json->Scope->Client->GroupBy)) {
			    $q .= " GROUP BY " . $json->Scope->Client->GroupBy;
			}
			break;
		    }
		    case "Branch": {
			$q .= isset($json->Scope->Branch->Join) ? (" " . $json->Scope->Branch->Join) : "";
			if (isset($json->Scope->Branch->Where)) {
			    $q .= " WHERE " . $json->Scope->Branch->Where;
			    $values["id"] = $this->controller->user->BranchID;
			}
			if (isset($json->Scope->Branch->GroupBy)) {
			    $q .= " GROUP BY " . $json->Scope->Branch->GroupBy;
			}
			break;
		    }
		    case "ServiceProvider": {
			$q .= isset($json->Scope->ServiceProvider->Join) ? (" " . $json->Scope->ServiceProvider->Join) : "";
			if (isset($json->Scope->ServiceProvider->Where)) {
			    $q .= " WHERE " . $json->Scope->ServiceProvider->Where;
			    $values["id"] = $this->controller->user->ServiceProviderID;
			}
			if (isset($json->Scope->ServiceProvider->GroupBy)) {
			    $q .= " GROUP BY " . $json->Scope->ServiceProvider->GroupBy;
			}
			break;
		    }
		    case "Manufacturer": {
			$q .= isset($json->Scope->Manufacturer->Join) ? (" " . $json->Scope->Manufacturer->Join) : "";
			if (isset($json->Scope->Manufacturer->Where)) {
			    $q .= " WHERE " . $json->Scope->Manufacturer->Where;
			    $values["id"] = $this->controller->user->ManufacturerID;
			}
			if (isset($json->Scope->Manufacturer->GroupBy)) {
			    $q .= " GROUP BY " . $json->Scope->Manufacturer->GroupBy;
			}
			break;
		    }
		}
		
		$countResult = $this->query($this->conn, $q, $values);
		if ($countResult[0]["count"] > 500) {
		    return json_encode([]);
		}
		
		$q = "	SELECT	'Blank' AS col, 
				-1 AS id
			UNION
			SELECT	{$data->RefTable->RefTableCol} AS col,
				{$data->RefTable->RefTableName}.{$data->RefTable->RefTableID} AS id
			FROM	{$data->RefTable->RefTableName} 
		     ";
		
		$values = [];
		
		switch ($this->controller->user->UserType) {
		    case "Network": {
			$q .= isset($json->Scope->Network->Join) ? (" " . $json->Scope->Network->Join) : "";
			if (isset($json->Scope->Network->Where)) {
			    $q .= " WHERE " . $json->Scope->Network->Where;
			    $values["id"] = $this->controller->user->NetworkID;
			}
			break;
		    }
		    case "Client": {
			$q .= isset($json->Scope->Client->Join) ? (" " . $json->Scope->Client->Join) : "";
			if (isset($json->Scope->Client->Where)) {
			    $q .= " WHERE " . $json->Scope->Client->Where;
			    $values["id"] = $this->controller->user->ClientID;
			}
			break;
		    }
		    case "Branch": {
			$q .= isset($json->Scope->Branch->Join) ? (" " . $json->Scope->Branch->Join) : "";
			if (isset($json->Scope->Branch->Where)) {
			    $q .= " WHERE " . $json->Scope->Branch->Where;
			    $values["id"] = $this->controller->user->BranchID;
			}
			break;
		    }
		    case "ServiceProvider": {
			$q .= isset($json->Scope->ServiceProvider->Join) ? (" " . $json->Scope->ServiceProvider->Join) : "";
			if (isset($json->Scope->ServiceProvider->Where)) {
			    $q .= " WHERE " . $json->Scope->ServiceProvider->Where;
			    $values["id"] = $this->controller->user->ServiceProviderID;
			}
			break;
		    }
		    case "Manufacturer": {
			$q .= isset($json->Scope->Manufacturer->Join) ? (" " . $json->Scope->Manufacturer->Join) : "";
			if (isset($json->Scope->Manufacturer->Where)) {
			    $q .= " WHERE " . $json->Scope->Manufacturer->Where;
			    $values["id"] = $this->controller->user->ManufacturerID;
			}
			break;
		    }
		}
		
		$result = $this->query($this->conn, $q, $values);
		
		if (count($result) != 0) {
		    $report = self::getReportData($reportID);
		    if (count($report) != 0) {
			$structure = json_decode($report["Structure"]);
			foreach ($structure->columns as $col) {
			    if ($col->table == $table && $col->column == $column) {
				foreach ($result as $key => $row) {
				    if ($col->subset && in_array($row["id"], $col->subset)) {
					$result[$key]["selected"] = true;
				    }
				}
			    }
			}
		    }
		    return json_encode($result);
		} else {
		    return null;
		}
		
	    }
	    
	} else {
	    
	    return null;
	    
	}
	
    }
    
    
    
    public function getReportFilterColumns($structure) {
	
	$where = "";
	
	$tables = [];
	foreach ($structure->columns as $column) {
	    $tables[] = $column->table;
	}
	$tables = array_unique($tables);
	
	foreach ($tables as $table) {
	    if ($where == "") {
		$where .= "col.TABLE_NAME = '{$table}'";
	    } else {
		$where .= " OR col.TABLE_NAME = '{$table}'";
	    }
	}
	
	if ($where == "") {
	    return null;
	}
	
	$q = "	SELECT	col.TABLE_NAME AS colTable,
			col.COLUMN_NAME AS colName,
			col.COLUMN_COMMENT AS colComment,
			col.DATA_TYPE AS colType
			
		FROM	COLUMNS AS col
			
		WHERE	col.TABLE_SCHEMA = :db AND 
			(col.DATA_TYPE = 'timestamp' OR col.DATA_TYPE = 'date' OR col.DATA_TYPE = 'datetime') AND
			col.COLUMN_COMMENT IS NOT NULL AND col.COLUMN_COMMENT != '' AND
			($where)
	     ";
	$db = explode("=", $this->controller->config["DataBase"]["Conn"])[2];
	$values = ["db" => $db];
	$result = $this->query($this->schemaConn, $q, $values);

	$return = [];
	
	foreach ($result as $key => $row) {
	    $json = json_decode($row["colComment"]);
	    if (isset($json->DateRange) && $json->DateRange) {
		$row["structure"] = $json;
		unset($row["colComment"]);
		$return[] = $row;
	    }
	}
	
	return $return;
	
    }    
    
    
    
    public function deleteReports($reports) {
	
	foreach ($reports as $id) {
	    $q = "DELETE FROM user_reports WHERE UserReportID = :id";
	    $values = ["id" => $id];
	    $this->execute($this->conn, $q, $values);
	}
	
    }
    
    
    
    public function getHelpInfo($code) {
	
	$q = "SELECT HelpTextTitle AS title, HelpText AS text FROM help_text WHERE HelpTextCode = :code";
	$values = ["code" => $code];
	$result = $this->query($this->conn, $q, $values);
	return isset($result[0]) ? $result[0] : null;
	
    }
    
    
    
    public function saveHelpInfo($data) {
	
	$q = "	UPDATE	help_text
		SET	HelpTextTitle = :title,
			HelpText = :text
		WHERE	HelpTextCode = :code
	     ";
	$values = [
	    "title" => $data["title"],
	    "text" => $data["text"],
	    "code" => $data["code"]
	];
	$result = $this->execute($this->conn, $q, $values);
	return $result;
	
    }
    
    
    
}
?>