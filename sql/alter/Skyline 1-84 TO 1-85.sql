# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-10-02 10:50                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.84');

# ---------------------------------------------------------------------- #
# Drop foreign key constraints                                           #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider` DROP FOREIGN KEY `county_TO_service_provider`;

ALTER TABLE `service_provider` DROP FOREIGN KEY `country_TO_service_provider`;

ALTER TABLE `service_provider` DROP FOREIGN KEY `user_TO_service_provider`;

ALTER TABLE `brand` DROP FOREIGN KEY `client_TO_brand`;

ALTER TABLE `brand` DROP FOREIGN KEY `network_TO_brand`;

ALTER TABLE `brand` DROP FOREIGN KEY `user_TO_brand`;

ALTER TABLE `email` DROP FOREIGN KEY `user_TO_email`;

ALTER TABLE `email_job` DROP FOREIGN KEY `job_TO_email_job`;

ALTER TABLE `email_job` DROP FOREIGN KEY `email_TO_email_job`;

ALTER TABLE `email_job` DROP FOREIGN KEY `user_TO_email_job`;

ALTER TABLE `network_service_provider` DROP FOREIGN KEY `service_provider_TO_network_service_provider`;

ALTER TABLE `user` DROP FOREIGN KEY `service_provider_TO_user`;

ALTER TABLE `job` DROP FOREIGN KEY `service_provider_TO_job`;

ALTER TABLE `brand_branch` DROP FOREIGN KEY `brand_TO_brand_branch`;

ALTER TABLE `job_type` DROP FOREIGN KEY `brand_TO_job_type`;

ALTER TABLE `county` DROP FOREIGN KEY `brand_TO_county`;

ALTER TABLE `country` DROP FOREIGN KEY `brand_TO_country`;

ALTER TABLE `payment_type` DROP FOREIGN KEY `brand_TO_payment_type`;

ALTER TABLE `customer_title` DROP FOREIGN KEY `brand_TO_customer_title`;

ALTER TABLE `client_purchase_order` DROP FOREIGN KEY `brand_TO_client_purchase_order`;

ALTER TABLE `service_type` DROP FOREIGN KEY `brand_TO_service_type`;

ALTER TABLE `user` DROP FOREIGN KEY `brand_TO_user`;

ALTER TABLE `audit_trail_action` DROP FOREIGN KEY `brand_TO_audit_trail_action`;

ALTER TABLE `contact_history_action` DROP FOREIGN KEY `brand_TO_contact_history_action`;

ALTER TABLE `security_question` DROP FOREIGN KEY `brand_TO_security_question`;

ALTER TABLE `central_service_allocation` DROP FOREIGN KEY `service_provider_TO_central_service_allocation`;

ALTER TABLE `town_allocation` DROP FOREIGN KEY `service_provider_TO_town_allocation`;

ALTER TABLE `client` DROP FOREIGN KEY `service_provider_TO_client`;

ALTER TABLE `general_default` DROP FOREIGN KEY `brand_TO_general_default`;

ALTER TABLE `postcode_allocation` DROP FOREIGN KEY `service_provider_TO_postcode_allocation`;

ALTER TABLE `completion_status` DROP FOREIGN KEY `brand_TO_completion_status`;

# ---------------------------------------------------------------------- #
# Modify table "service_provider"                                        #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider` ADD COLUMN `ReplyEmail` VARCHAR(64);

ALTER TABLE `service_provider` MODIFY `ReplyEmail` VARCHAR(64) AFTER `Skin`;

# ---------------------------------------------------------------------- #
# Modify table "brand"                                                   #
# ---------------------------------------------------------------------- #

ALTER TABLE `brand` ADD COLUMN `AutoSendEmails` ENUM('1','0') DEFAULT '1';

ALTER TABLE `brand` MODIFY `AutoSendEmails` ENUM('1','0') DEFAULT '1' AFTER `Skin`;

# ---------------------------------------------------------------------- #
# Modify table "email"                                                   #
# ---------------------------------------------------------------------- #

ALTER TABLE `email` ADD COLUMN `EmailCode` VARCHAR(64);

ALTER TABLE `email` MODIFY `EmailCode` VARCHAR(64) AFTER `EmailID`;

# ---------------------------------------------------------------------- #
# Modify table "email_job"                                               #
# ---------------------------------------------------------------------- #

ALTER TABLE `email_job` ADD COLUMN `MailBody` TEXT;

ALTER TABLE `email_job` ADD COLUMN `MailAccessCode` VARCHAR(32);

ALTER TABLE `email_job` MODIFY `MailBody` TEXT AFTER `EmailJobID`;

ALTER TABLE `email_job` MODIFY `MailAccessCode` VARCHAR(32) AFTER `MailBody`;

# ---------------------------------------------------------------------- #
# Add foreign key constraints                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider` ADD CONSTRAINT `county_TO_service_provider` 
    FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`);

ALTER TABLE `service_provider` ADD CONSTRAINT `country_TO_service_provider` 
    FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `service_provider` ADD CONSTRAINT `user_TO_service_provider` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `brand` ADD CONSTRAINT `client_TO_brand` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `brand` ADD CONSTRAINT `network_TO_brand` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `brand` ADD CONSTRAINT `user_TO_brand` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `email` ADD CONSTRAINT `user_TO_email` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `email_job` ADD CONSTRAINT `job_TO_email_job` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `email_job` ADD CONSTRAINT `email_TO_email_job` 
    FOREIGN KEY (`EmailID`) REFERENCES `email` (`EmailID`);

ALTER TABLE `email_job` ADD CONSTRAINT `user_TO_email_job` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `network_service_provider` ADD CONSTRAINT `service_provider_TO_network_service_provider` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `user` ADD CONSTRAINT `service_provider_TO_user` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `job` ADD CONSTRAINT `service_provider_TO_job` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `brand_branch` ADD CONSTRAINT `brand_TO_brand_branch` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`) ON UPDATE CASCADE;

ALTER TABLE `job_type` ADD CONSTRAINT `brand_TO_job_type` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `county` ADD CONSTRAINT `brand_TO_county` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `country` ADD CONSTRAINT `brand_TO_country` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `payment_type` ADD CONSTRAINT `brand_TO_payment_type` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `customer_title` ADD CONSTRAINT `brand_TO_customer_title` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `client_purchase_order` ADD CONSTRAINT `brand_TO_client_purchase_order` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `service_type` ADD CONSTRAINT `brand_TO_service_type` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `user` ADD CONSTRAINT `brand_TO_user` 
    FOREIGN KEY (`DefaultBrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `audit_trail_action` ADD CONSTRAINT `brand_TO_audit_trail_action` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `contact_history_action` ADD CONSTRAINT `brand_TO_contact_history_action` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `security_question` ADD CONSTRAINT `brand_TO_security_question` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `service_provider_TO_central_service_allocation` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `service_provider_TO_town_allocation` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `client` ADD CONSTRAINT `service_provider_TO_client` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `general_default` ADD CONSTRAINT `brand_TO_general_default` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `service_provider_TO_postcode_allocation` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `completion_status` ADD CONSTRAINT `brand_TO_completion_status` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.85');
