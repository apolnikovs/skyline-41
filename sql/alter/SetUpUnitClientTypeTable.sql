 --
-- Generating rows for table unit_client_type
--
INSERT INTO unit_client_type (UnitTypeID, ClientID, CreatedDate, ModifiedUserID)
select t1.UnitTypeID, t2.ClientID, null, t4.UserID
from unit_type t1, client t2
left join user t4 on t4.Username='sa';   