# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.186');

# ---------------------------------------------------------------------- #
# Add table "appointment"                                                 #
# ---------------------------------------------------------------------- #
ALTER TABLE `appointment`
ADD COLUMN `BreakStartSec` INT(11) NULL DEFAULT NULL AFTER `ViamenteExclude`,
ADD COLUMN `BreakDurationSec` INT(11) NULL DEFAULT NULL AFTER `BreakStartSec`;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.187');
